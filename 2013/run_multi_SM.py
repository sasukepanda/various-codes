import os
import sys
import subprocess
import shlex
import shutil
import argparse

fol = fol = "/u/leongs/Desktop/JulieP/multiSM"

######## Script starts here ##########
if __name__ == '__main__':
    list_fasta = os.listdir(os.path.join(fol, "fasta"))

    for f in list_fasta:
        for i, q in enumerate((50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1250, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000)):
            if False:
                last = "&"
            else:
                last = ""
            print('java -jar {mirbooking_exec_path} '
                 '-l {logbase} -t {prob_thr} -d {mirbooking_db_path} '
                 '{mir_qty} {nm_qty} {fasta_file} '
                 '>{dest} {last}').format(mirbooking_exec_path="/u/admc/soft/mirbook/StableMarriage.jar",
                                   logbase=16384,
                                   prob_thr=0.0118,
                                   mirbooking_db_path="/u/admc/public_data/mirbook/db",
                                   mir_qty="/u/leongs/Desktop/JulieP/multiSM/inputs/miRNA/GSM853219_hsa_absolute_annotated_mirbase.txt",
                                   nm_qty=os.path.join(fol, "qty_files", "GSM853210_absolute_RefSeq_normalised", "gene_{qt}.txt".format(qt=q)),
                                   fasta_file=os.path.join(fol, "fasta", f),
                                   dest=os.path.join(fol, "results", "{seq}_{qt}.txt".format(seq=f.replace(".fasta", ""),
                                                                                             qt=q)),
                                   last=last)
