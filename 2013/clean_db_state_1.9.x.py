import os
import re
import subprocess
import shlex
import shutil


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output

dict_project_uuid = dict()

# read the db_state file
state_file = "/u/leongs/data/mcweb-1.10_dev/db_state_1.9.x.txt"
workdir = "/export/home/leongs/mcweb-1.9_prod"
os.chdir(workdir)
with open(state_file, 'rb') as state:
    now_parsing = ""
    dict_task_id_to_parent_task_id = dict()

    for l in state:
        db_object = None
        line = l.strip()
        if '#users data' in line:
            now_parsing = 'users'
            print line
            continue
        elif '#tasks data' in line:
            now_parsing = 'tasks'
            print line
            continue
        elif '#samples data' in line:
            now_parsing = 'samples'
            print line
            continue
        elif '#project_guests data' in line:
            now_parsing = 'project_guests'
            print line
            continue

        if now_parsing == 'tasks':
            splitted = line.split('\t')
            if len(splitted) >= 10:
                list_var = ['id', 'parent_id',
                            'tool_name', 'status_name',
                            'label', 'user',
                            'start_date', 'last_activity', 'end_date',
                            'is_processed', 'project_uuid',
                            'project_name', 'project_owner',
                            'project_creation_date', 'project_last_activity',
                            'project_last_opened', 'project_is_deleted',
                            'remote_addr', 'user_agent',
                            'params_hash', 'cpu_time','walltime',
                            'used_memory', 'used_vmemory', 'exec_host']

                dict_params = dict()

                for index, var in enumerate(splitted):
                    value = var
                    dict_params[list_var[index]] = value

                if dict_params['tool_name'] == "mcfold2" or dict_params['tool_name'].startswith("mirbook"):
                    if os.path.exists(os.path.join(workdir, dict_params['id'])):
                        shutil.rmtree(os.path.join(workdir, dict_params['id']))
                    continue
                elif dict_params['tool_name'] == 'mcflashfold':
                    dict_params['tool_name'] = "mcfold2"
                    curr_dir = os.path.join(workdir, dict_params['id'])
                    for elem in os.listdir(curr_dir):
                        os.rename(os.path.join(curr_dir, elem),
                                  os.path.join(curr_dir, elem.replace("mcflashfold", "mcfold2")))

                print "\t".join([str(dict_params[k]) for k in list_var if k in dict_params])
        else:
            print line