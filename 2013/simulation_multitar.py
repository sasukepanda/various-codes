import random

for smart in xrange(16385):
    list_rand = []
    if smart == 0:
        list_rand.append("smart\gene")
        for gene in xrange(8000):
            list_rand.append("gene_{num:09d}".format(num=gene))
    else:
        list_rand.append("smart_{num:09d}".format(num=smart))
        for gene in xrange(8000):
            list_rand.append( str(2*random.random()) )
    print "\t".join(list_rand)