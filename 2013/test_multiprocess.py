from multiprocessing import Pool
import random

def thr(name, iter):
    for i in xrange(iter):
        pass
    return name

if __name__ == '__main__':
    pool = Pool(processes=3)
    list_rand = []
    for r in xrange(15):
        list_rand.append(random.randint(100000, 120000))
    print list_rand
    result = [pool.apply_async(thr, ["proc_{0}".format(j), j*500]) for j in list_rand]

    pool.close()
    pool.join()
    print "HERE"