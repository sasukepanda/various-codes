import os
import argparse
import sys

def validate_sequences(seq1, seq2):
    """ put the seq in uppercase and make sure they"re ATUGC, convert T to U """
    def convert_sequence(seq):
        temp_seq = ""
        for char in seq.upper().replace("T", "U"):
            if not char in ("A", "U", "G", "C"):
                sys.exit("Illegal character in sequence: {seq}".format(seq=seq))
            else:
                temp_seq += char
        return temp_seq
    return(convert_sequence(seq1), convert_sequence(seq2))

def validate_structure(struc, seq_length):
    """ put the seq in uppercase and make sure they"re ATUGC, convert T to U """
    if (struc.count("()") or struct.count("[]")):
        sys.exit("Cannot model () or []... sorry...")
    if (struc.count(".")*100) / seq_length > 40:
        sys.exit("Your structure is, well, quite unstructured...sorry...")
    for char in struc:
        if char not in ("(", ")", ".", "[", "]"):
            sys.exit("Illegal character in structure: {struc}".format(struc=struc))

if __name__ == '__main__':
 
    parser = argparse.ArgumentParser(description='Parser for %(prog)s')

    parser.add_argument('--sequence1', '-s1', action="store", help="The sequence of the first sequence", dest="sequence1", required=True)
    parser.add_argument('--dotbracket1', '-d1', action="store", help="The structure of the first sequence in dot-bracket notation", dest="dotbracket1", required=True)
    parser.add_argument('--sequence2', '-s2', action="store", default="", help="(optional) The sequence of a second sequence", dest="sequence2")
    parser.add_argument('--dotbracket2', '-d2', action="store", default="", help="(optional) The sequence of a second sequence", dest="dotbracket2")
    parser.add_argument('--noncanonical', '-c', action="store_true", help="(optional) Use non-canonical base-pairs. Default=False", dest="noncanonical")
    parser.add_argument('--mergermsd', '-m', action="store", default=1.5, type=float, help="(optional) Specify mergermsd value. Default=1.5", dest="mergermsd")

    ns = parser.parse_args()

    sequence1 = ns.sequence1
    dotbracket1 = ns.dotbracket1
    sequence2 = ns.sequence2
    dotbracket2 = ns.dotbracket2
    noncanonical = ns.noncanonical
    mergermsd = ns.mergermsd

    if len(sequence1) != len(dotbracket1) or len(sequence2) != len(dotbracket2):
        sys.exit("the sequence and its structure must be of the same length")

    sequence1, sequence2 = validate_sequences(sequence1, sequence2)
    validate_structure(dotbracket1, len(sequence1))
    validate_structure(dotbracket2, len(sequence2))

    # step 1
    # color the base pairs
    stack_pair = []
    previous = ""
    alphabet = string.uppercase
    ind_color = 0
    for c in dotbracket1:
        if c == "(":
            stack_pair.append(c)
            if previous in (")", "]", "["):
                # change color if we change stem
                ind_color += 1
        elif c == ")":
            pass
        previous = c