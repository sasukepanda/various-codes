import os
import argparse

if __name__ == '__main__':
 
    parser = argparse.ArgumentParser(description='Parser for qt_seq.py')

    parser.add_argument('--qt_file', '-q', action="store", dest="qt_file")
    parser.add_argument('--seq_file', '-s', action="store", dest="seq_file")

    ns = parser.parse_args()

    qt_file = ns.qt_file
    seq_file = ns.seq_file
    
    seq_dict = dict()
    with open(seq_file, 'r') as seq_cont:
        list_line = seq_cont.readlines()
        i = 0
        for i in xrange(len(list_line)):
            line = list_line[i]
            if line.startswith(">"):
                splitted = line.split()
                acc = splitted[1]
                seq = ""
                while i+1 < len(list_line):
                    if list_line[i+1].startswith(">"):
                        break
                    seq += list_line[i+1].strip()
                    i += 1
                seq_dict[acc] = seq
            i += 1

    # read the qt file
    list_not_found = []
    with open(qt_file, 'r') as qt_cont:
        for line in qt_cont:
            splitted = line.split()
            acc = splitted[0]
            if acc not in seq_dict:
                list_not_found.append(line.strip())
            else:
                print "{acc}\t{qty}\t{seq}".format(acc=acc, qty=splitted[1], seq=seq_dict.get(acc, "NOT FOUND"))

    print "These MIMAT were not found:"
    for m in list_not_found:
        print m