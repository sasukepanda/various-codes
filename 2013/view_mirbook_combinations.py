import os

main_dir = "/u/leongs/Desktop/StableMarriage/new_results"

list_sample = [elem for elem in os.listdir(main_dir) if os.path.isdir(os.path.join(main_dir, elem))]

list_comb = []

for sample in list_sample:
    print sample
    res_file = os.path.join(main_dir, sample, 'results', sample+'_wt')
    dict_comb = dict()
    with open(res_file, 'r') as res:
        for line in res:
            splitted = line.strip().split('\t')
            if not line.startswith('#') and len(splitted) == 7:
                gene = splitted[0].strip()
                miR = splitted[2].strip()
                pos = int(splitted[4].strip())
                prob = float(splitted[5].strip())
                number = int(splitted[6].strip())
                
                comb_key = gene + "_" + miR
                if not comb_key in dict_comb:
                    dict_comb[comb_key] = 0
                dict_comb[comb_key] += 1
                if dict_comb[comb_key] == 104:
                    print comb_key
                    break
#    list_comb.extend(dict_comb.values())
#    print max(dict_comb.values())
#print max(list_comb)