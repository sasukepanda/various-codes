# -*- coding: UTF-8 -*-

"""

"""

import os
import argparse
import sys
import math
import cPickle as pickle

TARGET_TYPE_CDS = "CDS"
TARGET_TYPE_3UTR = "3UTR"
TARGET_TYPE_5UTR = "5UTR"
TARGET_TYPE_UNKNOWN = "UNKNOWN"

def compute_min_max_avg_median(list_values):
    """ Return the min, max, average and median of the values in list_values """
    list_values.sort()
    maximum = int(max(list_values))
    minimum = int(min(list_values))
    avg = int(round(sum(list_values)/len(list_values)))

    ind = len(list_values)/2
    if len(list_values)%2 == 0:
        median = round((list_values[ind]+list_values[ind-1]) / 2.0)
    else:
        median = list_values[ind]

    return maximum, minimum, avg, int(median)

def save_formatted_file(content, target_file):
    """ save the qty file """
    directory = os.path.dirname(target_file)
    if not os.path.exists(directory):
        os.makedirs(directory)
    with open(target_file, 'w') as target:
        target.write("\n".join(content))

def compute_qty_file(qty_input, id_input):
    """ parse the mirna info file """
    # read the id_file
    dict_id = dict()
    with open(id_input, 'rb') as id_f:
        dict_id = pickle.load(id_f)

    # read the qty file
    with open(qty_input, 'rb') as qty_f:
        # only get the 'meaningful' lines in the list the elem.strip() condition makes sure there's no empty line in the list
        lines_list = []
        for elem in qty_f:
            if elem.startswith('#'):
                continue
            sp = elem.strip().split('\t')
            if len(sp) >= 2 and float(sp[1]) > 0:
                if sp[0] in dict_id:
                    lines_list.append(elem)
                else:
                    sys.stderr.write("{acc} not found in Database\n".format(acc=sp[0]))

        # sort the list by quantity in decreasing order
        lines_list.sort(key=lambda elem: float((elem.split())[1]), reverse=True)

        rank = 0
        last_value = -1

        print_list = set()
        dict_qty = dict()
        qty_list = []
        for idx, l in enumerate(lines_list):
            accession, qty = l.strip().split("\t")

            if last_value < 0 or last_value > float(qty):
                rank = float(idx + 1)

            last_value = float(qty)
            percentile = int(math.ceil((rank * 100) / len(lines_list)))

            infodict = dict(quantity=qty,
                            percentile=percentile,
                            **dict_id[accession])

            dict_qty[accession] = infodict
            print_list.add("\t".join((accession, str(qty), str(percentile))))

            qty_list.append(round(float(qty)))

        qty_data_dict = dict()
        # complete the sample'S max, min, avg and median values
        qty_data_dict["max"], qty_data_dict["min"], qty_data_dict["avg"], qty_data_dict["median"] = compute_min_max_avg_median(qty_list)

        return dict_qty, qty_data_dict, print_list


def get_type_from_position(gene_dict, pos):
    """ Given a position, return in what part of the gene the position is """
    # it's the nt8 that determine the determines the type of target
    position = pos
    if position <= gene_dict["gene_utr5_end"]:
        return TARGET_TYPE_5UTR
    elif position <= gene_dict["gene_cds_end"]:
        return TARGET_TYPE_CDS
    elif position <= gene_dict["gene_utr3_end"]:
        return TARGET_TYPE_3UTR
    else:
        return TARGET_TYPE_UNKNOWN

def process_different_keys(results_list):
    """ sort and classify the results in 2 dict: by gene and by mirna """
    by_gene = dict()
    by_mir = dict()
    by_combo = dict()
    for index, row in enumerate(results_list):
        gene = row['gene_accession']
        mirna = row['mirna_accession']
        if gene not in by_gene:
            by_gene[gene] = set()
        by_gene[gene].add(index)
         
        if mirna not in by_mir:
            by_mir[mirna] = set()
        by_mir[mirna].add(index)
        
        combo_key = "{g}_{m}".format(g=gene, m=mirna)
        if combo_key not in by_combo:
            by_combo[combo_key] = set()
        by_combo[combo_key].add(index)
        
    return by_gene, by_mir, by_combo

def compute_result(result_file, gene_dict_qty, mirna_dict_qty):
    with open(result_file, "r") as input_file:
        current_result_key = ""
        new_result = None

        # parse a 1st time to compute all the needed data:
        dict_data_gene = dict()
        dict_data_mir = dict()
        list_gene = set()
        dict_effectiveness = dict()
        parsing_eff = False
        list_lines = input_file.readlines()
        for l in list_lines:
            if l.startswith('#'):
                continue
            elif len(l.split()) == 7:
                splitted = l.split()

                gene = splitted[0].strip()
                miR = splitted[2].strip()
                pos = int(splitted[4].strip())
                prob = float(splitted[5].strip())
                number = int(splitted[6].strip())

                list_gene.add(gene)
                
                if not gene in dict_data_gene:
                    dict_data_gene[gene] = dict(sum_repression=0.0, sum_occupancy=0,
                                                utr5_occupancy=0, cds_occupancy=0, utr3_occupancy=0,
                                                list_mir=set())

                # compute the total booked
                dict_data_gene[gene]["sum_occupancy"] += number

                # compute the occupancy
                type = get_type_from_position(gene_dict_qty[gene], pos)
                if type == TARGET_TYPE_3UTR:
                    repression = (number*prob*1.0)
                    dict_data_gene[gene]["sum_repression"] += repression
                    dict_data_gene[gene]['utr3_occupancy'] += number
                elif type == TARGET_TYPE_5UTR:
                    repression = (number*prob*0.1)
                    dict_data_gene[gene]["sum_repression"] += repression
                    dict_data_gene[gene]['utr5_occupancy'] += number
                elif type == TARGET_TYPE_CDS:
                    repression = (number*prob*0.1)
                    dict_data_gene[gene]["sum_repression"] += repression
                    dict_data_gene[gene]['cds_occupancy'] += number

                dict_data_gene[gene]["list_mir"].add(miR)

                if not (miR + "_dict") in dict_data_gene[gene]:
                    dict_data_gene[gene][miR + "_dict"] = dict(repression_mir=0.0, dict_pos_prob=dict())

                if not ('{pos}_{prob}'.format(pos=pos, prob=prob)) in dict_data_gene[gene][miR + "_dict"]:
                    dict_data_gene[gene][miR + "_dict"]['dict_pos_prob']['{pos}_{prob}'.format(pos=pos, prob=prob)] = 0
                dict_data_gene[gene][miR + "_dict"]['dict_pos_prob']['{pos}_{prob}'.format(pos=pos, prob=prob)] += number
                dict_data_gene[gene][miR + "_dict"]["repression_mir"] += repression

                if not miR in dict_data_gene[gene]:
                    dict_data_gene[gene][miR] = 0
                dict_data_gene[gene][miR] += number
                
                if not ("g_{pos}".format(pos=pos)) in dict_data_gene[gene]:
                    dict_data_gene[gene]["g_{pos}".format(pos=pos)] = 0
                dict_data_gene[gene]["g_{pos}".format(pos=pos)] += number
                
                if not miR in dict_data_mir:
                    dict_data_mir[miR] = 0
                dict_data_mir[miR] += number

                mirna_dict_qty[miR]['targets_list'] = mirna_dict_qty[miR].get('targets_list', set())
                mirna_dict_qty[miR]['targets_number'] = mirna_dict_qty[miR].get('targets_number', 0)
                if not gene in mirna_dict_qty[miR]['targets_list']:
                    mirna_dict_qty[miR]['targets_list'].add(gene)
                    mirna_dict_qty[miR]['targets_number'] += 1

            else:
                pass

        # now compute a 2nd time to get the "real" values
        results_list_basic = []
        results_list_detailed = []
        for gene in list_gene:
            gene_dict_qty[gene]["sum_repression"] = dict_data_gene[gene]["sum_repression"]
            gene_dict_qty[gene]["sum_occupancy"] = dict_data_gene[gene]["sum_occupancy"]
            gene_dict_qty[gene]["cds_occupancy"] = dict_data_gene[gene]["cds_occupancy"]
            gene_dict_qty[gene]["utr3_occupancy"] = dict_data_gene[gene]["utr3_occupancy"]
            gene_dict_qty[gene]["utr5_occupancy"] = dict_data_gene[gene]["utr5_occupancy"]
            qty_gene = gene_dict_qty[gene]["quantity"]

            for mir in dict_data_gene[gene]["list_mir"]:
                total_effectiveness = dict_data_gene[gene][mir + "_dict"]['repression_mir']/dict_data_gene[gene]["sum_repression"]

                occupancy = 0
                for k, v in dict_data_gene[gene][mir + "_dict"]['dict_pos_prob'].iteritems():
                    splitted_k = k.split('_')
                    pos = int(splitted_k[0])
                    prob = splitted_k[1]

                    target_type = get_type_from_position(gene_dict_qty[gene], pos)

                    pond = 0.1
                    if target_type == 'CDS':
                        target_type_alias = "CDS"
                    elif target_type == '3UTR':
                        pond = 1.0
                        target_type_alias = "3'UTR"
                    else:
                        target_type_alias = "5'UTR"

                    effectiveness = (float(prob)*float(v)*pond)/dict_data_gene[gene]["sum_repression"]

                    results_list_detailed.append(dict(gene_accession=gene_dict_qty[gene]["accession"],
                                                      gene_name=gene_dict_qty[gene]["name"],
                                                      mirna_accession=mirna_dict_qty[mir]["accession"],
                                                      mirna_name=mirna_dict_qty[mir]["name"],
                                                      position=int(pos),
                                                      target_type=target_type_alias,
                                                      prob=float(prob),
                                                      number=int(v),
                                                      gene_percentile=int(gene_dict_qty[gene]["percentile"]),
                                                      mirna_percentile=int(mirna_dict_qty[mir]["percentile"]),
                                                      gene_quantity=float(gene_dict_qty[gene]["quantity"]),
                                                      mirna_quantity=float(mirna_dict_qty[mir]["quantity"]),
                                                      effectiveness=effectiveness))

                    occupancy += int(v)


                results_list_basic.append(dict(gene_accession=gene_dict_qty[gene]["accession"],
                                               gene_name=gene_dict_qty[gene]["name"],
                                               mirna_accession=mirna_dict_qty[mir]["accession"],
                                               mirna_name=mirna_dict_qty[mir]["name"],
                                               effectiveness=total_effectiveness,
                                               gene_percentile=int(gene_dict_qty[gene]["percentile"]),
                                               mirna_percentile=int(mirna_dict_qty[mir]["percentile"]),
                                               gene_quantity=float(gene_dict_qty[gene]["quantity"]),
                                               mirna_quantity=float(mirna_dict_qty[mir]["quantity"]),
                                               occupancy=occupancy))


        basic_gene_index, basic_mir_index, basic_combo_index = process_different_keys(results_list_basic)
        detailed_gene_index, detailed_mir_index, detailed_combo_index = process_different_keys(results_list_detailed)
        
        return basic_gene_index, basic_mir_index, basic_combo_index, results_list_basic, detailed_gene_index, detailed_mir_index, detailed_combo_index, results_list_detailed

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Parser for miRBooking Digestor')

    parser.add_argument('--workdir', '-w', action="store", dest="workdir", required=True)
    parser.add_argument('--mirna_qty_file', '-m', action="store", dest="mirna_qty_file", required=True)
    parser.add_argument('--mirna_id_file', '-M', action="store", dest="mirna_id_file", required=True)
    parser.add_argument('--gene_qty_file', '-g', action="store", dest="gene_qty_file", required=True)
    parser.add_argument('--gene_id_file', '-G', action="store", dest="gene_id_file", required=True)
    parser.add_argument('--result_file', '-r', action="store", dest="result_file", required=True)

    ns = parser.parse_args()
    
    workdir = ns.workdir
    mirna_qty_file = ns.mirna_qty_file
    mirna_id_file = ns.mirna_id_file
    gene_qty_file = ns.gene_qty_file
    gene_id_file = ns.gene_id_file
    result_file = ns.result_file
    
    mirna_dict_qty, mirna_qty_data, mirna_print_list = compute_qty_file(mirna_qty_file, mirna_id_file)
    gene_dict_qty, gene_qty_data, gene_print_list = compute_qty_file(gene_qty_file, gene_id_file)
    
    basic_gene_index, basic_mir_index, basic_combo_index, results_list_basic, \
    detailed_gene_index, detailed_mir_index, detailed_combo_index, results_list_detailed = compute_result(result_file, gene_dict_qty, mirna_dict_qty)
    
    # write in pickles
    with open(os.path.join(workdir, "mirna_dict_qty.pk"), 'wb') as pkl:
        pickle.dump(mirna_dict_qty, pkl, -1)
        
    with open(os.path.join(workdir, "mirna_stats.pk"), 'wb') as pkl:
        pickle.dump(mirna_qty_data, pkl, -1)
        
    with open(os.path.join(workdir, "gene_dict_qty.pk"), 'wb') as pkl:
        pickle.dump(gene_dict_qty, pkl, -1)
        
    with open(os.path.join(workdir, "gene_stats.pk"), 'wb') as pkl:
        pickle.dump(gene_qty_data, pkl, -1)
        
    with open(os.path.join(workdir, "basic_gene_index.pk"), 'wb') as pkl:
        pickle.dump(basic_gene_index, pkl, -1)
        
    with open(os.path.join(workdir, "basic_mir_index.pk"), 'wb') as pkl:
        pickle.dump(basic_mir_index, pkl, -1)
        
    with open(os.path.join(workdir, "basic_combo_index.pk"), 'wb') as pkl:
        pickle.dump(basic_combo_index, pkl, -1)
        
    with open(os.path.join(workdir, "basic_results_list.pk"), 'wb') as pkl:
        pickle.dump(results_list_basic, pkl, -1)
        
    with open(os.path.join(workdir, "detailed_gene_index.pk"), 'wb') as pkl:
        pickle.dump(detailed_gene_index, pkl, -1)
        
    with open(os.path.join(workdir, "detailed_mir_index.pk"), 'wb') as pkl:
        pickle.dump(detailed_mir_index, pkl, -1)
        
    with open(os.path.join(workdir, "detailed_combo_index.pk"), 'wb') as pkl:
        pickle.dump(detailed_combo_index, pkl, -1)
        
    with open(os.path.join(workdir, "detailed_results_list.pk"), 'wb') as pkl:
        pickle.dump(results_list_detailed, pkl, -1)
    

    save_formatted_file(mirna_print_list, os.path.join(workdir, "mrna.qty"))
    save_formatted_file(mirna_print_list, os.path.join(workdir, "mirna.qty"))