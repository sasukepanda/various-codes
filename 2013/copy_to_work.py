# -*- coding: UTF-8 -*-

# This file is part of mcweb
#
# mcweb is web application build on top of the mctools pipeline
# Copyright (C) 2011,2012,2013 Université de Montréal
#

"""
Utility function to handle communication with command-line executed command
"""

import subprocess
import shlex
import os
import shutil




decoy_f = "/u/leongs/mcweb_shared/decoy_db"

list_mirna = [elem for elem in os.listdir(decoy_f) if
                  os.path.isdir(os.path.join(decoy_f, elem))]

for mirna in list_mirna:
    main_dir = os.path.join(decoy_f, mirna)
    pdb_file = [elem for elem in os.listdir(main_dir) if elem.endswith(".pdb.gz")][0]
    shutil.copy(os.path.join(main_dir, pdb_file), os.path.join(main_dir, 'work', pdb_file))
#     print pdb_file
#     work_dir = os.path.join(decoy_f, mirna, 'work')
#     list_pdb = [elem for elem in os.listdir(work_dir) if elem.endswith(".pdb")]
#     
#     for pdb in list_pdb:
#         pdb_p = os.path.join(work_dir, pdb)
#         call_command('gzip "{unzipped}"'.format(unzipped=pdb_p))