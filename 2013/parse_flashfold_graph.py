import os
import argparse

if __name__ == '__main__':
 
    parser = argparse.ArgumentParser()

    parser.add_argument('--file', '-f', action="store", dest="file")

    ns = parser.parse_args()

    file_path = ns.file
    list_struct = []
    with open(file_path, "r") as txt:
        listlines = [elem.strip().split()[2] for elem in txt.readlines() if elem.strip() and len(elem.split()) == 5]
        set_lines = set(listlines)
        dict_count = dict((elem, listlines.count(elem)) for elem in list(set_lines))
        for k, v in dict_count.iteritems():
            if v != 1:
                print k
                print v, "times\n"