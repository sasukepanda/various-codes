list_nucleotide = ("A", "C", "G", "U")

i = 0

for n1 in list_nucleotide:
    for n2 in list_nucleotide:
        for n3 in list_nucleotide:
            for n4 in list_nucleotide:
                for n5 in list_nucleotide:
                    for n6 in list_nucleotide:
                        for n7 in list_nucleotide:
                            i += 1
                            seq = n1+n2+n3+n4+n5+n6+n7
                            print "SMART{num:06d}\tSmartRNA-{seq}\tN{seq}NNNNNNNNNNNNNN".format(num=i,
                                                                                                seq=seq)