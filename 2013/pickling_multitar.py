import pickle

main_dict = dict()
dict_smart = dict()
dict_gene = dict()

#with open("/u/leongs/Desktop/multitar/all_possible_seeds.txt", 'r') as smart_f:
#    for i, line in enumerate(smart_f):
#        acc = "smart_{num:09d}".format(num=i+1)
#        dict_smart[acc] = dict(sequence=line.strip(), index=i+1)

main_dict = dict()
#main_list = []
with open("/u/leongs/Desktop/multitar/simultitar.txt", 'r') as multitar_f:
    list_lines = [elem.strip() for elem in multitar_f.readlines() if elem.strip()]
#    main_list = [[float(elem) for elem in line.strip().split()[1:]] for line in list_lines[1:] if line.strip()]
    header = list_lines[0].split("\t")
#    dict_gene = dict((gene, i+1) for (i, gene) in enumerate(header[1:]))
    for line in list_lines[1:]:
        splitted = line.strip().split("\t")
        acc = splitted[0]
        for i, fc in enumerate(splitted[1:]):
            gene = header[i+1]
            unique_id = "{acc}X{gene}".format(acc=acc, gene=gene)
            main_dict[unique_id] = float(fc)

print len(main_dict)
try:
    with open("/u/leongs/Desktop/multitar/multitar_dict.pk", "w") as pickle_file:
        pickle.dump(main_dict, pickle_file, -1)
except Exception as e:
    print e

#try:
#    with open("/u/leongs/Desktop/multitar/smarts.pk", "w") as pickle_file:
#        pickleable_dict = dict()
#        pickleable_dict.update(dict_smart)
#        pickle.dump(pickleable_dict, pickle_file, -1)
#except Exception as e:
#    print e
#
#try:
#    with open("/u/leongs/Desktop/multitar/genes.pk", "w") as pickle_file:
#        pickleable_dict = dict()
#        pickleable_dict.update(dict_gene)
#        pickle.dump(pickleable_dict, pickle_file, -1)
#except Exception as e:
#    print e
#    
#try:
#    with open("/u/leongs/Desktop/multitar/multitar.pk", "w") as pickle_file:
#        pickle.dump(main_list, pickle_file, -1)
#except Exception as e:
#    print e