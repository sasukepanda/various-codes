# -*- coding: UTF-8 -*-

# This file is part of mcweb
#
# mcweb is web application build on top of the mctools pipeline
# Copyright (C) 2011,2012,2013 Université de Montréal
#

"""
Utility function to handle communication with command-line executed command
"""

import subprocess
import shlex
import os


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output


decoy_f = "/u/leongs/mcweb_shared/decoy_db"

list_mirna = [elem for elem in os.listdir(decoy_f) if
                  os.path.isdir(os.path.join(decoy_f, elem))]

for mirna in list_mirna:
    work_dir = os.path.join(decoy_f, mirna, 'work')
    list_pdb = [elem for elem in os.listdir(work_dir) if elem.endswith(".pdb")]
    
    for pdb in list_pdb:
        pdb_p = os.path.join(work_dir, pdb)
        call_command('gzip "{unzipped}"'.format(unzipped=pdb_p))