import os
import math
import cPickle

def compute_qty_file(qty_input_list):
    lines_list = qty_input_list

    # sort the list by quantity in decreasing order
    lines_list.sort(key=lambda elem: float((elem.split())[1]), reverse=True)

    rank = 0
    last_value = -1

    print_list = set()
    dict_qty = dict()
    for idx, l in enumerate(lines_list):
        accession, qty = l.strip().split("\t")

        if last_value < 0 or last_value > float(qty):
            rank = float(idx + 1)

        last_value = float(qty)
        percentile = int(math.ceil((rank * 100) / len(lines_list)))

        infodict = dict(accession=accession,
                        quantity=qty,
                        percentile=percentile)

        dict_qty[accession] = infodict
        
    return dict_qty

def separate_mrna_and_mirna(qty_input):
    # read the qty file
    with open(qty_input, 'rb') as qty_f:
        # only get the 'meaningful' lines in the list the elem.strip() condition makes sure there's no empty line in the list
        mrna_list = []
        mirna_list = []
        for elem in qty_f:
            if elem.startswith('#'):
                continue
            sp = elem.strip().split('\t')
            if len(sp) >= 2 and float(sp[1]) > 0:
                if sp[0].startswith("MIMAT"):
                    mirna_list.append(elem.strip())
                elif sp[0].startswith("NM_"):
                    mrna_list.append(elem.strip())
    return mrna_list, mirna_list


if __name__ == '__main__':
    cell_line_path = "/u/leongs/mcweb_shared/mirbook/cell_lines"
    list_files = os.listdir(cell_line_path)
    
    for f in list_files:
        if f.endswith(".txt"):
            mrna_l, mirna_l = separate_mrna_and_mirna(os.path.join(cell_line_path, f))
            
            dict_mrna = compute_qty_file(mrna_l)
            dict_mirna = compute_qty_file(mirna_l)
            
            os.mkdir(os.path.join("/u/leongs/mcweb_shared/mirbook/new_results", f.replace(".txt", ""), "quantities"))
            with open(os.path.join("/u/leongs/mcweb_shared/mirbook/new_results", f.replace(".txt", ""), "quantities", "gene_quantities.pk"), "wb") as g_qty:
                cPickle.dump(dict_mrna, g_qty, -1)
            with open(os.path.join("/u/leongs/mcweb_shared/mirbook/new_results", f.replace(".txt", ""), "quantities", "mirna_quantities.pk"), "wb") as mirna_qty:
                cPickle.dump(dict_mirna, mirna_qty, -1)