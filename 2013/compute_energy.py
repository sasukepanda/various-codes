# -*- coding: UTF-8 -*-

# This file is part of mcweb
#
# mcweb is web application build on top of the mctools pipeline
# Copyright (C) 2011,2012,2013 Université de Montréal
#

"""
Utility function to handle communication with command-line executed command
"""

import subprocess
import shlex
import os
import shutil


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output

decoy_f = "/u/leongs/mcweb_shared/decoy_db"

list_mirna = sorted([elem for elem in os.listdir(decoy_f) if
                     os.path.isdir(os.path.join(decoy_f, elem))])

list_fasta = []
for mirna in list_mirna:
    work_dir = os.path.join(decoy_f, mirna)
    
    image_dir = os.path.join(work_dir, 'images')
    list_images = os.listdir(image_dir)
    
#     os.mkdir(os.path.join('/u/leongs/public_html/mcweb/decoy_db', mirna))
    for image in list_images:
        if image.endswith('.jpg'):
            shutil.copy(os.path.join(image_dir, image),
                        os.path.join('/u/leongs/public_html/mcweb/decoy_db', mirna, mirna + '.jpg'))
        if image.endswith('.ps'):
            shutil.copy(os.path.join(image_dir, image),
                        os.path.join('/u/leongs/public_html/mcweb/decoy_db', mirna, mirna + '.ps'))
            os.chdir(os.path.join('/u/leongs/public_html/mcweb/decoy_db', mirna))
            call_command("convert -resize 75% {mirna}.ps {mirna}_75.png".format(mirna=mirna), echo=True)
        elif image.endswith('.pdf'):
            shutil.copy(os.path.join(image_dir, image),
                        os.path.join('/u/leongs/public_html/mcweb/decoy_db', mirna, mirna + '.pdf'))
#     accession = mirna
#     name = ""
#     sequence = ""
#     with open(os.path.join(work_dir, accession + ".fa"), 'rb') as fa_file:
#         for line in fa_file:
#             if line.startswith(">"):
#                 name = line.split()[0].replace(">", "")
#             else:
#                 sequence = line.strip()
# 
#     # read the .ss file and extract structure
#     structure = ""
#     with open(os.path.join(work_dir, accession + ".ss"), 'rb') as ss_file:
#         structure = ss_file.read().strip()



#     out, err = call_command('/u/leongs/MC-Flashfold/mcff -s "{seq}"'.format(seq=sequence), echo=False)
#     mfe_energy = float(out.strip().split()[1])
#     
#     out, err = call_command('/u/leongs/MC-Flashfold/mcff -s "{seq}" -m "{struct}"'.format(seq=sequence, struct=structure), echo=False)
#     struct_energy = float(out.strip().split()[1])
#     
#     #generate FASTA
#     suffix = ">{name}|{seq}|{struct}%20{energy}".format(name=name, seq=sequence, struct=structure, energy=struct_energy)
#     
#     out, err = call_command("curl http://www-lbit.iro.umontreal.ca/cgi-bin/mcfold/render.cgi?structure={suff}".format(suff=suffix))
#     print out
#     
#     splitted_out = out.split()
# 
#     os.mkdir(os.path.join(decoy_f, mirna, 'images'))
# 
#     for elem in splitted_out:
#         if elem.startswith('HREF="/mcfold/rundata/'):
#             url = elem.replace('HREF="', '"http://www-lbit.iro.umontreal.ca')
#             os.chdir(os.path.join(decoy_f, mirna, 'images'))
# 
#             out, err = call_command("wget "+ url, echo=True)
#             if err:
#                 print err


# 
# #     print (struct_energy - mfe_energy)
# #     print mirna
#     flashfold_cmd = '/u/leongs/MC-Flashfold/mcff -s "{seq}" -t {threshold}'.format(seq=sequence, threshold=((struct_energy - mfe_energy) + 0.5))
#     out, err = call_command(flashfold_cmd, echo=False)
#   
#     with open(os.path.join(work_dir, accession + "_recomputed_mcff.txt"), 'wb') as nrg_file:
#         nrg_file.write(out)