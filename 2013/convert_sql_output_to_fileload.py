dict_smart = dict()
dict_gene = dict()
dict_cell_line = dict()

with open("/export/home/leongs//multitar/inputs/output_postgres", 'r') as output_psql:
    for line in output_psql:
        stripped = line.strip().replace(' ', '')
        if stripped:
            splitted = stripped.split('|')
            if len(splitted) < 3 or splitted[0] == 'id':
                continue
            elif len(splitted) == 4:
                dict_smart[splitted[1]] = dict(id=int(splitted[0]),
                                               name=splitted[2],
                                               sequence=splitted[3])
            elif len(splitted) == 3:
                dict_gene[splitted[1]] = dict(id=splitted[0],
                                              name=splitted[2])
            else:
                dict_cell_line[splitted[1]] = dict(id=splitted[0],
                                                   description=splitted[2],
                                                   origin=splitted[3],
                                                   taxid=splitted[4])

id_multitar = 1

#with open("/u/leongs/Desktop/multitar/inputs/multitar.txt", 'r') as multitar:
#    list_lines = [elem.strip() for elem in multitar.readlines() if elem.strip()]
#    list_genes = list_lines[0].split('\t')[1:]
#    for line in list_lines[1:]:
#        splitted = line.split('\t')
#        smart_acc = splitted[0]
#        for i, fc in enumerate(splitted[1:]):
#            print "{parent_id}\t{id_gene}\t{id_smart}\t{fc}".format(parent_id=id_multitar,
#                                                                    id_gene=dict_gene[list_genes[i]]['id'],
#                                                                    id_smart=dict_smart[smart_acc]['id'],
#                                                                    fc=fc)



import random

#random_gene = sorted(random.sample(set(dict_gene.keys()), 8000))
random_gene = dict_gene.keys()

# print header
"smart\gene\t{app}".format(app="\t".join(random_gene))

for s_acc, v in dict_smart.iteritems():
    list_rand = []
    list_rand.append(s_acc)
    for gene in random_gene:
        list_rand.append( str(2*random.random()) )
    "\t".join(list_rand)
    
#    "{s_acc}\t{fc}".format(s_acc=s_acc,
#                           fc="\t".join([str(2*random.random()) for gene in random_gene]))