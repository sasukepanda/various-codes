import numpy
import binascii
import cPickle as pickle


list_seed = []
with open("/u/leongs/Documents/Gab/DU145_w_seq.txt", 'r') as seq_file:
    list_seed = [l.strip().split()[2][1:8] for l in seq_file.readlines()]

header = []
sc = []
main_dict = dict()
with open("/u/leongs/Documents/Gab/scores.txt", 'r') as sco:
    for i, line in enumerate(sco):
        if i==0:
            header = line.strip().split('\t')
        else:
            splitted = line.strip().split("\t")
            main_dict[splitted[0]] = dict()
            for j in xrange(1, len(splitted)):
                main_dict[splitted[0]][header[j]] = float(splitted[j])
#            if splitted[0] in list_seed:
#                main_dict[splitted[0]] = dict()
#                for j in xrange(1, len(splitted)):
#                    main_dict[splitted[0]][header[j]] = float(splitted[j])

try:
    with open("/u/leongs/Documents/Gab/pickle_all.pk", "w") as pickle_file:
        pickleable_dict = dict()
        pickleable_dict.update(main_dict)
        pickle.dump(pickleable_dict, pickle_file, -1)
except Exception as e:
    print e