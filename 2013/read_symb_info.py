filepath = "/u/leongs/Desktop/StableMarriage/Symb_info.tsv"

set_symbol = set()
with open(filepath, 'rb') as s_info:
    for line in s_info.readlines()[1:]:
        splitted = line.split('\t')
        if splitted[1]!= "-":
            for elem in splitted[1].split('|'):
                set_symbol.add(elem)
        set_symbol.add(splitted[0])
        
#print set_symbol
not_present= set()
with open("/u/leongs/Desktop/StableMarriage/list_gene_db.txt", 'rb') as g_db:
    for line in g_db:
        if not line.strip() in set_symbol:
            not_present.add(line.strip())
            
print "\n".join(sorted(list(not_present)))
print len(not_present)