import os

decoy_db_folder = "/export/home/leongs/decoydb"

dict_fasta = dict()

with open(os.path.join(decoy_db_folder, "hairpin", "hairpin.fa"), "r") as hp_fa:
    name = ""
    acc = ""
    seq = ""
    for line in hp_fa:
        if line.startswith(">") or not line.strip():
            if seq:
                if name.startswith("hsa"):
                    dict_fasta[name] = dict(accession=acc, sequence=seq)
            splitted = line.strip().replace('>', '').split()
            name = splitted[0]
            acc = splitted[1]
            seq = ""
        else:
            seq += line.strip()
    dict_fasta[name] = dict(accession=acc, sequence=seq)

for k, v in dict_fasta.iteritems():
    cmd = ('/u/leongs/Desktop/flashfold/f27 -tables /u/leongs/Desktop/flashfold/tables -name "{name}" '
           '-seq "{seq}" -ft 100000 -v >{output_file};').format(name=k,
                                                                seq=v["sequence"],
                                                                output_file=os.path.join(decoy_db_folder,
                                                                                         "results",
                                                                                         "{name}_{acc}".format(name=k,
                                                                                                               acc=v["accession"])))
    print cmd