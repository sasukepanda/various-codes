import os
import shutil

f_2d = "/u/leongs/mcweb_shared/2d"
decoy_db_f = "/u/leongs/mcweb_shared/decoy_db"
f_math = "/u/leongs/mcweb_shared/mature"

list_mir = os.listdir(decoy_db_f)

for mirna in list_mir:
    shutil.copy(os.path.join(f_2d, mirna),
                os.path.join(decoy_db_f, mirna, mirna + '.mcff'))
    shutil.copy(os.path.join(f_math, mirna),
                os.path.join(decoy_db_f, mirna, mirna + '.mat'))