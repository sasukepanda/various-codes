import os

import subprocess
import shlex


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output


list_cl = os.listdir("/u/leongs/mcweb_shared/mirbook/new_results")

for cell_line in sorted(list_cl):
    if os.path.isdir(os.path.join("/u/leongs/mcweb_shared/mirbook/new_results", cell_line)):
        cl = cell_line
        if cell_line == "NCIH332M":
            cl = "NCIH322M"

        script_content = "set threshold = 0.0179\nset logbase = 512\nuse {cl}".format(cl=cl)

        script_path = os.path.join("/u/leongs/mcweb_shared/mirbook/new_results", cell_line, cl + ".script")
        result_path = os.path.join("/u/leongs/mcweb_shared/mirbook/new_results", cell_line, "results")
        
        with open(script_path, 'wd') as s_f:
            s_f.write(script_content)

        out, err = call_command("java -jar /u/leongs/Desktop/multitar/Multitar_1.3.3.jar -s '/u/leongs/mcweb_shared/mirbook/min-scores' -o {res_p} {s_p}".format(res_p=result_path,
                                                                                                                                                                 s_p=script_path),
                                echo=True)
        print err
        
        os.rename(os.path.join("/u/leongs/mcweb_shared/mirbook/new_results", cell_line, "results", cl),
                  os.path.join("/u/leongs/mcweb_shared/mirbook/new_results", cell_line, "results", cell_line + "_wt"))

        cmd = ('python /u/leongs/workspace/mcweb/script/miRBooking_digestor.py '
               '--workdir /u/leongs/mcweb_shared/mirbook/new_results/{cell_line}/processed/ '
               '-m /u/leongs/mcweb_shared/mirbook/new_results/{cell_line}/quantities/mirna_quantities.pk '
               '-M /u/leongs/Desktop/StableMarriage/mir_pk.pk '
               '-g /u/leongs/mcweb_shared/mirbook/new_results/{cell_line}/quantities/gene_quantities.pk '
               '-G /u/leongs/mcweb_shared/mirbook/gene_pk.pk '
               '-r /u/leongs/mcweb_shared/mirbook/new_results/{cell_line}/results/{cell_line}_wt').format(cell_line=cell_line)

        out, err = call_command(cmd, echo=True)
        print err