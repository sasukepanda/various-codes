import os
import sys
import argparse
import shutil
import subprocess
import shlex

def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output

def call_convert(mirna, rate):
    out, err = call_command("convert -resize {rate}% {mirna}.ps {mirna}_70.png".format(mirna=mirna,
                                                                                       rate=rate), echo=(verbosity > 1))
    if err:
        print accession
        print err

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parser for DecoyDB Digestor')

    parser.add_argument('--dir', '-d', action="store", dest="dir", required=True)
    parser.add_argument('--verbosity', '-v', action="count", dest="verbosity", default=0)

    ns = parser.parse_args()

    dir = ns.dir
    verbosity = ns.verbosity

    for mirna in os.listdir(dir):
        image_dir = os.path.join(dir, image, "images")
        os.chdir(image_dir)
        for rate in [75, 70, 65, 60, 55, 50]:
            call_convert(mirna, rate)