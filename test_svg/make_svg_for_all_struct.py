import os
import shutil

hairpin_dir = "/u/leongs/projet_naim/mcff/hairpin"
struct_dir = "/u/leongs/projet_naim/mcff/2dclean"
dest_dir = "/export/home/leongs/VMWare_share/test_pseudoviewer/all_struct"

for elem in os.listdir(dest_dir):
    os.remove(os.path.join(dest_dir, elem))

for elem in sorted(os.listdir(struct_dir)):
    hairpin = ""
    with open(os.path.join(hairpin_dir, elem), 'rb') as h_c:
        hairpin = "".join(h_c.readlines()[1:])

    with open(os.path.join(struct_dir, elem), 'rb') as s_c:
        for i, line in enumerate(s_c):
            if line.strip() and i < 100:
                with open(os.path.join(dest_dir, elem + "_" + str(i+1) + ".txt"), 'wb') as struct_c:
                    struct_c.write("{h}\n{s}".format(h=hairpin.strip(),
                                                     s=line.split()[0]))
