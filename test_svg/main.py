from lxml import etree
import re
import os
import argparse
import cPickle


def recursive_delete_circle(elem):
    list_to_recurse = []
    desired_node = None
    for child in elem.getchildren():
        if child.attrib.get("fill", "none") in ["gold", "PaleGoldenrod", "LightSteelBlue"]:
            desired_node = elem
            elem.remove(child)

        list_to_recurse.append(child)

    if desired_node is None:
        for child in list_to_recurse:
            desired_node = recursive_delete_circle(child)
            if desired_node is not None:
                break
    return desired_node

def recursive_delete_nucleotide_text_nodes(node):
    curr_list_text_att = []
    for child in node.getchildren():
        if "text" in child.tag and "onmouseover" in child.attrib:
            temp_dict = dict(value=child.text)
            temp_dict.update(child.attrib)
            curr_list_text_att.append(temp_dict)
            node.remove(child)
        else:
            curr_list_text_att.extend(recursive_delete_nucleotide_text_nodes(child))
    return curr_list_text_att

def process_precursor(pk_filepath, svg_filepath, out_filepath):
    pickled_dict = dict()
    with open(pk_filepath, 'rb') as pk_f:
        pickled_dict = cPickle.load(pk_f)

    list_color_lvl = pickled_dict["color_lvl_all"]
    dict_color = dict((str(elem), []) for elem in set(list_color_lvl))

    new_svg_content = ""
    with open(svg_filepath, 'rb') as svg_c:
        new_svg_content = svg_c.read()#.replace('font-family="Tahoma"', 'font-family="courier new"')\
                                      #.replace('font-size="9.8"', 'font-size="9.0"')

    with open(out_filepath, 'wb') as svg_w:
        svg_w.write(new_svg_content)

    doc = etree.parse(out_filepath)

    root_elem = doc.getroot()
    desired_node = recursive_delete_circle(root_elem)
    list_text_att = recursive_delete_nucleotide_text_nodes(root_elem)

    for elem in list_text_att:
        splitted = elem["onmouseover"].split("'")
        pattern = re.search(r'\A[0-9]+', splitted[1])
        if pattern:
            st = pattern.group()
        else:
            alt_pattern = re.search(r'\([0-9]+\)', splitted[1].split()[1])
            if alt_pattern:
                st = alt_pattern.group().replace("(", "").replace(")", "")

        dict_color[str(list_color_lvl[int(st)-1])].append(dict(subnode=elem,
                                                               index=int(st)-1))
        elem["onmouseover"] = elem["onmouseover"].replace("')", ", {perc:.2f}%')".format(perc=float(pickled_dict["stat_all"][int(st)-1])*100))

    for color, list_subnodes in dict_color.iteritems():
        color_node = etree.SubElement(desired_node,
                                      "g",
                                      fill="rgb(255,{diff},{diff})".format(diff=255-int(color)))
        for subnode_dict in list_subnodes:
            subnode_index = subnode_dict["index"]
            subnode = subnode_dict["subnode"]
            
            if subnode_index in pickled_dict["mature_range"]:
                fill_dict = {"stroke": "PaleGoldenrod",
                             "stroke-width": "1"}
            else:
                fill_dict = {"stroke": "none"}
            
            g_node = etree.SubElement(color_node,
                                      "g",
                                      **fill_dict)
            circle_node = etree.SubElement(g_node,
                                           "circle",
                                           cx=str(float(subnode["x"])+2.82),
                                           cy=str(float(subnode["y"])-3.92),
                                           r="4.8")

    temp_parent_node = etree.SubElement(desired_node,
                                        "g",
                                        fill="black",
                                        stroke="none")
    for elem in list_text_att:
        subelem = etree.SubElement(temp_parent_node,
                                   "text",
                                   onmouseover=elem["onmouseover"],
                                   onmouseout=elem["onmouseout"],
                                   x=elem["x"],
                                   y=elem["y"])
        subelem.text = elem["value"]

    outfile = open(out_filepath, 'w')
    doc.write(outfile)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--pk_dir', '-p', action="store", required=True, dest="pk_dir")
    parser.add_argument('--svg_dir', '-s', action="store", required=True, dest="svg_dir")
    parser.add_argument('--out_dir', '-o', action="store", required=True, dest="out_dir")

    ns = parser.parse_args()

    pk_dir = ns.pk_dir
    svg_dir = ns.svg_dir
    out_dir = ns.out_dir

    list_svg = [elem for elem in os.listdir(svg_dir) if elem.endswith(".svg")]

    for svg in sorted(list_svg):
        pk_filepath = os.path.join(pk_dir, svg.split("__")[-1].replace(".svg", ".pk"))
        svg_filepath = os.path.join(svg_dir, svg)
        out_filepath = os.path.join(out_dir, svg)
        process_precursor(pk_filepath, svg_filepath, out_filepath)