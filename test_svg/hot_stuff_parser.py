# list_words = []
# 
# with open("/export/home/leongs/hot_stuff", 'rb') as hot_s:
#     for line in hot_s:
#         stripped = line.strip().replace(".", "").replace(',', '')
#         if stripped:
#             list_words.extend(stripped.split())
#             
# 
# print "\n".join(sorted(list(set(list_words))))

import os

dir_p = "/u/leongs/projet_naim/finalfold/remaining/bp_stats_frag_both_50/vectors"

for elem in [15,16,17,18,19,20,21,22,23,24,25,26,27]:
    vector_file = os.path.join(dir_p, str(elem))
    with open(vector_file, 'rb') as v_f:
        list_v = [l.strip() for l in v_f.readlines()]
        stringer = ('"AtoB": {a}, "AtoC": {b}, "AtoD": {c}, "BtoC": {d}, "BtoD": {e}, "CtoD": {f}').format(elem=elem,
                                                                                                           a=list_v[0],
                                                                                                           b=list_v[1],
                                                                                                           c=list_v[2],
                                                                                                           d=list_v[3],
                                                                                                           e=list_v[4],
                                                                                                           f=list_v[5])
               
        print stringer