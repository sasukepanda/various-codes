import numpy
import cairo
import math
import argparse
import os

_HEIGHT_RATIO, _WIDTH_RATIO = 0, 0
circle_radius = 25
font_size = 18

# if __name__ == '__main__':
#     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
# 
#     parser.add_argument('--height_ratio', '-H', action="store", dest="height_ratio", default=100)
#     parser.add_argument('--width_ratio', '-W', action="store", dest="width_ratio", default=100)
# 
#     ns = parser.parse_args()
# 
#     _HEIGHT_RATIO = int(ns.height_ratio)
#     _WIDTH_RATIO = int(ns.width_ratio)
# 
#     surface = cairo.ImageSurface(cairo.FORMAT_ARGB32,_WIDTH_RATIO,_HEIGHT_RATIO)
#     cr = cairo.Context(surface)
#     
#     # fill with solid white
#     cr.set_source_rgb(1.0, 1.0, 1.0)
#     cr.paint()
#     
#     # draw red circle
#     cr.arc(_HEIGHT_RATIO/2, _WIDTH_RATIO/2, circle_radius, 0, 0.25*math.pi)
#     cr.set_line_width(3)
#     cr.set_source_rgb(1.0, 0.0, 0.0)
#     cr.stroke()
#     
#     # write output
#     surface.write_to_png("/u/leongs/test_imagedraw/circle.png")

st = "ATOM    181  P     U A   6       3.292   7.472   9.528  1.00  0.00              "