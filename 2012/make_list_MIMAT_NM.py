import os

scanseed_dir = '/u/major/public_html/smartRNAs/HP_1mm/refseq-5pCDS3p-hsa'
list_scanseed_output = os.listdir(scanseed_dir)
list_scanseed_output.sort()


list_NM = set()
list_MIMAT = set()

for scan_out in list_scanseed_output:
    scanseed_out_path = os.path.join(scanseed_dir, scan_out)
    print 'reading ' + scanseed_out_path
    with open(scanseed_out_path, 'rb') as sc_out:
#        print 'now parsing'
        for line in sc_out:
            if line.strip():
                pass
                splitted = line.split()
#                print splitted[1]
#                print splitted[2]
                list_MIMAT.add(splitted[1])
                list_NM.add(splitted[2])
            else:
                print 'empty'
                
with open('/u/leongs/Desktop/NM_list', 'w') as NM_file:
    NM_file.write('\n'.join(list_NM))
    
with open('/u/leongs/Desktop/MIMAT_list', 'w') as MIMAT_file:
    MIMAT_file.write('\n'.join(list_MIMAT))