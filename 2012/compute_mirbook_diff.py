# -*- coding: UTF-8 -*-

"""
Compute the diff between 2 microtargetomes

Usage: python compute_mirbook_diff.py [options]

Options:
  -h, --help                 show this help
  -a ... , --sample1=...  path for the 1st sample
  -q ... , --qty1=...     path for the 1st sample's gene_qty file
  -b ... , --sample2=...  path for the 2nd sample
  -w ... , --qty2=...     path for the 2nd sample's gene_qty file
  -d ..., --dir=...       path for the output dir
  
Examples:
  compute_mirbook_diff.py --sample1=sample1.results --sample2=sample2.results
"""
import os
import argparse
import csv
import cPickle as pickle

def find_not_same_qty(qty1, qty2):
    """" return a set containing the genes for which the qties are not the same """
    dict_qty1 = dict()
    dict_per1 = dict()
    different_set = set()
    with open(qty1, 'r') as qty1_file:
        for line in qty1_file:
            splitted = line.split()
            if not line.startswith("#") and len(splitted) >= 3:
                dict_qty1[splitted[0]] = round(float(splitted[1]))
                dict_per1[splitted[0]] = int(splitted[2])

    # now compare it to qty2
    set_gene_sample2 = set()
    dict_qty2 = dict()
    dict_per2 = dict()
    with open(qty2, 'r') as qty2_file:
        for line in qty2_file:
            splitted = line.split()
            if not line.startswith("#") and len(splitted) >= 3:
                set_gene_sample2.add(splitted[0])
                qty2 = round(float(splitted[1]))
                dict_qty2[splitted[0]] = qty2
                dict_per2[splitted[0]] = int(splitted[2])
                if qty2 != dict_qty1.get(splitted[0], "False"):
                    different_set.add(splitted[0])

    in1_but_not2= set(set(dict_qty1.keys()) - set_gene_sample2)
    return different_set.union(in1_but_not2)


def extract_results(filepath):
    """ extract the results and put them in a dictionnary """
    dict_sample_results = dict()
    dict_gene_qty = dict()
    dict_gene_per = dict()
    with open(filepath, 'r') as input_f:
        for line in input_f:
            stripped = line.strip()
            if stripped:
                g_acc, g_name, g_qty, g_per, m_acc, m_name, \
                m_qty, m_per, pos, pos_tar, tar_type, prob, num = stripped.split('\t')
                unique_id="{gene_accession}_{mirna_accession}_{pos}".format(gene_accession=g_acc,
                                                                            mirna_accession=m_acc,
                                                                            pos=pos)

                dict_gene_qty[g_acc] = int(round(float(g_qty)))
                dict_gene_per[g_acc] = int(g_per)

                dict_sample_results[unique_id] = dict(gene_accession=g_acc,
                                                      gene_name=g_name,
                                                      gene_qty=round(float(g_qty)),
                                                      gene_percentile=int(g_per),
                                                      mirna_accession=m_acc,
                                                      mirna_name=m_name,
                                                      position=pos,
                                                      position_on_target=pos_tar,
                                                      target_type=tar_type,
                                                      prob=prob,
                                                      number=num)

    return dict_sample_results, dict_gene_qty, dict_gene_per


def make_temp_dict(model_sample):
    """ make a temp dict for the missing sample_item """
    new_dict = dict(gene_accession=model_sample["gene_accession"],
                    gene_name=model_sample["gene_name"],
                    mirna_accession=model_sample["mirna_accession"],
                    mirna_name=model_sample["mirna_name"],
                    position=model_sample["position"],
                    position_on_target=model_sample["position_on_target"],
                    target_type=model_sample["target_type"],
                    prob=model_sample["prob"],
                    number=0)

    return new_dict

if __name__ == '__main__':
 
    parser = argparse.ArgumentParser(description='Parser for compute_mirbook_diff.py')

    parser.add_argument('--sample1', '-a', action="store", dest="sample1")
    parser.add_argument('--qty1', '-q', action="store", dest="qty1", default="")
    parser.add_argument('--sample2', '-b', action="store", dest="sample2")
    parser.add_argument('--qty2', '-w', action="store", dest="qty2", default="")
    parser.add_argument('--dir', '-d', action="store", dest="dir", default="")

    ns = parser.parse_args()
    
    sample1 = ns.sample1
    qty1 = ns.qty1
    sample2 = ns.sample2
    qty2 = ns.qty2
    dir = ns.dir

    if qty1 and qty2:
        set_not_foldchangeable = find_not_same_qty(qty1, qty2)
    else:
        set_not_foldchangeable = set()

    dict_sample1, sample1_qty_dict, sample1_per_dict = extract_results(sample1)
    dict_sample2, sample2_qty_dict, sample2_per_dict = extract_results(sample2)

    list_unique_id = set(dict_sample1.keys())
    list_unique_id.update(dict_sample2.keys())
    print ('#gene_accession\t|gene_name\t|miRNA_accession\t|miRNA_name\t'
           '|pos\t|pos_target\t|target_type\t|prob\t|num1\t'
           '|num2\t|diff')

    fold_change_dict = dict()
    delta_occ_dict = dict()
    dict_gene_acc_name = dict()
    for id in list_unique_id:
        sample1 = dict_sample1.get(id, None)
        sample2 = dict_sample2.get(id, None)

        if not sample1:
            sample1 = make_temp_dict(sample2)
        elif not sample2:
            sample2 = make_temp_dict(sample1)

        gene_accession = sample1["gene_accession"]
        gene_name = sample1["gene_name"]
        mirna_accession = sample1["mirna_accession"]
        mirna_name = sample1["mirna_name"]
        position = sample1["position"]
        position_on_target = sample1["position_on_target"]
        target_type = sample1["target_type"]
        prob = sample1["prob"]
        number1 = sample1["number"]
        number2 = sample2["number"]
        dict_gene_acc_name[gene_accession] = gene_name
        # print in the right format
        diff = int(number2) - int(number1)

        print ('{g_accession}\t{g_name}\t{m_accession}\t{m_name}\t'
               '{pos}\t{pos_target}\t{target_type}\t{prob}\t{num1}\t'
               '{num2}\t{diff}').format(g_accession=gene_accession,
                                        g_name=gene_name,
                                        m_accession=mirna_accession,
                                        m_name=mirna_name,
                                        pos=position,
                                        pos_target=position_on_target,
                                        target_type=target_type,
                                        prob=prob,
                                        num1=number1,
                                        num2=number2,
                                        diff=diff)

        diff_item_key = "{g_acc}_{m_acc}".format(g_acc=gene_accession, m_acc=sample1["mirna_accession"])
        if not diff_item_key in delta_occ_dict:
            delta_occ_dict[diff_item_key] = dict(sum1=0,
                                                 sum2=0,
                                                 gene_accession=gene_accession,
                                                 gene_name=gene_name,
                                                 mirna_accession=sample1["mirna_accession"],
                                                 mirna_name=sample1["mirna_name"])

        delta_occ_dict[diff_item_key]["sum1"] += int(sample1['number'])
        delta_occ_dict[diff_item_key]["sum2"] += int(sample2["number"])

        # do the fold_change
        # compute the ponderation
        if sample1["target_type"].strip() == "3UTR":
            pond = 1.0
        else:
            pond = 0.1

        if not gene_accession in fold_change_dict:
            fold_change_dict[gene_accession] = dict(fc1=0, fc2=0, occ1=0, occ2=0)
        fold_change_dict[gene_accession]["fc1"] += (float(sample1["number"]) * float(sample1["prob"]) * pond)
        fold_change_dict[gene_accession]["fc2"] += (float(sample2["number"]) * float(sample1["prob"]) * pond)
        fold_change_dict[gene_accession]["occ1"] += int(sample1['number'])
        fold_change_dict[gene_accession]["occ2"] += int(sample2["number"])

    # print a list of the not foldable genes
    if set_not_foldchangeable:
        print "#Please beware that the following gene(s) don't have the same quantity in both samples:"
        print "\n".join(set_not_foldchangeable)

    if dir and os.path.exists(dir):
        list_fc = [('#Gene Accession', 'Gene Name', '#Copies Sample1','#Copies Sample2',
                    'Percentile Sample1', 'Percentile Sample2', 'Fold Change',
                    'Occupancy Sample1', 'Occupancy Sample2'), ]

        list_delta_occ = [('#Gene Accession', 'Gene Name', '#Copies Sample1','#Copies Sample2',
                           'Percentile Sample1', 'Percentile Sample2',
                           'miRNA Accession', 'miRNA Name', 'Occupancy Sample1', 'Occupancy Sample2', 'Fold Change'), ]

        dict_gene_info = dict()

        for k, v in delta_occ_dict.iteritems():
            gene_accession = v["gene_accession"]
            gene_name = v["gene_name"]
            if k in set_not_foldchangeable:
                gene_accession += "*"
                gene_name += "*"

            fc1 = fold_change_dict[v["gene_accession"]]['fc1'] + 0.1
            fc2 = fold_change_dict[v["gene_accession"]]['fc2'] + 0.1

            to_be_appended = (gene_accession,gene_name, sample1_qty_dict.get(gene_accession, 0), sample2_qty_dict.get(gene_accession, 0),
                              sample1_per_dict.get(gene_accession, 100), sample2_per_dict.get(gene_accession, 100),
                              v["mirna_accession"], v["mirna_name"], v["sum1"],
                              v["sum2"], fc1/fc2)
            list_delta_occ.append(to_be_appended)

        if set_not_foldchangeable:
            list_delta_occ.append(("*:The gene(s) have different quantities in sample1 and sample2", ))

        for k, v in fold_change_dict.iteritems():
            gene_accession = k
            gene_name = dict_gene_acc_name[k]
            if k in set_not_foldchangeable:
                gene_accession += "*"
                gene_name += "*"

            fc1 = v['fc1'] + 0.1
            fc2 = v['fc2'] + 0.1

            to_be_appended = (gene_accession, gene_name, sample1_qty_dict.get(gene_accession, 0), sample2_qty_dict.get(gene_accession, 0),
                              sample1_per_dict.get(gene_accession, 100), sample2_per_dict.get(gene_accession, 100),
                              fc1/fc2, v['occ1'], v['occ2'])
            list_fc.append(to_be_appended)
        if set_not_foldchangeable:
            list_fc.append(("*:The gene(s) have different quantities in sample1 and sample2", ))

        with open(os.path.join(dir, "fold_change.csv"), 'wb') as csv_file:
            spamWriter = csv.writer(csv_file, dialect='excel')
            for elem in list_fc:
                spamWriter.writerow(elem)

        with open(os.path.join(dir, "delta_occupancy.csv"), 'wb') as csv_file:
            spamWriter = csv.writer(csv_file, dialect='excel')
            for elem in list_delta_occ:
                spamWriter.writerow(elem)