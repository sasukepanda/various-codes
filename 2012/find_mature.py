import os
import time

result_directory = "/u/leongs/Documents/miRNA/fasta"

super_fasta = "/u/leongs/Documents/miRNA/hairpin.fa"
mature_fasta = "/u/leongs/Documents/miRNA/mature.fa"

dict_pre = dict()
dict_p_m = dict()

#list_me = os.listdir(result_directory)
#list_me.sort()
#
#for elem in list_me:
#    with open(os.path.join(result_directory, elem), 'rb') as elem_cont:
#        split_cont = elem_cont.read().split('\n')
#        name = elem.replace('.fasta', '')
##        name = split_cont[0].split()[1].strip()
#        seq = split_cont[1].strip()
#        dict_pre[name]=seq
#        dict_p_m[name] = []

dict_pre = dict()
with open(super_fasta, 'rb') as pre_cont:
    bKeep = True
    head = None
    seq = ''
    for line in pre_cont:
        if head and seq:
            if '>' in line:
                # save them
                splitted = head.split()
                name = splitted[0].replace('>', '')
                mature = splitted[4].strip()
                if not '-' in mature:
                    mature = splitted[5].strip()
                if not mature:
                    print "mature doesn't exists for " + name
                else:
                    dict_pre[name] = dict(sequence=seq, mature=mature)
                
                bKeep = True
                head = None
                seq = ''
                
        if 'hsa' in line:
            bKeep = True
            head = line
        
        elif bKeep:
            seq += line.strip()
        
dict_mat = dict()
with open(mature_fasta, 'rb') as fasta_cont:
    split_cont = fasta_cont.read().split('\n')
    i = 0
    while i < len(split_cont):
        if 'hsa' in split_cont[i]:
            name = split_cont[i].split()[0].replace('>', '').strip()
            
            i += 1
            seq = ''
            while not '>' in split_cont[i]:
                seq += split_cont[i].strip()
                i += 1
                
            dict_mat[name] = seq
            
        else:
            i += 1


dict_p_m = dict()
mat_keys_list = dict_mat.keys()
# pair the pre with mat (for real)
for pre, dict_tmp in dict_pre.iteritems():
    mature = dict_tmp['mature']
    
    list_mat = [elem for elem in mat_keys_list if mature.replace('mir', 'miR') == (elem.replace('-5p', '').replace('-3p', '').replace('hsa-', ''))]
    
    if len(mature.split('-')) > 2 or mature[len(mature)-1].isalpha():
        for i in xrange(1, len(mature)-1):
            list_mat.extend([elem for elem in mat_keys_list if mature[:0-i].replace('mir', 'miR') == (elem.replace('-5p', '').replace('-3p', '').replace('hsa-', ''))])
            list_mat = list(set(list_mat))
            if len(list_mat) > 0 and (len(mature[:0-i].split('-')) <= 2) and mature[:0-i][len(mature[:0-i])-1].isdigit():
                break
    
    if len(list_mat) > 2:
        print pre
        print list_mat
#        if pre == 'hsa-mir-3180-1' or pre == 'hsa-mir-3180-2' or pre == 'hsa-mir-3180-3':
#            list_mat = [elem for elem in list_mat if ('5p' in elem or '3p' in elem)]
#            print pre
#        else:
#            list_mat = [elem for elem in list_mat if not('5p' in elem or '3p' in elem)]
        
            
#    if len(list_mat) < 2 and len([elem for elem in list_mat if ('5p' in elem or '3p' in elem)]):
#        print pre
#        print mature
#        print list_mat
    
    dict_p_m[pre] = list_mat

    
#key_stem = dict_pre.keys()
#key_mat = dict_mat.keys()
#list_parsed = []
#list_not_found = []
#
#for stem in key_stem:
#    tmp_str = stem.replace('mir', 'miR')
#    
#    bFound = False
#    for mat in key_mat:
#        if tmp_str in mat:
#            bFound = True
#            list_parsed.append(key_mat)
#            break
#    
#    if not bFound:
#        list_not_found.append(tmp_str)
#        
#list_not_parsed = [elem for elem in key_mat if (not elem in list_parsed)]
#        
#for stem in list_not_found:
#    bFound = False
#    for i in xrange(1, len(stem)-1):
#        print stem[:0-i]
#        for mat in list_not_parsed:
#            if stem[:0-i] in mat:
#                bFound = True
#                list_parsed.append(key_mat)
#                list_not_parsed = [elem for elem in key_mat if (not elem in list_parsed)]
#                break
#        if bFound:
#            break
#    
#    if not bFound:
#        print 'mature not found for ' + stem
#    time.sleep(1)

#not_found = 0
#
#list_parsed = []
#
#for k_p, v_p in dict_pre.iteritems():
#    tmp_str = k_p.replace('mir', 'miR')
#    bFound = False
#            
#    for k_m, v_m in dict_mat.iteritems():
#        if tmp_str in k_m:
#            if (v_m in v_p):
#                list_parsed.append(k_m)
#                dict_p_m[k_p].append(k_m)
#                dict_p_m[k_p] = list(set(dict_p_m[k_p]))
##                print v_p.replace(v_m, '^')
#            else:
#                not_found += 1
#            bFound = True
#        
#    last = False
#    if len(k_p.split('-')) > 3 or (not k_p[len(k_p)-1].isdigit()):
#        for idx in xrange(1, 4):
#            new_str = tmp_str[:0-idx]
#            
#            for k_m2, v_m2 in dict_mat.iteritems():
#                if new_str in k_m2:
#                    if (v_m2 in v_p):
#                        list_parsed.append(k_m2)
#                        dict_p_m[k_p].append(k_m2)
#                        dict_p_m[k_p] = list(set(dict_p_m[k_p]))
##                        print v_p.replace(v_m2, '^')
#                    else:
#                        not_found += 1
#                    bFound = True
#                    
#            if last or (new_str[len(new_str)-1].isdigit() and len(new_str.split('-')) < 4):
#                break
#            
#            if new_str[len(new_str)-1] == '-':
#                last = True
#    
#    if not bFound:
#        print 'not found for ' + tmp_str

#list_parsed = list(set(list_parsed))

#list_nf = [elem for elem in dict_mat.keys() if (not elem in list_parsed)]
#print len(list_nf)
#print list_nf


#dict_p_m_pos = dict()
#for p, m in dict_p_m.iteritems():
#    tmp_dict = dict()
#    for mat in m:
#        seq_mat = dict_mat[mat]
#        seq_pre = dict_pre[p]
#        
#        pos = 'a'
#        tmp_str = seq_pre.replace(seq_mat, '^')
##        print tmp_str
#        if '-3p' in mat:
#            # find position
#            for i in xrange(1, len(tmp_str)):
#                if tmp_str[0-i] == '^':
#                    pos = 0-i
#        else:
#            for i in xrange(len(tmp_str)):
#                if tmp_str[i] == '^':
#                    pos = i+1
#            
#            if pos > len(tmp_str)/2:
#                pos = pos - len(tmp_str) -1
#
#                    
#        if pos != 'a':
#            tmp_dict[mat] = pos
#
#    dict_p_m_pos[p] = tmp_dict
#    
## compute the overlap
#mcfold_dir = '/u/leongs/Documents/miRNA/result_mcfold'
#for pre, dict_pos in dict_p_m_pos.iteritems():
#    if len(dict_pos) > 1:
#        if len(dict_pos) > 2:
#            print pre
#            print dict_pos
#        mcfold_filepath = os.path.join(mcfold_dir, pre + '.mcfold')
#        
#        with open(mcfold_filepath, 'r') as mcfold_cont:
#            splitted_f = mcfold_cont.read().split('\n')
#            name = splitted_f[0].replace('>', '').replace(' ', '_')
#            seq = splitted_f[1]
#            struct = splitted_f[2]
#            
#            list_min_max = []
#            for k, v in dict_pos.iteritems():
#                if v < 0:
#                    start = v+1 - len(dict_mat[k])
#                    end = v+1
#                    
#                else:
#                    start = v-1
#                    end = v-1 + len(dict_mat[k])
#                    
#                list_min_max.append('{start}-{end}'.format(start=start, end=end))
#                
#            # find all the pairs in the structure
#            list_pair = []
#            list_parsed = []
#            
#            for idx, elem in enumerate(struct):
#                if elem == ')':
#                    # backtrack to find its partner
#                    for back in reversed(xrange(idx)):
#                        if struct[back] == '(' and (not back in list_parsed):
#                            list_parsed.append(back)
#                            list_parsed.append(idx)
#                            list_pair.append('{back}-{idx}'.format(back=back, idx=idx))
#                            break
#            
#            # sort the pairs and the min_max list
#            list_pair.sort(key=lambda elem: int((elem.split('-'))[0]))
#            list_min_max.sort(key=lambda elem: int((elem.split('-'))[0]))
            
            
#            if '(' in struct:
#                number = struct.count('(')
#                print '{st}\t{num}'.format(st=name, num=number)
#            else:
#                print name