import os
import subprocess
import shlex

def call_command(command, pipe=None, echo = False):
    if echo:
        print command
    
    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    
    output=process.communicate(input=pipe)
    return output


folder_list = [elem for elem in os.listdir('/u/leongs/Desktop/workdir_temp/') if (os.path.isdir(os.path.join('/u/leongs/Desktop/workdir_temp/', elem)))]

folder_list.sort()
folder_content_list = []
result = 0
print len(folder_list) - 1
for i in xrange(len(folder_list) - 1):
    l1 = [el for el in os.listdir(os.path.join('/u/leongs/Desktop/workdir_temp/', folder_list[i])) if 'pdb.gz' in el]
    for j in xrange(i+1, len(folder_list)):
        l2 = [el for el in os.listdir(os.path.join('/u/leongs/Desktop/workdir_temp/', folder_list[j])) if 'pdb.gz' in el]
        if len(l1) == len(l2):
            l1.sort()
            l2.sort()
            for fichier in l1:
                if fichier in l2:
                    call_command('gunzip ' + os.path.join(os.path.join('/u/leongs/Desktop/workdir_temp/', folder_list[i]), fichier))
                    call_command('gunzip ' + os.path.join(os.path.join('/u/leongs/Desktop/workdir_temp/', folder_list[j]), fichier))
                    print 'comparing {0} with {1}'.format(os.path.join(folder_list[i], fichier), os.path.join(folder_list[j], fichier))
                    file_a = open(os.path.join(os.path.join('/u/leongs/Desktop/workdir_temp/', folder_list[i]), fichier.replace('.pdb.gz', '.pdb')), 'r')
                    file_b = open(os.path.join(os.path.join('/u/leongs/Desktop/workdir_temp/', folder_list[j]), fichier.replace('.pdb.gz', '.pdb')), 'r')
            
                    text_a = file_a.read()
                    text_b = file_b.read()

                    if text_a == text_b:
                        pass
                    else:
                        print 'not same'
                        result = 1
            
                    call_command('gzip ' + os.path.join(os.path.join('/u/leongs/Desktop/workdir_temp/', folder_list[i]), fichier.replace('.pdb.gz', '.pdb')))
                    call_command('gzip ' + os.path.join(os.path.join('/u/leongs/Desktop/workdir_temp/', folder_list[j]), fichier.replace('.pdb.gz', '.pdb')))
                else:
                    print 'not same'
                    result = 1

print result
