import cairo as C
import os
from collections import defaultdict

nm_length = 5572
nm_acc = 'NM_000314'

stable_file = '/u/leongs/Desktop/StableMarriage/new_results/DU145/results/DU145_wt'

dict_mir = dict()
dict_mir = defaultdict(lambda: [], dict_mir)
with open(stable_file, 'r') as stf:
    for line in stf:
        if nm_acc in line:
            splitted = line.strip().split()
            if len(splitted) > 3:
                mir_acc = splitted[2]
                dict_mir[mir_acc].append(dict(position=splitted[4],
                                              quantity=splitted[6]))

width, height = nm_length+5, nm_length+5
output = "test.pdf"

surf = C.PDFSurface(output,width,height)
ctx = C.Context(surf)

# fill everyting with white
ctx.new_path()
ctx.set_source_rgb(0.9,0.9,0.9)
ctx.rectangle(0,0,width,height)
ctx.fill()  # fill current path

# draw the gene
ctx.new_path()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(width/4, height/4, width/2, height/2)
ctx.stroke()


## display text in the center
#ctx.set_source_rgb(0,0,0)  # black
#txt = "Hello, Stack Overflow!"
#ctx.select_font_face("Ubuntu", C.FONT_SLANT_NORMAL, C.FONT_WEIGHT_NORMAL)
#ctx.set_font_size(18)
#x_off, y_off, tw, th = ctx.text_extents(txt)[:4]
#ctx.move_to(width/2-x_off-tw/2,height/2-y_off-th/2)
#ctx.show_text(txt)
#
## draw a circle in the center
#ctx.new_path()
#ctx.set_source_rgb(0,0.2,0.8)  # blue
#ctx.arc(width/2,height/2,tw*0.6,0,2*pi)
#ctx.stroke()  # stroke current path

# save to PNG
surf.write_to_png(output)