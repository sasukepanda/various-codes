from multiprocessing import Process
import time

def work():
    rang = range(50000000)
    #print (rang)
    list = []
    for elem in rang:
        list.append(elem)
    print ('job finished at {0}'.format(time.time(), ))

def submit():
    p = Process(target=work, args=())
    print ('process start at {0}'.format(time.time(), ))
    p.start()
    #p.join()
    return 'submit finished at {0}\n'.format(time.time(), )
    
if __name__ == '__main__':
    print 'opening file at at {0}'.format(time.time(), )
    logfile = open('/u/leongs/Desktop/file.log', 'w')
    print 'submit 1st work at {0}'.format(time.time(), )
    logfile.write(submit())
    print 'submit 2nd work at {0}'.format(time.time(), )
    logfile.write(submit())