import os
import sys

def parse_seq(current_seq, complete_seq, i):
    if i < len(complete_seq):
        if complete_seq[i] == 'R' or complete_seq[i] == 'W' or complete_seq[i] == 'M' \
                                  or complete_seq[i] == 'D' or complete_seq[i] == 'H' \
                                  or complete_seq[i] == 'V' or complete_seq[i] == 'A' \
                                  or complete_seq[i] == 'N':
            parse_seq(current_seq + 'A', complete_seq, i+1)
            
        if complete_seq[i] == 'Y' or complete_seq[i] == 'W' or complete_seq[i] == 'K' \
                                  or complete_seq[i] == 'B' or complete_seq[i] == 'D' \
                                  or complete_seq[i] == 'H' or complete_seq[i] == 'U' \
                                  or complete_seq[i] == 'N':
            parse_seq(current_seq + 'U', complete_seq, i+1)
            
        if complete_seq[i] == 'S' or complete_seq[i] == 'R' or complete_seq[i] == 'K' \
                                  or complete_seq[i] == 'B' or complete_seq[i] == 'D' \
                                  or complete_seq[i] == 'V' or complete_seq[i] == 'G' \
                                  or complete_seq[i] == 'N':
            parse_seq(current_seq + 'G', complete_seq, i+1)
            
        if complete_seq[i] == 'Y' or complete_seq[i] == 'S' or complete_seq[i] == 'M' \
                                  or complete_seq[i] == 'B' or complete_seq[i] == 'H' \
                                  or complete_seq[i] == 'V' or complete_seq[i] == 'C' \
                                  or complete_seq[i] == 'N':
            parse_seq(current_seq + 'C', complete_seq, i+1)
    
    else:
        print current_seq

if __name__ == "__main__":
    seq = sys.argv[1].upper().replace('T', 'U')
    parse_seq('', seq, 0)
    
    