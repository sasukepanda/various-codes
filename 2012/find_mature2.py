import os

data_file = "/u/leongs/Documents/miRNA/miRNA.dat"
pre_fa_file = '/u/leongs/Documents/miRNA/hairpin.fa'
mat_fa_file = '/u/leongs/Documents/miRNA/mature.fa'
mcfold_result_folder = '/u/leongs/Documents/miRNA/result_mcfold_with_mask'

list_data = []
with open(data_file, 'rb') as data_cont:
    splitted = data_cont.read().split('//')
    precursor_list = [elem for elem in splitted if ('Homo sapiens' in elem)]
    
    for elem in precursor_list:
        # split by sections
        sub_section_list = elem.split('XX')
        
        pre_id = ''
        accession = ''
        # list of dictionnaries
        list_mat = []
        for sub_section in sub_section_list:
            # get the precursor ID, '\n' is needed here
            if sub_section.startswith('\nID'):
                sub_splitted = sub_section.split()
                pre_id = sub_splitted[1].strip()
            
            # get the precursor accession
            elif sub_section.startswith('\nAC'):
                sub_splitted = sub_section.split()
                accession = sub_splitted[1].strip().replace(';','')
                
            # get the matures
            elif sub_section.startswith('\nFH'):
                # split the matures
                mat_splitted = sub_section.split('\nFT   miRNA           ')
                
                for mature in mat_splitted:
                    if mature.startswith('\nFH'):
                        continue
                    else:
                        # split line by line
                        split_mat = mature.split('\n')
                        dict_mat = dict()
                        for mat in split_mat:
                            line = mat.strip()
                            if not line:
                                continue
                            elif not line.startswith('FT'):
                                # get the begin and end position
                                pos = line.split('..')
                                dict_mat['begin'] = int(pos[0])
                                dict_mat['end'] = int(pos[1])
                            
                            else:
                                if 'accession' in line:
                                    # get accession ID
                                    tmp_split = line.split('"')
                                    dict_mat['accession'] = tmp_split[1]
                                elif 'product' in line:
                                    # get name 
                                    tmp_split = line.split('"')
                                    dict_mat['name'] = tmp_split[1]
                        # add the mature in the list
                        list_mat.append(dict_mat)
        
        # we got all the needed data for one precursor here
        list_data.append(dict(name=pre_id, accession=accession, list_mature=list_mat))

# save the precursor's sequences    
dict_pre = dict()   
with open(pre_fa_file, 'rb') as pre_cont:
    list_hsa = [elem for elem in pre_cont.read().split('>') if 'hsa' in elem]
    for hsa in list_hsa:
        name = ''
        seq = ''
        splitted_lines = hsa.split('\n')
        for line in splitted_lines:
            if 'hsa' in line:
                name = line.split()[0].strip()
            else:
                seq += line.strip()
        
        dict_pre[name] = seq

# save the mature's sequence
dict_mat = dict()
with open(mat_fa_file, 'rb') as fasta_cont:
    list_hsa = [elem for elem in fasta_cont.read().split('>') if 'hsa' in elem]
    for hsa in list_hsa:
        name = ''
        seq = ''
        splitted_lines = hsa.split('\n')
        for line in splitted_lines:
            if 'hsa' in line:
                name = line.split()[0].strip()
            else:
                seq += line.strip()
        
        dict_mat[name] = seq


# parse the MCFOLD structures to find the overlap
list_mcfold = os.listdir(mcfold_result_folder)
list_overlap = []
for res in list_mcfold:
    result_file = os.path.join(mcfold_result_folder, res)
    with open(result_file, 'rb') as mcfold_cont:
        # parse the result content to get infos we want
        splitted_res = mcfold_cont.read().strip().split('\n')
        name = splitted_res[0].split()[0].replace('>', '').strip()
        struct = splitted_res[2].split()[0].strip()

        # find the pairs and put them in a dict
        dict_pairs = dict()
        
        list_done = []
        for idx, elem in enumerate(struct):
            if elem == ')':
                for rev in reversed(xrange(idx)):
                    if struct[rev] == '(' and (not rev in list_done):
                        dict_pairs[idx] = rev
                        list_done.append(rev)
                        break
        
        structure_l = []
        for elem in struct:
            structure_l.append(' ')
            
        for k, v in dict_pairs.iteritems():
            structure_l[k] = v
            structure_l[v] = k
            
        left = 0
        right = len(struct) -1
        
        struct_left = ''
        struct_right = ''
        
        while True:
            if right - left < 0:
                break
            
            elif structure_l[left] == ' ' and structure_l[right] != ' ' or right == left:
                struct_left += struct[left]
                left += 1
                
            elif structure_l[left] != ' ' and structure_l[right] == ' ':
                struct_right += struct[right]
                right -= 1
            else:
                struct_left += struct[left]
                struct_right += struct[right]
                left += 1
                right -= 1         
                
#        if struct != struct_left + struct_right[::-1]:
#            print struct_left
#            print struct_right
#            print struct
#            print struct_left + struct_right[::-1]
#            print 'not same'
#            print '#############'
                
        open_start = -1
        open_end = -1
        close_start = -1
        close_end = -1
        
        for idx, elem in enumerate(struct):
            if elem == '(':
                if open_start < 0:
                    open_start = idx+1
                if idx < len(struct_left):
                    open_end = idx+1
            
            elif elem == ')':
                close_end = idx+1
                if close_start < 0 and idx >= len(struct_left):
                    close_start = idx+1
                    
#        print open_start
#        print open_end
#        print close_start
#        print close_end
#        print '#################'
        for data in list_data:
            if data['name'] == name:
                list_mature = data['list_mature']
                if len(list_mature) > 1:
                    mature_before = None
                    mature_after = None
                    for mat in list_mature:
                        if (mat['begin'] > open_end or mat['end'] > close_start) and mat['begin'] < close_end:
                            mature_after = mat
                        elif mat['begin'] < open_end:
                            mature_before = mat
                            
#                    if not (mature_before and mature_after):
#                        print '########################################################'
#                        print name
#                        print mature_before
#                        print mature_after
#                        print open_start
#                        print open_end
#                        print close_start
#                        print close_end
#                        print struct
#                        print list_mature

                    overlap = 0
                    min_mature_before = max([mature_before['begin'], open_start])
                    max_mature_before = min([mature_before['end'], open_end])
                    min_mature_after = max([mature_after['begin'], close_start])
                    max_mature_after = min([mature_after['end'], close_end])
                    
                    
                    left = len(struct_left)
                    right = len(struct_left) +1
                    while True:
                        if left < 1 or right > len(struct):
                            break
                        else:
                            if (min_mature_before <= left <= max_mature_before) and (min_mature_after <= right <= max_mature_after):
                                overlap += 1
                            left -= 1
                            right += 1
                    
                    list_overlap.append(overlap)
#                    print min_mature_before
#                    print max_mature_before
#                    print min_mature_after
#                    print max_mature_after
#                    print struct_left
#                    print struct_right
#                    print list_mature
#                    print '###########################'

#
#print len(list_overlap)
#print len([elem for elem in list_overlap if 18 < elem < 26])

#count = 0
#for data in list_data:
#    if len(data['list_mature']) <= 2:
#        print 'found'
#        count += 1
#
#print count

total = 0 
for elem in list_overlap:
    print elem
    total += elem

print total/len(list_overlap)