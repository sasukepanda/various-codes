import os
import sys

def parse_seq(current_seq, complete_seq, i, name):
    if i < len(complete_seq):
        if complete_seq[i] == 'R' or complete_seq[i] == 'W' or complete_seq[i] == 'M' \
                                  or complete_seq[i] == 'D' or complete_seq[i] == 'H' \
                                  or complete_seq[i] == 'V' or complete_seq[i] == 'A' \
                                  or complete_seq[i] == 'N':
            parse_seq(current_seq + 'A', complete_seq, i+1, name)
            
        if complete_seq[i] == 'Y' or complete_seq[i] == 'W' or complete_seq[i] == 'K' \
                                  or complete_seq[i] == 'B' or complete_seq[i] == 'D' \
                                  or complete_seq[i] == 'H' or complete_seq[i] == 'U' \
                                  or complete_seq[i] == 'N':
            parse_seq(current_seq + 'U', complete_seq, i+1, name)
            
        if complete_seq[i] == 'S' or complete_seq[i] == 'R' or complete_seq[i] == 'K' \
                                  or complete_seq[i] == 'B' or complete_seq[i] == 'D' \
                                  or complete_seq[i] == 'V' or complete_seq[i] == 'G' \
                                  or complete_seq[i] == 'N':
            parse_seq(current_seq + 'G', complete_seq, i+1, name)
            
        if complete_seq[i] == 'Y' or complete_seq[i] == 'S' or complete_seq[i] == 'M' \
                                  or complete_seq[i] == 'B' or complete_seq[i] == 'H' \
                                  or complete_seq[i] == 'V' or complete_seq[i] == 'C' \
                                  or complete_seq[i] == 'N':
            parse_seq(current_seq + 'C', complete_seq, i+1, name)
    
    else:
        print 'mcfold -s {seq} -n "{name}" -t 5;'.format(seq=current_seq,
                                                         name=name)

if __name__ == "__main__":
    input_file = sys.argv[1]
    with open(input_file, 'r') as input_cont:
        list_line = input_cont.read().splitlines()
        index = 0
        
        while True:
            if index >= len(list_line)-1:
                break
            
            if '>' in list_line[index]:
                name = list_line[index].replace('>', '')
                seq = list_line[index+1].replace('T', 'U')
                
                parse_seq('', seq, 0, name)
                
            index += 1