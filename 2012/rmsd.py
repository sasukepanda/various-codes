""" Usage python rmsd.py --decoy_folder --reference [options]

 Author:   Stephen Leong Koan
           RNA Engineering Laboratory
           Institute for Research in Immunology and Cancer
           Universite de Montreal

 Version 1.0 - 4 September 2012"""
 
import os
import sys
import subprocess
import shlex
import shutil
import argparse

def call_command(command, pipe=None, echo = False):
    if echo:
        print command
    
    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    
    output=process.communicate(input=pipe)
    
    if output[1] and not 'Min RMSD' in output[1]:
        raise Exception(output[1].strip())
    
    return output

######## Script starts here ##########
if __name__ == '__main__':
 
    parser = argparse.ArgumentParser(description='Parser for rmsd.py')

    parser.add_argument('--decoy_folder', '-d', action="store", dest="decoy_folder", help='Path to the decoy folder (with the pdb.gz files)')
    parser.add_argument('--reference', '-r', action="store", dest="reference", help='Path to the reference file to be used for computing the RMSD (.pdb file)')
    parser.add_argument('--numbering', '-n', action="store", dest="numbering", default="absolute", help='Nucleotides numbering. Can be either "absolute"(default) or "relative"')
    parser.add_argument('--atoms', '-a', action="store", dest="atoms", default="all", help='Atoms to align. Can be "all"(default), "nucleobase", "backbone" or "phosphate"')
    parser.add_argument('--extraalign', '-A', action="store", dest="extraalign", default=None, help="Nucleotides to align: (default is all. Example: A1-A10, A22, A25)")
    parser.add_argument('--extrarmsd', '-R', action="store", dest="extrarmsd", default=None, help='Nucleotides in RMSD: (default is all. Example: A1-A10, A22, A25)')
    parser.add_argument('--savermsd', '-s', action="store_true", default=False, help='Save alignment in file? (default=False)')
    parser.add_argument('--colorrmsd', '-c', action="store_true", default=False, help="Color alignment? (in PDB's temperature factor field) (default=False)")
    parser.add_argument('--colormin', '-m', action="store", dest="colormin", default=None, help="From blue (min RMSD in Angstroms)", type=float)
    parser.add_argument('--colormax', '-M', action="store", dest="colormax", default=None, help="To red (max RMSD in Angstroms)", type=float)
    
    ns = parser.parse_args()
    
    decoy_folder = ns.decoy_folder
    reference = ns.reference
    numbering = ns.numbering
    atoms = ns.atoms
    extraalign = ns.extraalign
    extrarmsd = ns.extrarmsd
    colorrmsd = ns.colorrmsd
    colormin = ns.colormin
    colormax = ns.colormax
    savermsd = ns.savermsd
    exec_folder = os.path.dirname(sys.argv[0])
    
    strOptions = "n"
    
    if 'relative' in numbering:
        strOptions += "e"
    
    if 'backbone' in atoms:
        strOptions += "B"
    elif 'nucleobase' in atoms:
        strOptions += "b"
    elif 'phosphate' in atoms:
        strOptions += " -A P -R P"
    
    if extraalign:
        strSecured = ""
        for letter in extraalign:
            if letter.isupper() or letter.isdigit() or "-" in letter or "," in letter or letter.isspace():
                strSecured += letter

        strOptions += " -a " + strSecured
        

    if extrarmsd:
        strSecured = ""
        for letter in extrarmsd:
            if letter.isupper() or letter.isdigit() or "-" in letter or "," in letter or letter.isspace():
                strSecured += letter

        strOptions += " -r " + strSecured

    rmsd_list = []
    pdb_list = [elem for elem in os.listdir(decoy_folder) \
                if ((elem.endswith('pdb.gz')) and reference!=os.path.join(decoy_folder, elem))]
    pdb_list.sort()

    for idx, pdb in enumerate(pdb_list):
        unzipped = pdb.replace('pdb.gz', 'pdb')
        
        if savermsd:
            out, err = call_command('{MCRMSD} -{strOptions} {reference_file} {pdb} -o {aligned}'.format(MCRMSD=os.path.join(exec_folder, 'MC-RMSD'),
                                                                                                                 strOptions=strOptions,
                                                                                                                 reference_file=reference,
                                                                                                                 pdb=os.path.join(decoy_folder, pdb),
                                                                                                                 aligned=os.path.join(decoy_folder, 'aligned.'+unzipped)))
            line_list = out.split('\n')
            for line in line_list:
                if 'Angstroms' in line:
                    print line
        
            if colorrmsd:
                cmd_line = '{colorRMSD_exec} {aligned}'.format(colorRMSD_exec=os.path.join(exec_folder, 'colorRMSD.exe'), 
                                                               aligned=os.path.join(decoy_folder, 'aligned.'+unzipped))
        
                if colormin and colormax:
                    cmd_line += ' {colormin} {colormax}'.format(colormin=colormin, colormax=colormax)
                    
                out, err = call_command(cmd_line)
                
                with open(os.path.join(decoy_folder, 'aligned.' + unzipped), 'w') as new_pdb:
                    new_pdb.write(out)
            
                call_command('gzip ' + os.path.join(decoy_folder, 'aligned.' + unzipped))
    
        else:
            out, err = call_command('{MCRMSD} -{strOptions} {reference_file} {pdb}'.format(MCRMSD=os.path.join(exec_folder, 'MC-RMSD'),
                                                                                           strOptions=strOptions,
                                                                                           reference_file=reference,
                                                                                           pdb=os.path.join(decoy_folder, pdb)))
        
            line_list = out.split('\n')
            for line in line_list:
                if 'Angstroms' in line:
                    print line