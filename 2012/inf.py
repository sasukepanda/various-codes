""" Usage python inf.py Decoy_Folder Reference
    - Decoy_Folder is a path to the folder containing the pdb.gz files.
    - Reference is a .pdb/.pdb.gz file used as a reference to compute the Interaction Network Fidelity (INF).

 Author:   Stephen Leong Koan
           RNA Engineering Laboratory
           Institute for Research in Immunology and Cancer
           Universite de Montreal

 Version 1.0 - 4 September 2012"""

import os
import sys
import subprocess
import shlex
import shutil

def call_command(command, pipe=None, echo = False):
    if echo:
        print command
    
    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    
    output=process.communicate(input=pipe)
    
    if output[1] and not 'JOB finished!' in output[1]:
        raise Exception(output[1].strip())
    
    return output


if __name__ == "__main__":
    if len(sys.argv) != 3 or sys.argv[1] == '-h'or sys.argv[1] == '--help':
        print "Usage python inf.py Decoy_Folder Reference\n   - Decoy_Folder is a path to the folder containing the pdb.gz files.\n   - Reference is a .pdb/.pdb.gz file used as a reference to compute the Interaction Network Fidelity (INF)."
    else:
        exec_folder = os.path.dirname(sys.argv[0])
        decoy_folder = sys.argv[1]
        reference = sys.argv[2]
        
        # add the environment variables
        os.environ['RNAVIEW'] = os.path.join(exec_folder, 'RNAVIEW')
        
        # annotate the reference file
        out, err = call_command('{mcannotate} {ref}'.format(mcannotate=os.path.join(exec_folder, 'MC-Annotate'),
                                                            ref=reference))
        
        with open(os.path.join(decoy_folder, 'tmp.annot'), 'w') as annot_file:
            annot_file.write(out)
        
        call_command('{rnaview} {ref}'.format(rnaview=os.path.join(exec_folder, 'RNAVIEW', 'bin', 'rnaview'),
                                              ref=reference))

        out, err = call_command('{inject_rnaview} {ref}.out {tmp_annot}'.format(inject_rnaview=os.path.join(exec_folder, 
                                                                                                            'inject_rnaview.exe'),
                                                                                ref=reference,
                                                                                tmp_annot=os.path.join(decoy_folder, 'tmp.annot')))
        
        with open(os.path.join(decoy_folder, 'reference.annot'), 'w') as reference_annot:
            reference_annot.write(out)
            
        inf_list = []
        # get list of pdb that are not the reference in the decoy
        pdb_list = [elem for elem in os.listdir(decoy_folder) \
                    if ((elem.endswith('pdb.gz')) and reference!=os.path.join(decoy_folder, elem))]
        pdb_list.sort()
        
        for idx, pdb in enumerate(pdb_list):
            curr_pdb = os.path.join(decoy_folder, pdb)
            call_command('gunzip ' + curr_pdb)
            unzipped = curr_pdb.replace('pdb.gz', 'pdb')
            
            # run mcannotate
            out, err = call_command('{mcannotate} {unzipped}'.format(mcannotate=os.path.join(exec_folder, 'MC-Annotate'),
                                                                     unzipped=unzipped))
            
            with open(os.path.join(decoy_folder, 'tmp.annot'), 'w') as annot_file:
                annot_file.write(out)
            
            call_command('{rnaview} {unzipped}'.format(rnaview=os.path.join(exec_folder, 'RNAVIEW', 'bin', 'rnaview'), 
                                                       unzipped=unzipped))
            
            out, err = call_command('{inject_rnaview} {unzipped}.out {tmp_annot}'.format(inject_rnaview=os.path.join(exec_folder, 
                                                                                                                     'inject_rnaview.exe'), 
                                                                                         unzipped=unzipped,
                                                                                         tmp_annot=os.path.join(decoy_folder, 'tmp.annot')))
            
            with open(os.path.join(unzipped + '.annot'), 'w') as unzipped_annot:
                unzipped_annot.write(out)
                
            out, err = call_command('{rna_matthews} {reference_annot} {unzipped}.annot'.format(rna_matthews=os.path.join(exec_folder, 
                                                                                                                       'rna_matthews.exe'), 
                                                                                             unzipped=unzipped,
                                                                                             reference_annot=os.path.join(decoy_folder, 'reference.annot')))
            
            print out.strip()
            
            call_command('gzip ' + unzipped)
            try:
                os.remove(unzipped + '.out')
                os.remove(unzipped + '.annot')
            except Exception as e:
                pass    
            