import os

input_folder = "/u/leongs/Desktop/workdir_temp/1d145745-561a-44e5-baa5-ddb9fe2dc5f9"

with open(os.path.join(input_folder, 'mccons.script.txt'), 'rb') as mccons_input:
    list_seq = [elem for elem in mccons_input.read().split('>') if elem.strip()]
    
    print list_seq[0]
    list_line = ('>' + list_seq[1]).splitlines()
    
    with open("/u/leongs/Downloads/tRNA_ARG.out.txt", 'r') as mcfold_out:
        mcfold_cont = [elem for elem in mcfold_out.read().splitlines() if not 'BP' in elem]
        
        if len(list_line) != len(mcfold_cont):
            print 'not same'
        else:
            for i, l in enumerate(list_line):
                if l != mcfold_cont[i]:
                    print l
                    print mcfold_cont[i]
                    print 'not same in input'