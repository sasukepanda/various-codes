# -*- coding: UTF-8 -*-

"""
K-clustering for PDB Files

Clusterize PDB files

Usage: python kclus.py [options]

Options:
  -h, --help                 show this help
  -c ... , --nbclusters=...  clusterize pdf files in specified number of clusters
  -r ... , --nbrestarts=...
  -m ... , --nbmodels=...
  -s, --specialP
  
Examples:
  kclus.py --nbclusters=5 --nbrestarts=3 --nbmodels=4 --specialP
"""

import os
import subprocess
import shlex
import sys
import argparse
import random
import math
import shutil
from datetime import datetime

SPECIAL_PHOSPHATE = False
STRUCTURE_LIST = None
MAPINDEX2FILE = None
WORKING_DIRECTORY = None

class Point3D(object):
    """ point3D object """
    x = 0
    y = 0
    z = 0
    
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z
        
    def get_x(self):
        return self.x
    
    def get_y(self):
        return self.y
    
    def get_z(self):
        return self.z
    
    def get_coordinates(self):
        return dict(x=self.x, y=self.y, z=self.z)
    
    def set_x(self, x):
        self.x = x
    
    def set_y(self, y):
        self.y = y
    
    def set_z(self, z):
        self.z = z
        
    def set_point(self, x=None, y=None, z=None):
        if x:
            self.x = x
        if y:
            self.y = y
        if z:
            self.z = z
        
    def __repr__(self):
        """ returns a string representing the 3D point """
        return '3D point object : x={0} ; y={1} ; z={2}'.format(self.x, self.y, self.z)
    
    
################## Align Atoms ##############

def multiply_4x4(ma, mb):
    """ Calculate the product of AB as two four by four matrices """
    temp = [[0, 0, 0, 0], 
            [0, 0, 0, 0], 
            [0, 0, 0, 0], 
            [0, 0, 0, 0]]
        
    temp[0][0] = (ma[0][0] * mb[0][0]) + (ma[1][0] * mb[0][1]) + (ma[2][0] * mb[0][2]) + (ma[3][0] * mb[0][3])
    temp[0][1] = (ma[0][1] * mb[0][0]) + (ma[1][1] * mb[0][1]) + (ma[2][1] * mb[0][2]) + (ma[3][1] * mb[0][3])    
    temp[0][2] = (ma[0][2] * mb[0][0]) + (ma[1][2] * mb[0][1]) + (ma[2][2] * mb[0][2]) + (ma[3][2] * mb[0][3])
    temp[0][3] = (ma[0][3] * mb[0][0]) + (ma[1][3] * mb[0][1]) + (ma[2][3] * mb[0][2]) + (ma[3][3] * mb[0][3])
    
    temp[1][0] = (ma[0][0] * mb[1][0]) + (ma[1][0] * mb[1][1]) + (ma[2][0] * mb[1][2]) + (ma[3][0] * mb[1][3])
    temp[1][1] = (ma[0][1] * mb[1][0]) + (ma[1][1] * mb[1][1]) + (ma[2][1] * mb[1][2]) + (ma[3][1] * mb[1][3])    
    temp[1][2] = (ma[0][2] * mb[1][0]) + (ma[1][2] * mb[1][1]) + (ma[2][2] * mb[1][2]) + (ma[3][2] * mb[1][3])
    temp[1][3] = (ma[0][3] * mb[1][0]) + (ma[1][3] * mb[1][1]) + (ma[2][3] * mb[1][2]) + (ma[3][3] * mb[1][3])
    
    temp[2][0] = (ma[0][0] * mb[2][0]) + (ma[1][0] * mb[2][1]) + (ma[2][0] * mb[2][2]) + (ma[3][0] * mb[2][3])
    temp[2][1] = (ma[0][1] * mb[2][0]) + (ma[1][1] * mb[2][1]) + (ma[2][1] * mb[2][2]) + (ma[3][1] * mb[2][3])    
    temp[2][2] = (ma[0][2] * mb[2][0]) + (ma[1][2] * mb[2][1]) + (ma[2][2] * mb[2][2]) + (ma[3][2] * mb[2][3])
    temp[2][3] = (ma[0][3] * mb[2][0]) + (ma[1][3] * mb[2][1]) + (ma[2][3] * mb[2][2]) + (ma[3][3] * mb[2][3])
    
    temp[3][0] = (ma[0][0] * mb[3][0]) + (ma[1][0] * mb[3][1]) + (ma[2][0] * mb[3][2]) + (ma[3][0] * mb[3][3])
    temp[3][1] = (ma[0][1] * mb[3][0]) + (ma[1][1] * mb[3][1]) + (ma[2][1] * mb[3][2]) + (ma[3][1] * mb[3][3])    
    temp[3][2] = (ma[0][2] * mb[3][0]) + (ma[1][2] * mb[3][1]) + (ma[2][2] * mb[3][2]) + (ma[3][2] * mb[3][3])
    temp[3][3] = (ma[0][3] * mb[3][0]) + (ma[1][3] * mb[3][1]) + (ma[2][3] * mb[3][2]) + (ma[3][3] * mb[3][3])
    
    return temp

def cofactor(matrix, size, row, col, co_factor):
    """ Return the cofactor of a matrix by removal of a single col and row """
    k = 0
    l = 0
    index = -1
    for i in xrange(size):
        for j in xrange(size):
            index = index + 1
            if((index % size) != col) and (index < (row * size) or index >= ((row+1) * size)):
                k = int(l/len(co_factor[k]))
                co_factor[k][l%(len(co_factor[k]))] = matrix[i][j]
                l = l+1
                
                # alternative way
#                co_factor[k][l] = matrix[i][j]
#                l = l+1
#                if l >= len(co_factor[k]):
#                    l = 0
#                    k = k+1
            

            
def determan_2x2(deter):
    """ Calculate the value of a 2 by 2 determanant """
    return (deter[0][0] * deter[1][1] - deter[1][0] * deter[0][1])
            
def determan_3x3(deter):
    """ Calculate the value of a 3 by 3 determanant """
    sign = (1.0, -1.0, 1.0)
    
    value = 0.0 # partial sum
    
    twoby = [[0.0, 0.0], 
             [0.0, 0.0]] # store temp 2x2 determanant
        
    for i in xrange(3):
        k = 0
        for j in xrange(3):
            if j != i:
                twoby[k][0] = deter[j][1]
                twoby[k][1] = deter[j][2]
                k = k+1
                
        value = value + (sign[i] * deter[i][0] * determan_2x2(twoby))
        
    return value
    

def determan_4x4(deter):
    """ Calculate the value of a 4 by 4 determanant """
    sign = (1.0, -1.0, 1.0, -1.0)
    
    value = 0.0 # partial sum
    
    threeby = [[0.0, 0.0, 0.0], 
               [0.0, 0.0, 0.0], 
               [0.0, 0.0, 0.0]] # store temp 3x3 determanant
    
    for i in xrange(4):
        cofactor(deter, 4, i, 0, threeby)
        value = value + (sign[i] * deter[i][0] * determan_3x3(threeby))
        
    return value


def inverse_4x4(matrix):
    """ Calculate the inverse of a 4x4 matrix """
    deter = determan_4x4(matrix)
    
    co_factor = [[0.0, 0.0, 0.0], 
                 [0.0, 0.0, 0.0], 
                 [0.0, 0.0, 0.0]] # Temp storage of reduced matrix
        
    inverse = [[0.0, 0.0, 0.0, 0.0], 
               [0.0, 0.0, 0.0, 0.0], 
               [0.0, 0.0, 0.0, 0.0], 
               [0.0, 0.0, 0.0, 0.0]]
    
    for i in xrange(4):
        for j in xrange(4):
            cofactor(matrix, 4, i, j, co_factor)
            sign = 0
            if (i+j)%2 == 0:
                sign = 1.0
            else:
                sign = -1.0
                
            inverse[j][i] = sign * determan_3x3(co_factor) / deter
            
    return inverse

def replace_align(mgr1, mgr2):
    """ Align the file amino acid with the position of the amino acid being replaced in the object. This means calculating a transform which will translate and rotate the file object into the correct position in the object space. """
    t_o = [[0.0, 0.0, 0.0, 0.0], 
           [0.0, 0.0, 0.0, 0.0], 
           [0.0, 0.0, 0.0, 0.0], 
           [0.0, 0.0, 0.0, 0.0]] # Transform for object
    
    t_f = [[0.0, 0.0, 0.0, 0.0], 
           [0.0, 0.0, 0.0, 0.0], 
           [0.0, 0.0, 0.0, 0.0], 
           [0.0, 0.0, 0.0, 0.0]] # Transform for file
    
    for i in xrange(4):
        for j in xrange(4):
            if i == j:
                t_o[i][j] = 1.0
                t_f[i][j] = 1.0
            else:
                t_o[i][j] = 0.0
                t_f[i][j] = 0.0
        
    pv_o = [[mgr2[1].get_x(), mgr2[1].get_y(), mgr2[1].get_z()], 
            [mgr2[2].get_x(), mgr2[2].get_y(), mgr2[2].get_z()]] #Pure Vectors for 0 and CA of object
    
    t_o[0][3] = -(mgr2[0].get_x())
    t_o[1][3] = -(mgr2[0].get_y())
    t_o[2][3] = -(mgr2[0].get_z())
    
    v_o = [[0, 0, 0], 
           [0, 0, 0]] # Vectors for 0 and CA of object
    
    for i in xrange(2):
        for j in xrange(3):
            v_o[i][j] = (pv_o[i][0] * t_o[j][0]) + (pv_o[i][1] * t_o[j][1]) + (pv_o[i][2] * t_o[j][2]) + t_o[j][3]
    
    pv_f = [[mgr1[1].get_x(), mgr1[1].get_y(), mgr1[1].get_z()], 
            [mgr1[2].get_x(), mgr1[2].get_y(), mgr1[2].get_z()]] #Pure Vectors for 0 and CA of file
    
    t_f[0][3] = -(mgr1[0].get_x())
    t_f[1][3] = -(mgr1[0].get_y())
    t_f[2][3] = -(mgr1[0].get_z())
    
    v_f = [[0, 0, 0], 
           [0, 0, 0]] # Vectors for 0 and CA of file
    
    for i in xrange(2):
        for j in xrange(3):
            v_f[i][j] = (pv_f[i][0] * t_f[j][0]) + (pv_f[i][1] * t_f[j][1]) + (pv_f[i][2] * t_f[j][2]) + t_f[j][3]
    
    cos_a = v_o[0][0] / math.sqrt(float(v_o[0][0] * v_o[0][0] + v_o[0][1] * v_o[0][1]))
    sin_a = -(v_o[0][1]) / math.sqrt(float(v_o[0][0] * v_o[0][0] + v_o[0][1] * v_o[0][1]))
    
    t = [[0, 0, 0, 0], 
         [0, 0, 0, 0], 
         [0, 0, 0, 0], 
         [0, 0, 0, 0]] # Temporary matrix
    
    for i in xrange(4):
        for j in xrange(4):
            if i == j:
                t[i][j] = 1.0
            else:
                t[i][j] = 0.0
    
    t[0][0] = cos_a
    t[1][1] = cos_a
    t[1][0] = sin_a
    t[0][1] = -(sin_a)
    
    prod = multiply_4x4(t_o, t)
    
    # copy the contents of prod inside t_o
    t_o = []
    t_o.extend(prod)
    
    for i in xrange(2):
        for j in xrange(3):
            v_o[i][j] = (pv_o[i][0] * prod[j][0]) + (pv_o[i][1] * prod[j][1]) + (pv_o[i][2] * prod[j][2]) + prod[j][3]
            
    cos_a = v_o[0][0] / math.sqrt(float(v_o[0][0] * v_o[0][0] + v_o[0][2] * v_o[0][2]))
    sin_a = -(v_o[0][2]) / math.sqrt(float(v_o[0][0] * v_o[0][0] + v_o[0][2] * v_o[0][2]))
    
    for i in xrange(4):
        for j in xrange(4):
            if i == j:
                t[i][j] = 1.0
            else:
                t[i][j] = 0.0
        
    t[0][0] = cos_a
    t[2][2] = cos_a
    t[2][0] = sin_a
    t[0][2] = -(sin_a)
    
    prod = multiply_4x4(t_o, t)
    
    # copy the contents of prod inside t_o
    t_o = []
    t_o.extend(prod)
    
    for i in xrange(2):
        for j in xrange(3):
            v_o[i][j] = (pv_o[i][0] * prod[j][0]) + (pv_o[i][1] * prod[j][1]) + (pv_o[i][2] * prod[j][2]) + prod[j][3]
            
    cos_a = v_f[0][0] / math.sqrt(float(v_f[0][0] * v_f[0][0] + v_f[0][1] * v_f[0][1]))
    sin_a = -(v_f[0][1]) / math.sqrt(float(v_f[0][0] * v_f[0][0] + v_f[0][1] * v_f[0][1]))
    
    for i in xrange(4):
        for j in xrange(4):
            if i == j:
                t[i][j] = 1.0
            else:
                t[i][j] = 0.0
        
    t[0][0] = cos_a
    t[1][1] = cos_a
    t[1][0] = sin_a
    t[0][1] = -(sin_a)
    
    prod = multiply_4x4(t_f, t)
    
    # copy the contents of prod inside t_f
    t_f = []
    t_f.extend(prod)
    
    for i in xrange(2):
        for j in xrange(3):
            v_f[i][j] = (pv_f[i][0] * prod[j][0]) + (pv_f[i][1] * prod[j][1]) + (pv_f[i][2] * prod[j][2]) + prod[j][3]
            
    cos_a = v_f[0][0] / math.sqrt(float(v_f[0][0] * v_f[0][0] + v_f[0][2] * v_f[0][2]))
    sin_a = -(v_f[0][2]) / math.sqrt(float(v_f[0][0] * v_f[0][0] + v_f[0][2] * v_f[0][2]))
    
    for i in xrange(4):
        for j in xrange(4):
            if i == j:
                t[i][j] = 1.0
            else:
                t[i][j] = 0.0
        
    t[0][0] = cos_a
    t[2][2] = cos_a
    t[2][0] = sin_a
    t[0][2] = -(sin_a)
    
    prod = multiply_4x4(t_f, t)
    
    # copy the contents of prod inside t_f
    t_f = []
    t_f.extend(prod)
    
    for i in xrange(2):
        for j in xrange(3):
            v_f[i][j] = (pv_f[i][0] * t_f[j][0]) + (pv_f[i][1] * t_f[j][1]) + (pv_f[i][2] * t_f[j][2]) + t_f[j][3]
                
    angle1 = math.acos(float(v_o[1][1] / math.sqrt(float((v_o[1][1] * v_o[1][1]) + (v_o[1][2] * v_o[1][2])))))
    
    if v_o[1][2] < 0.0:
        angle1 = -angle1
        
    angle2 = math.acos(float(v_f[1][1] / math.sqrt(float((v_f[1][1] * v_f[1][1]) + (v_f[1][2] * v_f[1][2])))))
    
    if v_f[1][2] < 0.0:
        angle2 = -angle2
        
    cos_a = math.cos(float(angle1 - angle2))
    sin_a = math.sin(float(angle1 - angle2))
    
    for i in xrange(4):
        for j in xrange(4):
            if i == j:
                t[i][j] = 1.0
            else:
                t[i][j] = 0.0
                
    t[1][1] = cos_a
    t[2][2] = cos_a
    t[2][1] = sin_a
    t[1][2] = -(sin_a)
    
    prod = multiply_4x4(t_f, t)
    t_f = []
    t_f.extend(prod)

    i_o = inverse_4x4(t_o)
    
    transform = multiply_4x4(t_f, i_o)
    
    return transform

def inverse_6x6(input):
    """ Create the inverse of a 6 by 6 matrix by the Gauss-Jordan reduction method. Algorithm taken from Applied Numerical Methods by Carnahan, Luther, Wilkes (Wiley and Sons, 1969) """
    matrix = []
    
    matrix.extend(input)
    
    inverse = [[0, 0, 0, 0, 0, 0], 
               [0, 0, 0, 0, 0, 0], 
               [0, 0, 0, 0, 0, 0], 
               [0, 0, 0, 0, 0, 0], 
               [0, 0, 0, 0, 0, 0], 
               [0, 0, 0, 0, 0, 0]]
    
    # start with the inverse being the identity matrix
    for row in xrange(6):
        for col in xrange(6):
            if row == col:
                inverse[row][col] = 1.0
            else:
                inverse[row][col] = 0.0

        
    # for each row, divide by the pivot (diagonal) element. Since (as we will see later), only elements at or to the right of the diagonal are still
    # non-zero, only that portion of the row is actually divided in the matrix. Do all operations to both the matrix and the inverse at the same time
    for row in xrange(6):
        for col in xrange(6):
            if col > row:
                matrix[col][row] /= matrix[row][row]
            inverse[col][row] /= matrix[row][row]
        matrix[row][row] = 1.0
        
        # Now the diagona element is 1.0. Substract from each row the product of the row elements just above the diagonal times the pivot row.
        # This makes zero in the column above and below the diagonal element which was just set to one.
        col = row
        for i in xrange(6):
            if i!= row and matrix[col][i] != 0.0:
                for j in xrange(6):
                    if j > col:
                        matrix[j][i] -= matrix[col][i]*matrix[j][row]
                    inverse[j][i] -= matrix[col][i]*inverse[j][row]
                matrix[col][i] = 0.0
                
    return inverse


def align_atoms(mgr1, mgr2):
    """ Perform RMS alignment of two sets of coordinates by the iterative method suggested by BIOSYM (Hilderbrandt). 
    ..."""
    count = float(len(mgr1))
    
    transform = replace_align(mgr1, mgr2)
    
    apply = [[0, 0, 0, 0], 
             [0, 0, 0, 0], 
             [0, 0, 0, 0], 
             [0, 0, 0, 0]] # Next transform to be applied
    
    for k in xrange(4):
        for l in xrange(4):
            if l == k:
                apply[k][l] = 1.0
            else:
                apply[k][l] = 0.0

    p_atom_fit = mgr1[0] # Atom to be fit
    p_atom_ref = mgr2[0] # Atom which is not moved
    
    apply[0][3] = p_atom_fit.get_x() - p_atom_ref.get_x()
    apply[1][3] = p_atom_fit.get_y() - p_atom_ref.get_y()
    apply[2][3] = p_atom_fit.get_z() - p_atom_ref.get_z()
    
    prod4 = multiply_4x4(transform, apply)
    
    transform = []
    transform.extend(prod4)
    
    com_fit = [0.0, 0.0, 0.0] # Center of mass of FIT coords
    com_ref = [0.0, 0.0, 0.0] # Center of mass of REF coords
    
    for i in xrange(int(count)):
        p_atom_fit = mgr1[i]
        p_atom_ref = mgr2[i]
        
        com_fit[0] += (p_atom_fit.get_x() * transform[0][0] + p_atom_fit.get_y() * transform[0][1] + p_atom_fit.get_z() * transform[0][2] + transform[0][3])
        com_fit[1] += (p_atom_fit.get_x() * transform[1][0] + p_atom_fit.get_y() * transform[1][1] + p_atom_fit.get_z() * transform[1][2] + transform[1][3])
        com_fit[2] += (p_atom_fit.get_x() * transform[2][0] + p_atom_fit.get_y() * transform[2][1] + p_atom_fit.get_z() * transform[2][2] + transform[2][3])
        
        com_ref[0] += p_atom_ref.get_x()
        com_ref[1] += p_atom_ref.get_y()
        com_ref[2] += p_atom_ref.get_z()
        
    for i in xrange(3):
        com_fit[i] /= count
        com_ref[i] /= count
        
    for i in xrange(4):
        for j in xrange(4):
            if i == j:
                apply[i][j] = 1.0
            else:
                apply[i][j] = 0.0
    
    for i in xrange(3):
        apply[i][3] = -(com_fit[i])
        
    prod4 = multiply_4x4(transform, apply)
    transform = []
    transform.extend(prod4)
    
    right_side = [0, 0, 0, 0, 0, 0] # Right side of min equation
    
    lsq_matrix = [[0, 0, 0, 0, 0, 0], 
                  [0, 0, 0, 0, 0, 0], 
                  [0, 0, 0, 0, 0, 0], 
                  [0, 0, 0, 0, 0, 0], 
                  [0, 0, 0, 0, 0, 0], 
                  [0, 0, 0, 0, 0, 0]] #Least squares symmetric matrix
    
    for iter in xrange(20):
        for i in xrange(6):
            right_side[i] = 0.0
            for j in xrange(6):
                lsq_matrix[i][j] = 0.0
    
        deviation = 0.0
    
        lsq_matrix[0][0] = count
        lsq_matrix[1][1] = count
        lsq_matrix[2][2] = count
    
        fit  = [0, 0, 0] # Transformed coord of atom to be fit
        ref = [0, 0, 0] # Copy of reference coords
        rms_last = 0 # Initial value doesn't matter since not used in firs iteration
    
        for i in xrange(int(count)):
            p_atom_fit = mgr1[i]
            p_atom_ref = mgr2[i]
            fit[0] = (p_atom_fit.get_x() * transform[0][0]) + (p_atom_fit.get_y() * transform[0][1]) + (p_atom_fit.get_z() * transform[0][2]) + transform[0][3]
            fit[1] = (p_atom_fit.get_x() * transform[1][0]) + (p_atom_fit.get_y() * transform[1][1]) + (p_atom_fit.get_z() * transform[1][2]) + transform[1][3]
            fit[2] = (p_atom_fit.get_x() * transform[2][0]) + (p_atom_fit.get_y() * transform[2][1]) + (p_atom_fit.get_z() * transform[2][2]) + transform[2][3]
            
            ref[0] = p_atom_ref.get_x() - com_ref[0]
            ref[1] = p_atom_ref.get_y() - com_ref[1]
            ref[2] = p_atom_ref.get_z() - com_ref[2]
            
            lsq_matrix[1][5] += fit[0]
            lsq_matrix[2][4] -= fit[0]
            lsq_matrix[2][3] += fit[1]
            lsq_matrix[0][5] -= fit[1]
            lsq_matrix[0][4] += fit[2]
            lsq_matrix[1][3] -= fit[2]
            lsq_matrix[3][4] -= fit[0]*fit[1]
            lsq_matrix[3][5] -= fit[0]*fit[2]
            lsq_matrix[4][5] -= fit[1]*fit[2]
            lsq_matrix[5][5] += fit[0]*fit[0] + fit[1]*fit[1]
            lsq_matrix[4][4] += fit[0]*fit[0] + fit[2]*fit[2]
            lsq_matrix[3][3] += fit[1]*fit[1] + fit[2]*fit[2]
            
            diff_x = ref[0] - fit[0]
            diff_y = ref[1] - fit[1]
            diff_z = ref[2] - fit[2]
            
            deviation += diff_x*diff_x + diff_y*diff_y + diff_z*diff_z
            
            right_side[0] += diff_x
            right_side[1] += diff_y
            right_side[2] += diff_z
            right_side[3] += fit[1]*ref[2] - fit[2]*ref[1]
            right_side[4] += fit[2]*ref[0] - fit[0]*ref[2]
            right_side[5] += fit[0]*ref[1] - fit[1]*ref[0]
    
        lsq_matrix[5][1] = lsq_matrix[1][5]
        lsq_matrix[4][2] = lsq_matrix[2][4]
        lsq_matrix[3][2] = lsq_matrix[2][3]
        lsq_matrix[5][0] = lsq_matrix[0][5]
        lsq_matrix[4][0] = lsq_matrix[0][4]
        lsq_matrix[3][1] = lsq_matrix[1][3]
        lsq_matrix[4][3] = lsq_matrix[3][4]
        lsq_matrix[5][3] = lsq_matrix[3][5]
        lsq_matrix[5][4] = lsq_matrix[4][5]
    
        deviation = math.sqrt(deviation / count)
        
        if deviation == 0.0:
            break
        
        if iter > 0:
            if rms_last != 0:
                if math.fabs(deviation/rms_last - float(1.0)) < 0.000001:
                    break
                
        rms_last = deviation
        
        inverse = inverse_6x6(lsq_matrix)
        
        prod = [0, 0, 0, 0, 0, 0] # Product of inverse and right side
        
        for i in xrange(6):
            prod[i] = 0.0
            for j in xrange(6):
                prod[i] += inverse[i][j]*right_side[j]
        
        for ival in xrange(3):
            for i in xrange(4):
                for j in xrange(4):
                    apply[i][j] = 0.0
                    
            apply[ival][ival] = 1.0
            apply[3][3] = 1.0
            
            if ival > 1:
                m = ival-2
            else:
                m = ival+1
                
            if m > 1:
                n = m-2
            else:
                n = m+1
            
            apply[m][m] = math.cos(float(prod[ival+3]))
            apply[n][n] = apply[m][m]
            apply[n][m] = math.sin(float(prod[ival+3]))
            apply[m][n] = -(apply[n][m])
            
            prod4 = multiply_4x4(transform, apply)
            transform = []
            transform.extend(prod4)
            
        for i in xrange(4):
            for j in xrange(4):
                if i == j:
                    apply[i][j] = 1.0
                else:
                    apply[i][j] = 0.0
            
        apply[0][3] = prod[0]
        apply[1][3] = prod[1]
        apply[2][3] = prod[2]
            
        prod4 = multiply_4x4(transform, apply)
        transform = []
        transform.extend(prod4)
        
    for i in xrange(3):
        apply[i][3] = com_ref[i]
        
    prod4 = multiply_4x4(transform, apply)
    transform = []
    transform.extend(prod4)
    
    return rms_last
    
        
################### K-clustering
    

def usage():
    print __doc__

def call_command(command, pipe=None, echo = False):
    if echo:
        print command
    
    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    
    output=process.communicate(input=pipe)
    return output


def assign_groups(groups_list, centroids_list, matRMSD, iRestartNo):
    """ Assign groups """
    if iRestartNo < 100:
        print '\tgrouping',
    
    # for each structure, check which centroid is closest
    for i in xrange(len(STRUCTURE_LIST)):
        if iRestartNo < 100 and (i+1)%1000 == 0:
            print '[{i}]'.format(i=i+1),
            
        minRMSD = sys.float_info.max # max for a float
        iMink = 0
        
        for k in xrange(len(centroids_list)):
            j = centroids_list[k]
            if matRMSD[i][j] == sys.float_info.max:
                # align_atoms
                matRMSD[i][j] = align_atoms(STRUCTURE_LIST[i], STRUCTURE_LIST[j])
            if matRMSD[i][j] < minRMSD:
                minRMSD = matRMSD[i][j]
                iMink = k
        
        groups_list.append(iMink)
    
    if iRestartNo < 100:
        print '\n'
        
        
        
def get_centroid(k, groups_list, matRMSD, iRestartNo):
    """ return the index for the centroid """
    if iRestartNo < 100:
        print '\tcentroiding: ',
    
    RMSD_list = []
    for i in xrange(len(groups_list)):
        RMSD_list.append(0.0)
        
    for i in xrange(len(STRUCTURE_LIST)):
        if iRestartNo < 100 and ((i+1) % 1000 == 0):
            print '[{0}]'.format(i+1),
        if groups_list[i] != k:
            continue
        for j in xrange(i+1, len(STRUCTURE_LIST)):
            if groups_list[j] != k:
                continue
            
            fRMSD = matRMSD[i][j]
            
            if fRMSD == sys.float_info.max:
                fRMSD = align_atoms(STRUCTURE_LIST[i], 
                                    STRUCTURE_LIST[j])
                matRMSD[i][j] = fRMSD
            
            RMSD_list[i] += fRMSD
            RMSD_list[j] += fRMSD 
        
    iMinK = 0
    fMinRMSD = sys.float_info.max
    
    
    for i in xrange(len(STRUCTURE_LIST)):
        if groups_list[i] != k:
            continue
        
        if RMSD_list[i] < fMinRMSD:
            fMinRMSD = RMSD_list[i]
            iMinK = i
    print "\n",
    
    return iMinK, fMinRMSD
   
    
def get_stats_RMSD(k, groups_list, j, matRMSD):
    """ Return the stats for the RMSD (mean, stdev, max) """
    fMax = 0.0
    fMean = 0.0
    fCount = 0.0
    fStdDev = 0.0
        
    for i in xrange(len(groups_list)):
        if groups_list[i] == k:
            fCount += 1
            fRMSD = matRMSD[i][j]
            fMean += fRMSD
            if fRMSD > fMax:
                fMax = fRMSD
        
    fMean = fMean / fCount
        
    fStdDev = 0.0
    for i in xrange(len(groups_list)):
        if groups_list[i] == k:
            fRMSD = matRMSD[i][j]
            fStdDev += (fRMSD - fMean) * (fRMSD - fMean)
                
    fStdDev = math.sqrt(fStdDev / fCount)
        
    return fMean, fStdDev, fMax
    
    
def visit_files():
    """ save the coordinates from the PDB files into """
    # standard number of atoms in pdb files (the 1st PDB is used as standard)
    nbAtoms = 0    
    pdb_list = [elem for elem in os.listdir(WORKING_DIRECTORY) if ('pdb.gz' in elem)]
    for pdb in sorted(pdb_list):
        call_command('gunzip ' + pdb)
        unzipped = pdb.replace('pdb.gz', 'pdb')
        
        with open(os.path.join(WORKING_DIRECTORY, unzipped), 'r') as read_file:
            point_list = []
            
            for line in read_file:
                if 'ATOM' in line:
                    # get the coordinates
                    splitted = line.split()
                    if splitted[2] == 'P':
                        # get residue number
                        iResNo = int(splitted[5])
                        if (1004 >= iResNo >= 1001) or not SPECIAL_PHOSPHATE:
                            point_list.append(Point3D(x=float(splitted[6]), y=float(splitted[7]), z=float(splitted[8])))
            
            if len(point_list) > 0:
                if nbAtoms == 0:
                    nbAtoms = len(point_list)
                if nbAtoms == len(point_list):
                    global STRUCTURE_LIST
                    STRUCTURE_LIST.append(point_list)
                
                    # update MAPINDEX2FILE
                    global MAPINDEX2FILE
                    MAPINDEX2FILE[len(STRUCTURE_LIST)-1] = pdb
                else:
                    print 'This structure has a different number of atoms: {structure} ({atoms} instead of {mean})'.format(structure=pdb,
                                                                                                                           atoms=len(point_list),
                                                                                                                           mean=nbAtoms)
        call_command('gzip ' + unzipped)
        print 'Loaded ' + pdb

        
if __name__ == '__main__':
    start_time = datetime.now()
    
    STRUCTURE_LIST = []
    MAPINDEX2FILE = dict()
 
    parser = argparse.ArgumentParser(description='Parser for k_clustering')

    parser.add_argument('--nbclusters', '-c', action="store", dest="nbclusters" ,default=1, type=int)
    parser.add_argument('--nbrestarts', '-r', action="store", dest="nbrestarts", default=1, type=int)
    parser.add_argument('--nbmodels', '-m', action="store", dest="nbmodels", default=0.25, type=float)
    parser.add_argument('--specialP', '-s', action="store_true", default=False)
    parser.add_argument('--workdir', '-w', action="store", dest="workdir", default=None)
    
    ns = parser.parse_args()
    
    nbclusters = ns.nbclusters
    nbrestarts = ns.nbrestarts
    nbmodels = ns.nbmodels
    SPECIAL_PHOSPHATE = ns.specialP
    if ns.workdir:
        WORKING_DIRECTORY = ns.workdir
    else:
        WORKING_DIRECTORY = os.getcwd()
            
    if nbclusters < 1:
        nbclusters = 1
    elif nbclusters > 100:
        nbclusters = 100
    if nbrestarts <1:
        nbrestarts = 1
    if 0 >= nbmodels >= 1.0:
        nbmodels = 1.0
        
    for centro in [elem for elem in os.listdir(WORKING_DIRECTORY) if ('centroid' in elem)]:
        try:
            os.remove(os.path.join(WORKING_DIRECTORY, centro))
        except Exception as e:
            pass
    
    # keep only the wanted proportion of models
    print 'Preparing the directory...\nThe following files will be clusterized:'
    pdb_list = [elem for elem in os.listdir(WORKING_DIRECTORY) if ('pdb.gz' in elem)]
    for pdb in sorted(pdb_list):
        if random.random() > nbmodels:
            try:
                os.remove(os.path.join(WORKING_DIRECTORY, pdb))
            except Exception as e:
                pass
        else:
            print 'Loaded ' + pdb
            
    os.chdir(WORKING_DIRECTORY)
            
    # get the coordinates
    visit_files()
    
    # create the matrix for RMSD (2D matrix of length=len(STRUCTURE_LIST) in each dimension)
    matRMSD = []
    for i in xrange(len(STRUCTURE_LIST)):
        row = []
        # create the "2nd dimension"
        for j in xrange(len(STRUCTURE_LIST)):
            row.append(sys.float_info.max) # max for a float
        matRMSD.append(row)
        
    centroids_list = []
    
    for i in xrange(nbclusters):
        centroids_list.append(0)
    
    min_centroids_list = []
    min_groups_list = []
    fTotMinRMSD = sys.float_info.max
    
    for restarts in xrange(nbrestarts):
        print '---------------------------------\nrestart #{r}, Total RMSD to centroids: {minRMSD}'.format(r=restarts+1, 
                                                                                                           minRMSD=fTotMinRMSD)
        # assign random centroids
        for i in xrange(nbclusters):
            rand = random.random()
            j = int(rand * len(STRUCTURE_LIST))
            centroids_list[i] = j
        
        # assign groups according to current centroids
        groups_list = []
        assign_groups(groups_list, centroids_list, matRMSD, restarts)
        
        # try to better the total RMSD
        iLoop = 0
        bLoop = True
        
        while True:
            fTotRMSD = 0.0
            for i in xrange(nbclusters):
                # get new centroid for a give group
                centroids_list[i], fMinRMSD = get_centroid(i, groups_list, matRMSD, restarts)
                fTotRMSD += fMinRMSD
                
            # assign groups according to new centroids
            new_groups = []
            assign_groups(new_groups, centroids_list, matRMSD, restarts)
                
            # if these new groups are the same as previous then stop
            if new_groups == groups_list:
                bLoop = False
                
            groups_list = new_groups
            
            if fTotRMSD < fTotMinRMSD:
                fTotMinRMSD = fTotRMSD
                min_centroids_list = centroids_list
                min_groups_list = groups_list
                    
            iLoop += 1
            if not (bLoop and iLoop < 10):
                break
        
    # sort centroids according to their population
    list_group_sort = []
    list_group_size = []
    for i in xrange(nbclusters):
        list_group_sort.append(i)
        list_group_size.append(min_groups_list.count(i))
            
    # selection sort
    for i in xrange(nbclusters):
        for j in xrange(i+1, nbclusters):
            if list_group_size[j] > list_group_size[i]:
                # swap size
                tmp = list_group_size[i]
                list_group_size[i] = list_group_size[j]
                list_group_size[j] = tmp
                
                # swap sort
                tmp = list_group_sort[i]
                list_group_sort[i] = list_group_sort[j]
                list_group_sort[j] = tmp
        
    list_sort_group = []
    for i in xrange(nbclusters):
        list_sort_group.append(0)
    
    for i in xrange(nbclusters):
        list_sort_group[list_group_sort[i]] = i
            
    # sorted version
    print '\n\n----------------------------------\nBest Centroids are:'
    print list_group_sort
    print min_centroids_list
    for group in list_group_sort:
        fMean, fStdDev, fMax = get_stats_RMSD(group, 
                                              min_groups_list, 
                                              min_centroids_list[group], 
                                              matRMSD)
        # note : fMax is not very useful for now
        # Show best centroids
        print 'Centroid #{0}: {1} |S|={2} mean: {3} +/- {4} max: {5}'.format(i+1, 
                                                                             MAPINDEX2FILE[min_centroids_list[group]], 
                                                                             list_group_size[i],
                                                                             fMean,
                                                                             fStdDev,
                                                                             fMax)
        call_command('gunzip ' + os.path.join(WORKING_DIRECTORY, MAPINDEX2FILE[min_centroids_list[group]]))
        unzipped = MAPINDEX2FILE[min_centroids_list[group]].replace('pdb.gz', 'pdb')
   
        try:
            shutil.copy(os.path.join(WORKING_DIRECTORY, unzipped), 
                        os.path.join(WORKING_DIRECTORY, 'centroid-{n:03d}.pdb'.format(n = 1+group)))
        except Exception as e:
            pass
        
        call_command('gzip ' + unzipped)
        call_command('gzip ' + os.path.join(WORKING_DIRECTORY, 'centroid-{n:03d}.pdb'.format(n = 1+group)))
        
    print 'Total RMSD to centroids: {0}'.format(fTotMinRMSD)
    
    # align all centroids
    ################# MCRMSD HERE
    out, err = call_command('{mcrmsd} -n centroid-*.pdb.gz -o centroids.pdb'.format(mcrmsd='/soft/bioinfo/linux/mcrms-dev/bin/MC-RMSD'))
    print out
        
    print 'Centroid-Centroid distance matrix:'
        
    fMinCC = sys.float_info.max
    for i in xrange(nbclusters):
        for j in xrange(nbclusters):
            I = min_centroids_list[list_group_sort[i]]
            J = min_centroids_list[list_group_sort[j]]
            fRMSD = matRMSD[I][J]
            print ' {0:5.2f}'.format(fRMSD)
            if fRMSD > 0.0001 and fRMSD < fMinCC:
                fMinCC = fRMSD
    
    print 'Minimum Centroid-Centroid distance: {0:5.2f}'.format(fMinCC)
    
    # produce output file
    with open(os.path.join(WORKING_DIRECTORY, 'clusters.data'), 'w') as output_file:
        for i in xrange(len(min_groups_list)):
            output_file.write('Model {model} is in cluster: {cluster}\n'.format(model=MAPINDEX2FILE[i], 
                                                                                cluster=1+list_sort_group[min_groups_list[i]]))
        
    nbFoldSecs =  datetime.now() - start_time
        
    print 'Computation time: {time}'.format(time=nbFoldSecs)
        
    print 'Done.'
        