# -*- coding: UTF-8 -*-

"""
Takes a regular stable marriage output and add gene and site occupancy to the file

Usage: python generate_occupancy.py --input=[file or folder path] [options]

Options:
  -h, --help                 show this help
  -i ... , --input=...       input file or folder
  -g ... , --gene_quantity     file containing infos about the gene quantity
  -o ... , --output=...      output file or folder
  
===================================================================================

The syntax for the input file has to be the following:
- the line must contain the following informations separated with tabs in the following order:
    'transcript(NM_...)\tmiRNA(MIMAT...)\tMRE_position\tprobability\tnumber'
- the line will be ignored if it doesn't meet the above condition or if it begins with '#'
"""

import os
import math
import argparse
import time

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()

    parser.add_argument('--input', '-i', action="store", dest="input_path", required=True)
    parser.add_argument('--gene_quantity', '-g', action="store", dest="gene_quantity_path", required=True)
    parser.add_argument('--mir_quantity', '-m', action="store", dest="mir_quantity_path", required=True)
    parser.add_argument('--output', '-o', action="store", dest="output_path",  default=os.path.abspath(os.curdir))
    parser.add_argument('--verbose', '-v', dest='verbose', action='store_true')
    
    ns = parser.parse_args()
    
    input_path = os.path.join(os.path.abspath(os.curdir), ns.input_path)
    output_path = os.path.join(os.path.abspath(os.curdir), ns.output_path)
    verbose = ns.verbose
    gene_quantity_path = os.path.join(os.path.abspath(os.curdir), ns.gene_quantity_path)
    mir_quantity_path = os.path.join(os.path.abspath(os.curdir), ns.mir_quantity_path)
    
    # this variable is used to determine whether the params are valids or not
    validated = True
    
    if input_path == output_path:
        print "Error : the input and output shouldn't be the same"
        validated= False
    
    if os.path.isdir(input_path):
        file_list = [os.path.join(input_path, elem) for elem in sorted(os.listdir(input_path)) if not os.path.isdir(os.path.join(input_path, elem))]
        if not os.path.exists(output_path) or os.path.isfile(output_path):
            print "Error : output directory not found " + output_path
            validated = False
    else:
        file_list = [input_path]
        
        output_folderpath, output_file = os.path.split(output_path)
        
        if not os.path.exists(output_folderpath):
            print "Error : output directory not found " + output_folderpath
            validated = False
    if not os.path.exists(gene_quantity_path) or os.path.isdir(gene_quantity_path):
        print "Error : gene quantity file not found " + gene_quantity_path
        validated = False
        
    if not os.path.exists(mir_quantity_path) or os.path.isdir(mir_quantity_path):
        print "Error : miRNA quantity file not found " + mir_quantity_path
        validated = False
        
    if validated:
        # start by putting the gene data in a dict
        gene_quantity_dict = dict()
        
        with open(gene_quantity_path, 'r') as gene_qty_cont:
            for line in gene_qty_cont:
                if not line.startswith('#'):
                    splitted = line.split()
                    gene_quantity_dict[splitted[0].strip()] = float(splitted[1].strip())
        
        # then the miR data in a dict
        mir_qty_dict = dict()
        
        with open(mir_quantity_path, 'r') as mir_qty_cont:
            for line in mir_qty_cont:
                if not line.startswith('#'):
                    splitted = line.split()
                    mir_qty_dict[splitted[0].strip()] = float(splitted[1].strip())
        
        for f in file_list:
            with open(f, 'r') as stable_file:
                if verbose:
                    print '#reading ' + f
                # dictionnary that will sepearate lines by gene, the lines will be stored as list
                gene_line_dict = dict()
                total_mir = 0
                list_line = stable_file.readlines()
                        
                goccupancy_dict = dict()
                goccupancy_mir_list = dict()
                list_to_print = []
                soccupancy_dict = dict()
                
                tmp_str = '#gene\tmiR\tpos\tprob\tnumber\t'
                        
                tmp_str += 'all_mre_occupancy\toccupied_mre_occupancy\tmir_proportion\t'
                
                tmp_str += 'mirna_proportion\tmre_proportion\ttotal_item_number'
                
                
                list_to_print.append(tmp_str)
                
                dict_mir = dict()
                dict_gene = dict()
                
                dict_data = dict()
                for idx, line in enumerate(list_line):
                    splitted = line.split()
        
                    if 7 == len(splitted) and not 'CellLine' in line and not line.startswith('#'):
                        gene = splitted[0].strip()
                        gene_symbol = splitted[1].strip()
                        miR = splitted[2].strip()
                        miR_symbol = splitted[3].strip()
                        pos = splitted[4].strip()
                        prob = splitted[5].strip()
                        number = int(splitted[6].strip())
                        
                        dict_mir[miR] = miR_symbol
                        dict_gene[gene] = gene_symbol
                        
                        if not dict_data.has_key(miR):
                            dict_data[miR] = 0
                        dict_data[miR] += number
                        
                        if not dict_data.has_key(miR + ',' + gene):
                            dict_data[miR + ',' + gene] = 0
                        dict_data[miR + ',' + gene] += number
                            
                        if not dict_data.has_key(miR + ',' + gene + 'dict'):
                            dict_data[miR + ',' + gene + 'dict'] = dict()
                                
                        if not dict_data[miR + ',' + gene + 'dict'].has_key(pos + '_' + prob):
                            dict_data[miR + ',' + gene + 'dict'][pos + '_' + prob] = 0
                        dict_data[miR + ',' + gene + 'dict'][pos + '_' + prob] += number                        
                        
                        if not dict_data.has_key(gene):
                            dict_data[gene] = 0
                        dict_data[gene] += number
                        
                        if not dict_data.has_key(gene + ',' + pos):
                            dict_data[gene + ',' + pos] = 0
                        dict_data[gene + ',' + pos] += number
                
                print '#' + str(len(dict_data))
#                list_data_k = dict_data.keys()
                for gene, gene_a in dict_gene.iteritems():
                    gene_occupancy = dict_data[gene]
                    
                    print str(gene) + '\t' + str(gene_a) + '\t' + str(gene_occupancy)
                    
                    for mir, mir_a in dict_mir.iteritems():
                        if dict_data.has_key(mir + ',' + gene):
                            influence = float(dict_data[mir + ',' + gene]) / float(gene_occupancy) 
                            
                            attraction = float(dict_data[mir + ',' + gene]) / float(dict_data[mir])
                            
                            print str(gene) + '\t' + str(gene_a) + '\t' + str(mir) + '\t' + str(mir_a) + '\t' + str(influence) + '\t' + str(attraction)
                            for k, v in dict_data[mir + ',' + gene + 'dict'].iteritems():
                                splitted_k = k.split('_')
                                pos = splitted_k[0]
                                prob = splitted_k[1]
                                occupied_mre_occupancy = float(v) / float(dict_data[gene + ',' + pos])
                                
                                print str(gene) + '\t' + str(gene_a) + '\t' + str(mir) + '\t' + str(mir_a) + '\t' + str(pos) + '\t' + str(prob) + '\t' + str(v) + '\t' + str(occupied_mre_occupancy)
                                
                                
                            
                    
#                        total_mre = 0
#                        total_miR = 0
#                        total_mre_pos = 0
#                        
#                        set_pos_on_gene = set()
##                        for l in gene_line_dict[gene]:
##                            sp = l.split()
##                            g = sp[0].strip()
##                            m = sp[2].strip()
##                            p = sp[4].strip()
##                            n = int(sp[6].strip())
##                            
##                            total_mre += n
##                            if m == miR:
##                                total_miR += n
##                            if p == pos:
##                                total_mre_pos += n
##                            
##                            set_pos_on_gene.add(p)
#                        
#                        total_used_mir_in_sample = 0
#                        
#                        for k, v in gene_line_dict.iteritems():
#                            for l in v:
#                                sp = l.split()
#                                g = sp[0].strip()
#                                m = sp[2].strip()
#                                p = sp[4].strip()
#                                n = int(sp[6].strip())
#                                
#                                if m == miR:
#                                    total_used_mir_in_sample += n
#                                    
#                                    
#                                if k == gene:
#                                    
#                                    total_mre += n
#                                    if m == miR:
#                                        total_miR += n
#                                    if p == pos:
#                                        total_mre_pos += n
#                                    
#                                    set_pos_on_gene.add(p)
#                                
#                                
#                            
#                        nb_pos_on_gene = len(set_pos_on_gene)
#                        
#                        
#                        gene_occupancy = total_mre
#                        
#                        influence = float(total_miR) / float(gene_occupancy)
#                        
#                        attraction = float(total_miR) / float(total_used_mir_in_sample)
#                        
#                        if splitted[1].strip() == 'RPLP1':
#                            if splitted[3].strip() == 'hsa-miR-370':
#                                print attraction
#                        
#                        
#                        
#                        
#                        
#                        # position specific
#                        occupied_mre_occupancy = float(number)/float(round(total_mre_pos))
#                        
#                        all_mre_occupancy = float(number)/float(gene_quantity_dict[gene])
#                        
#                        # not position specific
#                        mirna_proportion = float(total_miR)/float(total_mre)
#                        
#                        mre_proportion = float(total_miR)/(float(nb_pos_on_gene)*float(gene_quantity_dict[gene]))
#                        
#                        
#                        tmp_str = '{gene}\t{miR}\t{pos}\t{prob}\t{number}\t'.format(gene=gene,
#                                                                                    prob=prob,
#                                                                                    miR=miR,
#                                                                                    pos=pos,
#                                                                                    number=number)
#                        
#                        tmp_str += '{all_mre_occupancy}\t{occupied_mre_occupancy}\t'.format(all_mre_occupancy=all_mre_occupancy,
#                                                                                            occupied_mre_occupancy=occupied_mre_occupancy)
#                        
#                        tmp_str += '{mirna_proportion}\t{mre_proportion}\t{total_item_number}'.format(mirna_proportion=mirna_proportion,
#                                                                                                    mre_proportion=mre_proportion,
#                                                                                                    total_item_number=total_miR)
#                        
#                        list_to_print.append(tmp_str)
#                        
#                    elif not line.startswith('#'):
#                        list_to_print.append(line.strip())
#                        
##                # write the file
##                if os.path.isdir(output_path):
##                    new_file = os.path.join(output_path, os.path.split(f)[0])
##                else:
##                    new_file = output_path
##                    
##                if verbose:
##                    print 'writing ' + new_file
##                    
##                with open(new_file, 'w') as mir_cont:
##                    mir_cont.write('\n'.join(list_to_print))
