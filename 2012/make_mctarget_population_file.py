import os
import math
import time

res_num = 0
result_folder = '/u/lisivero/public_html/mariages_stables/Fversion/dataWithNM'

cell_line_list = sorted(os.listdir(result_folder))
idx2 = 0
print "CREATE TABLE temp () INHERITS (result_base_mctarget_items, result_mctarget_items, result_mctarget_stable_items);"
                    
for cell_line in cell_line_list:
    if cell_line != 'HCT116':
        continue
    
    curr_folder = os.path.join(result_folder, cell_line)
    
    list_sample = sorted(os.listdir(os.path.join(curr_folder, 'results')))
    
    for sample_filename in sorted(list_sample, reverse=True):
        splitted = sample_filename.split('_')
        
        sample_name = splitted[0]
        
        tissue_origin = ''
        taxid = ''
        if cell_line == 'DU145':
            tissue_origin = 'Prostate'
            taxid = '9606'
        elif cell_line == 'PC3':
            tissue_origin = 'Pancreatic adenocarcinoma'
            taxid = '9606'
        elif cell_line == 'HCT116':
            tissue_origin = 'Colon'
            taxid = '9606'
        
        if len(splitted) > 2:
            overexpressed_mirna = splitted[1]
            multiplier = splitted[2].replace('copies', '')
            sample_name += ' {mirna} ({multiplier})'.format(mirna=overexpressed_mirna, multiplier=multiplier)
            
            if overexpressed_mirna != 'wt' and overexpressed_mirna != 'MIMAT0000770' and overexpressed_mirna != 'MIMAT0000416' and overexpressed_mirna != 'MIMAT0000427':
                continue
            
        # starts with creating the sample table
        print "INSERT INTO sample (name, description, tissue_origin, taxid) VALUES ('{sample_name}', '', '{tissue_origin}', {taxid});".format(sample_name=sample_name,
                                                                                                                                            tissue_origin=tissue_origin,
                                                                                                                                            taxid=taxid)
        # now we add the sample_gene_info tables
        dict_transcript_qty = dict()
        gene_info_filepath = os.path.join(curr_folder, 'inputs', 'all_normalised_absolute_RefSeq.txt')
        
        with open(gene_info_filepath, 'rb') as gene_info:
            # only get the 'meaningful' lines in the list
            lines_list = [elem for elem in gene_info.readlines() if not elem.startswith('#')]
            
            # sort the list by quantity in decreasing order
            lines_list.sort(key=lambda elem: float((elem.split())[1]), reverse=True)
            
            rank = 0
            last_value = -1
            
            for idx, l in enumerate(lines_list):
                accession, qty = l.strip().split('\t')
                
                dict_transcript_qty[accession] = round(float(qty))
                
                if last_value < 0 or last_value > float(qty):
                    rank = float(idx+1)
                    
                last_value = float(qty)
                percentile = int(math.ceil((rank*100)/len(lines_list)))
                
                # add the sample_gene_info table
                print "INSERT INTO sample_gene_info (id_sample, id_transcript, quantity, percentile) VALUES ((SELECT id FROM sample WHERE name='{sample_name}'), '{accession}', {qty}, {percentile});".format(sample_name=sample_name,
                                                                                                                                                                                                            accession=accession,
                                                                                                                                                                                                            qty=qty,
                                                                                                                                                                                                            percentile=percentile)
                
        # now it's the sample_mirna_info tables
        if len(splitted) > 2:
            mirna_filename = overexpressed_mirna + '_over'
        else:
            mirna_filename = [elem for elem in sorted(os.listdir(os.path.join(curr_folder, 'inputs'))) if not ('MIMAT' in elem or elem=='all_normalised_absolute_RefSeq.txt')][0]
            
        dict_mirna_qty = dict()
        with open(os.path.join(curr_folder, 'inputs', mirna_filename), 'rb') as mirna_info:
            # only get the 'meaningful' lines in the list
            lines_list = [elem for elem in mirna_info.readlines() if not elem.startswith('#')]
            
            # sort the list by quantity in decreasing order
            lines_list.sort(key=lambda elem: float((elem.split())[1]), reverse=True)
            
            rank = 0
            last_value = -1
            
            for idx, l in enumerate(lines_list):
                accession, qty = l.strip().split('\t')
                
                dict_mirna_qty[accession] = round(float(qty))
                
                if last_value < 0 or last_value > float(qty):
                    rank = float(idx+1)
                    
                last_value = float(qty)
                percentile = int(math.ceil((rank*100)/len(lines_list)))
                
                # add the sample_mirna_info table
                print "INSERT INTO sample_mirna_info (id_sample, id_mirna, quantity, percentile) VALUES ((SELECT id FROM sample WHERE name='{sample_name}'), (SELECT id FROM mirna WHERE accession='{accession}'), {qty}, {percentile});".format(sample_name=sample_name,
                                                                                                                                                                                                                                                accession=accession,
                                                                                                                                                                                                                                                qty=qty,
                                                                                                                                                                                                                                                percentile=percentile)
                
        
        # and finally the results
        result_filepath = os.path.join(curr_folder, 'results', sample_filename)
        
        with open(result_filepath, 'rb') as result_cont:
            splitted_file = result_cont.read().splitlines()
            
            new_result = dict()
            
            list_lines = [elem for elem in splitted_file if (not elem.startswith('#') and len(elem.split())==7 and not 'CellLine' in elem)]
            
            # parse a 1st time to separate the lines by transcript
            transcript_line_dict = dict()
            for l in list_lines:
                sp = l.split('\t')
                if not transcript_line_dict.has_key(sp[0]):
                    transcript_line_dict[sp[0]] = []
                transcript_line_dict[sp[0]].append(l)
                
            # now parse the transcript_line_dict to get the needed infos
            for k, v in transcript_line_dict.iteritems():
                # start parsing the lines once to compute the "number of mir in a position", the "total number of MRE on the transcript"
                # the "sum of mir's number" and the "total number of sites on the transcript"
                
                # number of mir in a position, the used key is the position
                number_pos = dict()
                # total number of MRE on the transcript
                total_mre = 0
                # sum of mir's number, the used key is the miR's MIMAT
                total_number_mir = dict()
                # total number of sites on the transcript
                pos_set = set()
                
                for l in v:
                    splitted = l.split('\t')
                    mir = splitted[2]
                    pos = splitted[4]
                    num = int(splitted[6])
                    
                    total_mre += num
                    
                    if not number_pos.has_key(pos):
                        number_pos[pos] = 0
                    number_pos[pos] += num
                    
                    if not total_number_mir.has_key(mir):
                        total_number_mir[mir] = 0
                    total_number_mir[mir] += num
                    
                    pos_set.add(pos)
                
                # now reparse the lines to put the datas in the DB
                for l in v:
                    sr = l.split('\t')
                    id_transcript = sr[0]
                    mirna_accession = sr[2]
                    pos = sr[4]
                    prob = sr[5]
                    number = sr[6]
                    
                    nb_pos_on_gene = len(pos_set)
                    transcript_quantity = dict_transcript_qty[id_transcript]
                    
                    # position specific
                    occupied_mre_occupancy = float(number)/float(number_pos[pos])
                    all_mre_occupancy = float(number)/transcript_quantity
                    
                    # not position specific
                    mirna_proportion = float(total_number_mir[mirna_accession])/float(total_mre)
                    mre_proportion = float(total_number_mir[mirna_accession])/float(nb_pos_on_gene * transcript_quantity)
                    
                    if not new_result.has_key(id_transcript+mirna_accession):
                        # create the new_result dict()
                        new_result[id_transcript+mirna_accession] = dict(id_transcript=id_transcript,
                                                                         id_mirna="(SELECT id FROM mirna WHERE accession='{mirna_accession}')".format(mirna_accession=mirna_accession),
                                                                         id_sample="(SELECT id FROM sample WHERE name='{sample_name}')".format(sample_name=sample_name),
                                                                         overexpressed_mirna='',
                                                                         mirna_proportion=mirna_proportion,
                                                                         mre_proportion=mre_proportion,
                                                                         stable_items=[])
                    
                    # create the result_mctarget_items and then the stable item object
#                    print "INSERT INTO result_base_mctarget_items (type) VALUES ('mctarget_item');"
#                    
#                    
                    target_type = "CASE WHEN {pos} < (SELECT utr5_end FROM gene WHERE accession='{accession}') THEN target_type('5UTR') WHEN {pos} < (SELECT cds_end FROM gene WHERE accession='{accession}') THEN target_type('CDS') WHEN {pos} < (SELECT utr3_end FROM gene WHERE accession='{accession}') THEN target_type('3UTR') ELSE 'UNKNOWN' END".format(pos=int(pos)-3,
                                                                                                                                                                                                                                                                                                                          accession=id_transcript)
#
#                    res_num += 1
#                    mctarget_item = "INSERT INTO result_mctarget_items (id, position, prob, target_type) "
#                    mctarget_item += "VALUES ({res_num}, {pos}, {prob}, ({target_type}));".format(res_num=res_num,
#                                                                                                  pos=pos,
#                                                                                                  target_type=target_type,
#                                                                                                  prob=prob)
#                    print mctarget_item
#                    
#                    tmp_str = "INSERT INTO result_mctarget_stable_items (number, all_mre_occupancy, occupied_mre_occupancy) "
#                    tmp_str += "VALUES ({number}, {all_mre_occupancy}, {occupied_mre_occupancy});".format(number=number,
#                                                                                                          all_mre_occupancy=all_mre_occupancy,
#                                                                                                          occupied_mre_occupancy=occupied_mre_occupancy)
#                    
#                    
#                    print tmp_str
                    res_num += 1
                    tmp_str = "INSERT INTO temp (type, parent_id, position, prob, target_type, number, all_mre_occupancy, occupied_mre_occupancy) "
                    tmp_str += "VALUES ('mctarget_item', {par_id}, {pos}, {prob}, ({target_type}), {number}, {all_mre_occupancy}, {occupied_mre_occupancy});".format(pos=pos,
                                                                                                                                                                     par_id=res_num,
                                                                                                                                          target_type=target_type,
                                                                                                                                          prob=prob,
                                                                                                                                          number=number,
                                                                                                                                          all_mre_occupancy=all_mre_occupancy,
                                                                                                                                          occupied_mre_occupancy=occupied_mre_occupancy)
                    
                    print tmp_str
                    
                    
                    
                    
                    tmp_item = "SELECT result_base_mctarget_items JOIN result_mctarget_items ON id JOIN result_mctarget_stable_items ON id WHERE result_mctarget_items.position={pos} AND result_mctarget_items.prob={prob}".format(pos=pos,
                                                                                                                                                                                                                                    target_type=target_type,
                                                                                                                                                                                                                                    prob=prob)
                    tmp_dict = new_result[id_transcript+mirna_accession]
                    item_list = tmp_dict['stable_items']
                    
                    item_list.append(tmp_item)
                    
                    tmp_dict['stable_items'] = item_list
                    new_result[id_transcript+mirna_accession] = tmp_dict
                    
            # create the result
            list_new_result = []
            for k_2, result_dict in new_result.iteritems():
                query = "INSERT INTO result_mctarget (id_transcript, id_mirna, id_sample, overexpressed_mirna, mirna_proportion, mre_proportion, stable_items) "
                query += "VALUES ({id_transcript}, {id_mirna}, {id_sample}, '{overexpressed_mirna}', {mirna_proportion}, {mre_proportion}, '{stable_items}');".format(id_transcript=result_dict['id_transcript'],
                                                                                                                                                                id_mirna=result_dict['id_mirna'],
                                                                                                                                                                id_sample=result_dict['id_sample'],
                                                                                                                                                                overexpressed_mirna=result_dict['overexpressed_mirna'],
                                                                                                                                                                mirna_proportion=result_dict['mirna_proportion'],
                                                                                                                                                                mre_proportion=result_dict['mre_proportion'],
                                                                                                                                                                stable_items="{{(" + ")},{(".join(result_dict['stable_items']) + ")}}")
                
                print query
                
                this_result = "SELECT result_mctarget WHERE result_mctarget.id_transcript={id_transcript} AND ".format(id_transcript=id_transcript)
                this_result += "result_mctarget.id_mirna={id_mirna} AND result_mctarget.id_sample={id_sample} AND result_mctarget.overexpressed_mirna='{overexpressed_mirna}'".format(id_mirna=result_dict['id_mirna'],
                                                                                                                                                                                    id_sample=result_dict['id_sample'],
                                                                                                                                                                                    overexpressed_mirna=result_dict['overexpressed_mirna'])
                
                list_new_result.append(this_result)
            
#            print "INSERT INTO result_targeting (stable_items) VALUES ({new_results});".format(new_results="{{(" + ")},{(".join(list_new_result) + ")}}")
            
            
print "DROP TABLE temp cascade;"
