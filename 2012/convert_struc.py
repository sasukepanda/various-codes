sequence = "UGUAUCCUUGAAUGGAUUUUUGGAGCAGGAGUGGACACCUGACCCAAAGGAAAUCAAUCCAUAGGCUAGCAAU"
structure = "((((((((((((((((((((((((((((((((..)))))))))))))...))))))))))).)))))).)).."


#   u                      -          
#agg ugagguaguagguuguauaguu uagaauuaca
#||| :|||:|||:||||||||||||| |::::||:: 
#ucc uuucgauccuccgacaugucaa agagggaacu
#   -                      u         
   
   
left = 0
right = len(structure)-1

lay1 = ''
lay2 = ''
lay3 = ''
lay4 = ''
lay5 = ''

# find the pairs and put them in a dict
dict_pairs = dict()

list_done = []
for idx, elem in enumerate(structure):
    if elem == ')':
        for rev in reversed(xrange(idx)):
            if structure[rev] == '(' and (not rev in list_done):
                dict_pairs[idx] = rev
                list_done.append(rev)
                break
                
structure_l = []
for elem in structure:
    structure_l.append(' ')
    
for k, v in dict_pairs.iteritems():
    structure_l[k] = v
    structure_l[v] = k

while True:
    if left == right:
        lay3 += sequence[left]
        break
    
    elif left > right:
        break
    
    elif structure_l[left] == right and structure_l[right] == left:
        lay1 += ' '
        lay2 += sequence[left]
        
        list_temp = [sequence[left].upper(), sequence[right].upper()]
        print list_temp
        if ('A' in list_temp and 'U' in list_temp) or ('C' in list_temp and 'G' in list_temp):
            lay3 += '|'
        else:
            lay3 += ':'
            
        lay4 += sequence[right]
        lay5 += ' '
        
        left += 1
        right -= 1
    
    elif right-left == 1 and structure_l[left] == ' ' and structure_l[right] == ' ':
        lay1 += ' '
        lay2 += sequence[left]
        lay3 += ' '
        lay4 += sequence[right]
        lay5 += ' '
        
        break

        
    elif structure_l[left] == ' ':
        if structure_l[right] == ' ':
            r2 = 0
            for idx2 in xrange(left+1, len(structure)):
                if not structure_l[idx2] == ' ':
                    r2 = structure_l[idx2]
                    break
            l2 = 0    
            for idx2 in reversed(xrange(right)):
                if not structure_l[idx2] == ' ':
                    l2 = structure_l[idx2]
                    break
            
            if l2-left == right-r2 or l2-left == 1 or r2-right == 1:
                lay1 += sequence[left]
                lay2 += ' '
                lay3 += ' '
                lay4 += ' '
                lay5 += sequence[right]
                
                right -= 1
                left += 1
                continue
        
        lay1 += sequence[left]
        lay2 += ' '
        lay3 += ' '
        lay4 += ' '
        lay5 += '-'
        
        left += 1
        
    elif structure_l[right] == ' ':
        lay1 += '-'
        lay2 += ' '
        lay3 += ' '
        lay4 += ' '
        lay5 += sequence[right]
        
        right -= 1
        
        
print '{lay1}\n{lay2}\n{lay3}\n{lay4}\n{lay5}'.format(lay1=lay1,
                                                      lay2=lay2,
                                                      lay3=lay3,
                                                      lay4=lay4,
                                                      lay5=lay5)