import os
import subprocess
import shlex

def call_command(command, pipe=None, echo = False):
    if echo:
        print command
    
    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    
    output=process.communicate(input=pipe)
    return output

#file_a = open('/u/leongs/Desktop/workdir_temp/0568ef1a-ff1b-4047-ae13-fab2f53fc92f/', 'r')
##file_a = open('/u/leongs/Desktop/workdir_temp/500/TIZMmZZ8MF/output_marc2', 'r')
#file_b = open('/u/leongs/Desktop/workdir_temp/output_9999', 'r')

map_a = dict()
map_b = dict()

folder_one = '/u/leongs/Desktop/workdir_temp/db0480ae-2794-4d36-87f1-092c4e9ee5c2/'
folder_two = '/u/leongs/Desktop/workdir_temp/824b08ca-ec5f-40a3-a82c-cbe017951275/'


list_one = os.listdir(folder_one)
list_two = os.listdir(folder_two)
list_one.sort()
for fichier in list_one:
    if 'pdb.gz' in fichier:
        if fichier in list_two:
            call_command('gunzip ' + os.path.join(folder_one, fichier))
            call_command('gunzip ' + os.path.join(folder_two, fichier))
            print 'comparing {0} with {1}'.format(os.path.join(folder_one, fichier), os.path.join(folder_two, fichier))
            file_a = open(os.path.join(folder_one, fichier.replace('.pdb.gz', '.pdb')), 'r')
            file_b = open(os.path.join(folder_two, fichier.replace('.pdb.gz', '.pdb')), 'r')
            
            text_a = file_a.read()
            text_b = file_b.read()

            if text_a == text_b:
                pass
            else:
                print 'not same'
                break
            
            call_command('gzip ' + os.path.join(folder_one, fichier.replace('.pdb.gz', '.pdb')))
            call_command('gzip ' + os.path.join(folder_two, fichier.replace('.pdb.gz', '.pdb')))
        else:
            print 'not same'

print 'same'