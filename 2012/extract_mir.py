#import os
#
#threeDwork = "/u/mokdada/public_html/miRNA/3Dwork"
#miRNA_file = "/u/leongs/Desktop/mirbase_seq.txt"
#
#list_folder = os.listdir(threeDwork)
#list_folder.sort()
#for elem in list_folder:
#    file_path = os.path.join(threeDwork, elem, 'MCSYM')
#    
#    if os.path.exists(file_path):
#        mcc_file = os.path.join(file_path, elem+'.mcc')
#        if os.path.exists(mcc_file):
#            with open(mcc_file, 'r') as mcc_cont:
#                bStruct = False
#                sequence = None
#                struct = None
#                for line in mcc_cont:
#                    if sequence and struct:
#                        print sequence
#                        print struct
#                    if 'sequence' in line:
#                        splitted = line.split()
#                        sequence = splitted[3]
#                        bStruct = True
#                    elif bStruct:
#                        splitted = line.split()
#                        structure = splitted[1]
#                        break
#            pass
#        
#        else:
#            print 'no mcc file in '+file_path
#            print os.listdir(file_path)
#            
##sequence_file = "/u/mokdada/public_html/miRNA/Sequences.txt"
##miRNA_file = "/u/leongs/Desktop/mirbase_seq.txt"
##
##with open(sequence_file, 'r') as sequence_cont:
##    list_seq = []
##    for line in sequence_cont:
##        if not '>' in line:
##            sequence = line.strip()
##            list_seq.append(sequence)
##            print sequence
##            match = None
##            with open(miRNA_file, 'r') as mirna_cont:
##                for mirna_line in mirna_cont:
##                    if sequence in mirna_line:
##                        print mirna_line
##    print len(list_seq)
##            if not match:
##                print 'No match'




import os
import subprocess
import shlex

threeDwork = "/u/mokdada/public_html/miRNA/3Dwork"
miRNA_file = "/u/leongs/Desktop/mirbase_seq.txt"

def call_command(command, pipe=None, echo = False):
    if echo:
        print command
    
    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    
    output=process.communicate(input=pipe)
    return output


if __name__ == '__main__':
    list_mir = os.listdir('/u/leongs/Documents/miRNA/best')
    os.chdir("/u/leongs/Documents/miRNA")
    list_mir.sort()
    count = 0
    for mir in list_mir:
        print "processing " + mir
        mir_path = os.path.join('/u/leongs/Documents/miRNA/best', mir)
        
        sequence_name = mir.replace(".pdb", "")
        if ".pdb" in mir:
            os.environ['LD_LIBRARY_PATH'] = '/u/leongs/Desktop/projet_bench/pdb2db/lib64/'
            os.environ['LIBRARY_PATH'] = '/u/leongs/Desktop/projet_bench/pdb2db/lib64/'
            out, err = call_command("/u/leongs/Desktop/projet_bench/pdb2db/pdb2db-0.1.0 -g -p -c 1 " + mir_path)
            new_out = out.replace('()', '..')
                
            if err:
                print err
            
            bKeep= True
            splitted = new_out.split('\n')
            if len(splitted) != 3:
                print len(splitted)
                print mir
                
            try:
                if '[' in splitted[2]:
                    count += 1
                    bKeep = False
                    print '[ in ' + mir
                if 'X' in splitted[1] or 'x' in splitted[1]:
                    count += 1
                    bKeep = False
                    print 'X in ' + mir
            except Exception as e:
                print e
                print mir
                
            print new_out
                
#            if bKeep:
#                with open(os.path.join("/u/leongs/Desktop/projet_bench/selected_pdb2/output/", sequence_name), 'w') as output:
#                    output.write(new_out)
            