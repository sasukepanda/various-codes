import os
import shutil
import subprocess
import shlex
import argparse


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--upper', action="store", dest="upper")
    parser.add_argument('--lower', action="store", dest="lower")

    ns = parser.parse_args()

    upper = int(ns.upper)
    lower = int(ns.lower)
    index = str(lower) + "_" + str(upper)

    MCSYM_DB = "/u/leongs/Downloads/MC-Sym_Linux/MCSYM-DB"
    workdir = "/u/leongs/Desktop/test_scriptgen/new_workdir/workdir_"+index

    if not os.path.exists(workdir):
        os.mkdir(workdir)

    # make the symling to MCSYM_DB
    os.symlink(MCSYM_DB, os.path.join(workdir, 'MCSYM-DB'))

    os.chdir(workdir)
    os.environ["MCSYM_DB"] = "/soft/bioinfo/share/mcsym/db/mcsymdb-4.2.1.bin.gz"

    for scr in sorted(os.listdir("/u/leongs/Desktop/test_scriptgen/scripts")):
        ind = int(scr.replace(".mcc", "").replace("struct_", ""))
        if lower <= ind < upper:
            script = os.path.join("/u/leongs/Desktop/test_scriptgen/new_workdir/new_scripts", scr)
            # copy the script file
            shutil.copy(script, os.path.join(workdir, "script.mcc"))
        
            try:
                out, err = call_command('/u/admc/soft/MCSYM-soft/cgi-bin/MC-Sym/prep_mcsym.exe script.mcc')
                if err:
                    print scr
                    print("prep_mcsym: {msg}".format(msg=err))
            except Exception as e:
                print (str(e))

            # run MCSYM
            out, err = call_command("/soft/bioinfo/linux_RH5/mcsym-4.2.2/bin/mcsym script.mcc")

            if err:
                print scr
                print err

            list_pdb = sorted([elem for elem in os.listdir(workdir) if elem.endswith("pdb.gz")])
            list_dir = [elem for elem in os.listdir(workdir) if elem != "MCSYM-DB"]
            if not list_dir and not err and not list_pdb:
                print scr
                print "no PDB generated with " + scr
            elif list_pdb:
                shutil.copy(os.path.join(workdir, list_pdb[0]),
                            os.path.join("/u/leongs/Desktop/test_scriptgen/new_workdir/decoy_bank", list_pdb[0]))
                with open(os.path.join("/u/leongs/Desktop/test_scriptgen/new_workdir/decoy_bank", scr.replace(".mcc", ".out")), 'wb') as out_f:
                    out_f.write(out)
            if list_dir:
                for elem in list_dir:
                    os.remove(elem)