import os
import shutil
import subprocess
import shlex


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output


root_dir = "/export/home/leongs/mcweb-1.10_prod_oct24"
list_workdir = os.listdir(root_dir)

dict_seq = dict()
for workid in sorted(list_workdir):
    workdir = os.path.join(root_dir, workid)

    if os.path.isdir(workdir):
        # try to find mcfold.out or mcfold2.out or mcflashfold.out in the workdir
        list_content = os.listdir(workdir)
        in_dir = False
        for out_file in ["mcfold.out", "mcfold2.out", "mcflashfold.out"]:
            if out_file in list_content:
                in_dir = True
                break
        if not in_dir:
            shutil.rmtree(workdir)
    else:
        os.remove(workdir)

    to_be_deleted = False
    for tool in ["mcfold", "mcfold2", "mcflashfold"]:
        out_file = os.path.join(workdir, tool + "_result.out")
        if os.path.exists(out_file):
            with open(out_file, 'rb') as out_cont:
                content = [elem.strip() for elem in out_cont.readlines()]
                seq = content[1]
                if "(" in seq:
                    to_be_deleted = True
                    break
                struct = [elem.strip().split()[0] for elem in content[2:]]
                if seq not in dict_seq:
                    dict_seq[seq] = set()
                for s in struct:
                    dict_seq[seq].add(s)
    if to_be_deleted:
        shutil.rmtree(workdir)


index = 1
for seq, list_struct in dict_seq.iteritems():
    for struct in list_struct:
        if struct.count("(") < 2:
            continue
        # make seq str
        if "-" in seq:
            splitted = seq.split("-")
            seq_str = "--sequence1 {s1} --sequence2 {s2}".format(s1=splitted[0],
                                                                 s2=splitted[-1])
            # do the struct part
            struct1 = struct[:len(splitted[0])]
            struct2 = struct[-len(splitted[-1]):]
            seq_str += ' --structure1 "{st1}" --structure2 "{st2}"'.format(st1=struct1,
                                                                           st2=struct2)
        else:
            seq_str = '--sequence1 {s} --structure1 "{st}"'.format(s=seq, st=struct)

        name = "struct_{n:05d}".format(n=index)

        cmd_line = ('python /u/leongs/workspace/mcweb/script/new_mcsymize.py '
                    '--name {name} {seq_struct} --use_high_res_ncm '
                    '--db_path /u/leongs/Downloads/MC-Sym_Linux/MCSYM-DB '
                    '--max_number 1 '
                    '>/u/leongs/Desktop/test_scriptgen/new_scripts/{name}.mcc').format(name=name,
                                                                                       seq_struct=seq_str)

        print "echo " + name
        print cmd_line
#         with open(os.path.join("/u/leongs/Desktop/test_scriptgen", "list_command.txt"), "a") as cmd_f:
#             cmd_f.write(cmd_line + "\n")

#         out, err = call_command(cmd_line)
#         if err:
#             with open(os.path.join("/u/leongs/Desktop/test_scriptgen", "err_output.txt"), "a") as err_f:
#                 err_f.write(cmd_line + "\n")
#                 err_f.write(err + "\n")
# 
#         if out:
#             with open(os.path.join("/u/leongs/Desktop/test_scriptgen/scripts", name + ".mcc"), "wb") as script_f:
#                 script_f.write(out)
#         else:
#             print "#no out"
#             print cmd_line

        index += 1