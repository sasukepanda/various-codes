import os
import multiprocessing
import subprocess
import shlex


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output


def call_mcsymize(line):
    splitted = line.split(">")
    cmd = splitted[0]
    destination = splitted[1]
    
    out, err = call_command(cmd)
    if out:
        with open(destination, 'wb') as dest_o:
            dest_o.write(out)

if __name__ == '__main__':
    list_line = []
    with open("/u/leongs/Desktop/test_scriptgen/list_commandnew.bat", 'rb') as cmd_c:
        list_line = [elem.strip() for elem in cmd_c.readlines() if elem.strip() and not elem.startswith("echo")]

    for line in list_line:
        print line
        call_mcsymize(line)

#     pool = multiprocessing.Pool(3)
#     pool.map(call_mcsymize, list_line, chunksize=1)
