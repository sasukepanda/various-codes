import os
import shutil
import subprocess
import shlex
import argparse


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--index', action="store", dest="index")
    parser.add_argument('--workdir', action="store", dest="workdir")
    parser.add_argument('--script_dir', action="store", dest="script_dir")
    parser.add_argument('--decoy_bank', action="store", dest="decoy_bank")

    ns = parser.parse_args()

    index = int(ns.index)
    workdir = ns.workdir
    script_dir = ns.script_dir
    decoy_bank = ns.decoy_bank

    MCSYM_DB = "/u/leongs/Downloads/MC-Sym_Linux/MCSYM-DB"

    # make the symling to MCSYM_DB
    os.symlink(MCSYM_DB, os.path.join(workdir, 'MCSYM-DB'))

    os.chdir(workdir)
    os.environ["MCSYM_DB"] = "/soft/bioinfo/share/mcsym/db/mcsymdb-4.2.1.bin.gz"

    for scr in sorted(os.listdir(script_dir)):
        ind = int(scr.replace(".mcc", "").replace("struct_", ""))
        if ind == index:
            script = os.path.join(script_dir, scr)
            # copy the script file
            shutil.copy(script, os.path.join(workdir, "script.mcc"))
        
            try:
                out, err = call_command('/u/admc/soft/MCSYM-soft/cgi-bin/MC-Sym/prep_mcsym.exe script.mcc')
#                 if err:
#                     print scr
#                     print("prep_mcsym: {msg}".format(msg=err))
            except Exception as e:
                print (str(e))

            # run MCSYM
            out, err = call_command("/soft/bioinfo/linux_RH5/mcsym-4.2.2/bin/mcsym script.mcc")

            if err:
                print scr
                print err

            list_pdb = sorted([elem for elem in os.listdir(workdir) if elem.endswith("pdb.gz")])
            list_dir = [elem for elem in os.listdir(workdir) if elem != "MCSYM-DB"]
            if not list_dir and not err and not list_pdb:
                print scr
                print "no PDB generated with " + scr
            elif list_pdb:
                shutil.copy(os.path.join(workdir, list_pdb[0]),
                            os.path.join(decoy_bank, list_pdb[0]))
            if list_dir:
                for elem in list_dir:
                    os.remove(elem)