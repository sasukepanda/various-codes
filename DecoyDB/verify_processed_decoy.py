import os

processed_decoy_dir = "/u/leongs/reproduction_projet_naim/rel20/3D/processed/decoy"

list_decoys = os.listdir(processed_decoy_dir)

for decoy in sorted(list_decoys):
    out_dir = os.path.join(processed_decoy_dir, decoy, "out")
    if len(os.listdir(out_dir)) < 5:
        print decoy