import os
import cPickle


flashfold_dir = "/u/leongs/reproduction_projet_naim/rel20/2D/filtered"
workdir = "/u/leongs/reproduction_projet_naim/rel20/3D"
pbs_dir = "/u/leongs/reproduction_projet_naim/rel20/3D/pbs"
digested_data_pk = "/u/leongs/reproduction_projet_naim/rel20/2D/digested_data.pk"
path_mcsymizer = "/u/leongs/workspace/mcweb/script/mcsymizer.py"


def write_pbs_script(list_command, name, index):
    modulo_val = 1
    pbs_subdir = os.path.join(pbs_dir, str(index%modulo_val + 1))
    if (index%modulo_val + 1):
        if not os.path.exists(pbs_subdir):
            os.mkdir(pbs_subdir)
        pbs_folder = os.path.join(pbs_subdir, "pbs")
        if not os.path.exists(pbs_folder):
            os.mkdir(pbs_folder)
        if list_command:
            commands = ('#!/bin/sh\n'
                        '#PBS -N {name}\n'
                        '#PBS -l walltime=10000:00:00\n'
                        'export MCSYM_DB=/soft/bioinfo/share/mcsym/db/mcsymdb-4.2.1.bin.gz\n'
                        '{commands}').format(name=name,
                                             commands="\n".join(list_command))

            with open(os.path.join(pbs_folder, name + ".pbs"), 'wb') as pbs_f:
                pbs_f.write(commands)

            with open(os.path.join(pbs_subdir, "list.txt"), 'a') as list:
                list.write(os.path.join(pbs_folder, name + ".pbs") + '\n')

            with open(os.path.join(pbs_subdir, "execute.bash"), 'wb') as exec_bash:
                exec_bash.write("#!/bin/bash\n$* > /dev/null")

            with open(os.path.join(pbs_subdir, "launch.bash"), 'wb') as pbs_f:
                pbs_f.write("#!/bin/bash\ncat {list} | xargs --max-args=1 --max-procs=$* {execute_bash} \n".format(list=os.path.join(pbs_subdir, "list.txt"),
                                                                                                                   execute_bash=os.path.join(pbs_subdir, "execute.bash")))
    
            with open(os.path.join(pbs_subdir, "launch_pbs.pbs"), 'wb') as pbs_f:
                pbs_f.write(("#!/bin/bash\n"
                             "#PBS -N {name}\n"
                             "#PBS -l walltime=10000:00:00\n"
                             "#PBS -o {stdout_filename}\n"
                             "#PBS -e {stderr_filename}\n"
                             "/u/leongs/reproduction_projet_naim/rel20/3D/pbs/{name}/launch.bash 8").format(stdout_filename=os.path.join(pbs_subdir, "out.txt"),
                                                                                                            stderr_filename=os.path.join(pbs_subdir, "err.txt"),
                                                                                                            name=str(index%modulo_val + 1)))


list_digested_data = []
with open(digested_data_pk, 'rb') as dd:
        list_digested_data = cPickle.load(dd)


list_command = []
for index, hairpin_dict in enumerate(list_digested_data):
    list_command = []

    hairpin_name = hairpin_dict['name']
    hairpin_acc = hairpin_dict['accession']
    hairpin_seq = hairpin_dict['sequence']
    mcfold_output = os.path.join(flashfold_dir, hairpin_acc)

    hairpin_main_dir = os.path.join("/u/leongs/reproduction_projet_naim/rel20/3D/decoys", hairpin_acc)

    list_command.append("mkdir {decoy_dir}".format(decoy_dir=hairpin_main_dir))

    with open(mcfold_output, 'rb') as mcfold_o:
        for i, line in enumerate(mcfold_o):
            if line.strip():
                structure = line.split()[0]
            if i >= 100:
                break
            else:
                workdir = os.path.join(hairpin_main_dir, hairpin_acc + "_" + str(i))
                list_command.append("mkdir "+workdir)
                list_command.append("cd "+ workdir)
                # make mcsym_script
                cmd = ('python {mcsymizer} --db_path {db_path} '
                       '--sequence1 {seq} --structure1 "{struct}" '
                       '--use_high_res_ncm '
                       '--max_number 500 '
                       '--name {acc}_{i} '
                       '--timeout 30 > {script_file}').format(mcsymizer=path_mcsymizer,
                                                                  db_path="/u/leongs/Downloads/MC-Sym_Linux/MCSYM-DB",
                                                                  seq=hairpin_seq,
                                                                  struct=structure,
                                                                  acc=hairpin_acc,
                                                                  i=i,
                                                                  script_file=os.path.join(workdir, "script.mcc"))

                list_command.append(cmd)

                cmd = "/soft/bioinfo/linux_RH5/mcsym-4.2.2/bin/mcsym script.mcc"
                list_command.append(cmd)
    write_pbs_script(list_command, hairpin_acc, index)