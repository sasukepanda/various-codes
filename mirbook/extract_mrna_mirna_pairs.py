import os

i_data = "/u/admc/public_data/mirbook/initial_data"

list_cl = [elem for elem in os.listdir(i_data) if os.path.isdir(os.path.join(i_data, elem))]


set_pairs = set()
for cl in list_cl:
    cl_dir = os.path.join(i_data, cl, 'results')
    with open(os.path.join(cl_dir, cl+"_wt"), 'rb') as cl_out:
        for line in cl_out:
            splitted = line.strip().split()
            if len(splitted) == 7:
                set_pairs.add("{g_acc}\t{g_name}\t{m_acc}\t{m_name}\t{pos}\t{hp}".format(g_acc=splitted[0],
                                                                                         g_name=splitted[1],
                                                                                         m_acc=splitted[2],
                                                                                         m_name=splitted[3],
                                                                                         pos=splitted[4],
                                                                                         hp=splitted[5]))

print "\n".join(sorted(list(set_pairs)))