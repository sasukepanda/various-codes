import os
import shutil
import cPickle

working_directory = "/u/leongs/debug_frc/9cc60edf-1986-4f3f-b312-eed1feed1839"

# make sure all the files are present
sample_info_pk_path = os.path.join(working_directory, 'sample_info.pk')
output_filepath = os.path.join(working_directory, 'mirbooking_out')
digested_mrna_filepath = os.path.join(working_directory, 'processed_res', 'digested_mrna.qty')
digested_mirna_filepath = os.path.join(working_directory, 'processed_res', 'digested_mirna.qty')


# create the sample entry
sample_info_dict = dict()

with open(sample_info_pk_path, 'rb') as sample_pk:
    sample_info_dict = cPickle.load(sample_pk)

sample_name = sample_info_dict['sample_name']

sample_id = "e25b72ae-10a2-4027-9122-d32f2d4e18f8"
sample_name = "HeLa_miR-515-3p(1000)"

# read the processed directory
#copy the output file
shutil.copy(os.path.join(working_directory, 'mirbooking_out'),
            os.path.join("/u/admc/data/mcweb-1.10_prod/mirbook_data_files/samples", 
                         sample_id, 
                         "{sample_name}.mirbooking.out".format(sample_name=sample_name.replace(' ', '_'))))

os.chdir(working_directory)
archive_path = os.path.join(working_directory, 'processed_res.tar.bz2')

try:
    #copy the pickle files
    for elem in os.listdir(os.path.join(working_directory, 'processed_res')):
        if elem.endswith(".pk"):
            shutil.copy(os.path.join(working_directory, 'processed_res', elem),
                os.path.join("/u/admc/data/mcweb-1.10_prod/mirbook_data_files/samples",
                             sample_id,
                             "{sample_name}_{f_name}".format(sample_name=sample_name.replace(' ', '_'),
                                                             f_name=elem)))
    # copy the qty files
    shutil.copy(digested_mrna_filepath,
                os.path.join("/u/admc/data/mcweb-1.10_prod/mirbook_data_files/samples",
                             sample_id,
                             "{sample_name}_mrna.qty".format(sample_name=sample_name.replace(' ', '_'))))

    shutil.copy(digested_mirna_filepath,
                os.path.join("/u/admc/data/mcweb-1.10_prod/mirbook_data_files/samples",
                             sample_id,
                             "{sample_name}_mirna.qty".format(sample_name=sample_name.replace(' ', '_'))))

    #update sample's min and max
    with open(os.path.join(working_directory, 'processed_res', 'mirna_stats.pk'), 'rb') as mirna_stat_pk:
        temp_dict = cPickle.load(mirna_stat_pk)
        print "sample.mirna_max_qty", temp_dict['max']
        print "sample.mirna_min_qty", temp_dict['min']
        print "sample.mirna_avg_qty", temp_dict['avg']
        print "sample.mirna_median_qty", temp_dict['median']
    with open(os.path.join(working_directory, 'processed_res', 'gene_stats.pk'), 'rb') as gene_stats_pk:
        temp_dict = cPickle.load(gene_stats_pk)
        print "sample.gene_max_qty", temp_dict['max']
        print "sample.gene_min_qty", temp_dict['min']
        print "sample.gene_avg_qty", temp_dict['avg']
        print "sample.gene_median_qty", temp_dict['median']

    # copy sample_info file
    shutil.copy(sample_info_pk_path,
                os.path.join("/u/admc/data/mcweb-1.10_prod/mirbook_data_files/samples", 
                             sample_id, 
                             "{sample_name}_sample_info.pk".format(sample_name=sample_name.replace(' ', '_'))))

except Exception as e:
    print e