import cPickle
import os
import json
import random

def print_json(gene_dict, acc, processed_dir, final_dict):
    list_mirna = []
    dict_to_print = dict(length=0)
    if os.path.exists(os.path.join(processed_dir, "detailed_{acc}_results_list.pk".format(acc=acc))):
        with open(os.path.join(processed_dir, "detailed_{acc}_results_list.pk".format(acc=acc)), 'rb') as detailed_pk:
            list_mirna = cPickle.load(detailed_pk)

#         if len(final_dict.get(str(len(list_mirna)), [])) < 5:
        if True:
            dict_to_print = dict(length=len(list_mirna),
                                 json= json.dumps(dict(gene={"name":gene_dict["name"],
                                                             "accession":gene_dict["accession"],
                                                             "length":int(gene_dict["gene_length"]),
                                                             "cds_start":int(gene_dict["gene_cds_start"]),
                                                             "cds_end":int(gene_dict["gene_cds_end"]),
                                                             "quantity":int(round(float(gene_dict["quantity"]))),
                                                             "percentile":int(gene_dict["percentile"])},
                                                       microrna=sorted([{"name":mir["mirna_name"],
                                                                         "accession":mir["mirna_accession"],
                                                                         "target_type":mir["target_type"],
                                                                         "effectiveness":float(mir["effectiveness"]),
                                                                         "quantity":int(round(float(mir["mirna_quantity"]))),
                                                                         "percentile":int(mir["mirna_percentile"]),
                                                                         "number":int(mir["number"]),
                                                                         "position":int(mir["position"]),
                                                                         "alignment": "{gene}\n{mir}".format(gene=mir["gene_mirna_binding_dict"]["gene"],
                                                                                                             mir=mir["gene_mirna_binding_dict"]["microrna"]),
                                                                         "sum_effectiveness": sum([float(submir["effectiveness"]) for submir in list_mirna if submir["mirna_accession"]==mir["mirna_accession"]])}\
                                                                         for mir in list_mirna if mir["gene_accession"]==acc],
                                                                       key=lambda el: el["sum_effectiveness"],
                                                                       reverse=True)[:15]))) # only keep 15 most effectives
    return dict_to_print

# data_dir = "/u/admc/public_data/mirbook/initial_data"
data_dir = "/u/leongs/mcweb_shared/new_mirbook/results"

list_cell_lines = [elem for elem in os.listdir(data_dir) if os.path.isdir(os.path.join(data_dir, elem))]


final_dict = dict()

for cell_line in sorted(list_cell_lines):
    processed_dir = os.path.join(data_dir, cell_line, "processed", "processed_res")
    curr_gene_dict = dict()
    with open(os.path.join(processed_dir, "gene_dict_qty.pk"), 'rb') as gene_qty:
        curr_gene_dict = cPickle.load(gene_qty)

    list_gene_accession = curr_gene_dict.keys()
    random.shuffle(list_gene_accession)

    for gene_accession in list_gene_accession:
        gene_dict = curr_gene_dict[gene_accession]
        curr_dict = print_json(gene_dict, gene_accession, processed_dir, final_dict)
        if curr_dict["length"] > 0:
            if not str(curr_dict["length"]) in final_dict:
                final_dict[str(curr_dict["length"])] = []
            final_dict[str(curr_dict["length"])].append(dict(cell_line=cell_line, acc=gene_accession, json=curr_dict["json"]))

#     if cell_line == "DU145":
#         print_json(curr_gene_dict["NM_000314"], "NM_000314", processed_dir)
#     elif cell_line == "SF268":
#         print_json(curr_gene_dict["NM_018557"], "NM_018557", processed_dir)

keys_list = sorted([int(elem) for elem in final_dict.keys()])

# print sum([len(val) for val in final_dict.values()])

for key in keys_list:
#     list_randomized = list(final_dict[str(key)])
#     random.shuffle(list_randomized)
    for i, c_dict in enumerate(final_dict[str(key)]):
        print "####################################################"
        print c_dict["cell_line"], c_dict["acc"], key
        print c_dict["json"]