import os
import argparse
import subprocess
import shlex


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output


def generate_ss_images(curr_f_dir, accession, sequence, structure):
    #generate FASTA
    suffix = ">{accession}|{seq}|{struct}".format(accession=accession, seq=sequence, struct=structure)
     
    out, err = call_command("curl http://www-lbit.iro.umontreal.ca/cgi-bin/mcfold/render.cgi?structure={suff}".format(suff=suffix),
                            echo=False)
    splitted_out = out.split()

    if not os.path.exists(os.path.join(curr_f_dir, accession)):
        os.mkdir(os.path.join(curr_f_dir, accession))
 
    for elem in splitted_out:
        if elem.startswith('HREF="/mcfold/rundata/'):
            url = elem.replace('HREF="', '"http://www-lbit.iro.umontreal.ca')
            os.chdir(os.path.join(curr_f_dir, accession))

            out, err = call_command("wget "+ url, echo=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--workdir', action="store", dest="workdir")
    parser.add_argument('--img_dir', action="store", dest="img_dir")
    parser.add_argument('--ss_dir', action="store", dest="ss_dir")

    ns = parser.parse_args()
    workdir = ns.workdir
    img_dir = ns.img_dir
    ss_dir = ns.ss_dir

    list_acc = os.listdir(workdir)

    count_y = 0
    count_weird = 0
    count_hairpin = 0
    dict_shape = dict()
    list_y = []
    for acc in sorted(list_acc):
        mir_dir = os.path.join(workdir, acc)
        if not os.path.exists(os.path.join(mir_dir, acc + ".fa")) or not os.path.exists(os.path.join(ss_dir, acc + ".ss")):
            continue
        
        seq = ""
        struct = ""
        with open(os.path.join(mir_dir, acc + ".fa"), 'rb') as seq_c:
            seq = seq_c.readlines()[1].strip()
        with open(os.path.join(ss_dir, acc + ".ss"), 'rb') as struct_c:
            struct = struct_c.read().strip()

        t5, err = call_command('RNAshapes -D "{struct}" -t5'.format(struct=struct.replace("*", ".")))
        if t5.count("[") > 1 and t5.strip() != "[[][]]":
            count_weird += 1
            if t5.strip() not in dict_shape:
                dict_shape[t5.strip()] = []
            dict_shape[t5.strip()].append(">{acc}\n{seq}\n{struct}\n".format(acc=acc,
                                                                             seq=seq.strip(),
                                                                             struct=struct.strip()))
            if not os.path.exists(img_dir):
                os.mkdir(img_dir)
            generate_ss_images(img_dir, acc, seq, struct)
        elif t5.strip() == "[[][]]":
            count_y += 1
            list_y.append(">{acc}\n{seq}\n{struct}\n".format(acc=acc,
                                                             seq=seq.strip(),
                                                             struct=struct.strip()))
        elif t5.strip() == "[]":
            count_hairpin += 1

    print "count_weird: ", count_weird
    for sh, ct in dict_shape.iteritems():
        print sh, len(ct)
        for elem in ct:
            print elem
    
    print "#####################################################################################"
    
    print "count Y: ", count_y
    print "\n".join(list_y)
    
    print "count hairpin: ", count_hairpin
    print "number of struct: ", len(list_acc)