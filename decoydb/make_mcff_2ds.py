import os
import shutil
import argparse
import sys
import subprocess
import shlex
import time

def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--workspace', action="store", dest="workspace")
    parser.add_argument('--desired_length', action="store", dest="desired_length")
    parser.add_argument('--threads', action="store", dest="threads")

    ns = parser.parse_args()
    workspace = ns.workspace
    desired_length = int(ns.desired_length)
    threads = int(ns.threads)
    starting_threshold = 5

    os.chdir(workspace)

    struct_list = []
    with open(os.path.join(workspace, 'filelist'), 'rb') as f_list:
        struct_list = [elem.strip() for elem in f_list.readlines() if elem.strip()]

    while struct_list:
        # edit the make2d.bash file
        new_cmd = ('#!/bin/bash\n'
                   '/u/admc/MC-Flashfold/mcff --tables /u/admc/MC-Flashfold/tables -s '
                   '"$(tail -n-1 hairpin/$*)" -um "$(./maskgen.pl hairpin/$* mature/$*)" '
                   '-t {threshold} -ns > ./2d/$*').format(threshold=starting_threshold)
        print "threshold", starting_threshold
        starting_threshold += 0.5

        with open("make2D.bash", 'wb') as make2d_s:
            make2d_s.write(new_cmd)
        
        # call 2Dgen
        out, err = call_command("2Dgen.bash {thr}".format(thr=threads), echo=True)
        if err:
            print err
            break
        
        # sleep for 10secs
        time.sleep(10)
        
        # call 2Dfilter
        out, err = call_command("2Dfilter.sh {thr}".format(thr=threads), echo=True)
        if err:
            print err
            break
        
        # sleep for 10secs
        time.sleep(10)
        
        # empty 2d directory and replace with 2dclean
        shutil.rmtree("2d")
        os.rename("2dclean", "2d")

        os.mkdir("2dclean")
        
        #clean
        out, err = call_command(("python /u/leongs/git/various-codes/decoydb/clean_mcff_out_folder.py "
                                 "--mcff_out_dir /u/leongs/projet_naim/mcff/2d/ --threads {thr} "
                                 "--size {desired_length}").format(thr=threads,
                                                                   desired_length=desired_length), echo=True)
        if err:
            break
        
        struct_list = [elem.strip() for elem in out.splitlines() if elem.strip()]
        
        # write the filelist file
        with open(os.path.join(workspace, 'filelist'), 'wb') as f_list:
            f_list.write(out)

        # sleep for 10secs
        time.sleep(10)