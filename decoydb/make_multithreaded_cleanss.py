import os
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--mcff_out', action="store", dest="mcff_out")
    parser.add_argument('--hairpin', action="store", dest="hairpin")
    parser.add_argument('--mature', action="store", dest="mature")
    parser.add_argument('--accession', action="store", dest="accession")

    ns = parser.parse_args()
    mcff_out = ns.mcff_out
    hairpin = ns.hairpin
    mature = ns.mature
    accession = ns.accession

    