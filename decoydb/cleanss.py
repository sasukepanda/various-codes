import os
import argparse
import sys
import shlex

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--ssfile', action="store", dest="ssfile")
#     parser.add_argument('--seqfile', action="store", dest="seqfile")
#     parser.add_argument('--matfile', action="store", dest="matfile")
    parser.add_argument('--destination', action="store", dest="destination")

    ns = parser.parse_args()
    ssfile = ns.ssfile
#     seqfile = ns.seqfile
#     matfile = ns.matfile
    destination = ns.destination

    list_struct = []
    with open(ssfile, 'rb') as ss_c:
        list_struct = list(set([elem.strip() for elem in ss_c.readlines() if elem.strip()]))
    list_struct.sort(key=lambda elem: float((elem.split())[1]))

    last_energy = 0
    new_list_lines = []
    for l in list_struct:
        splitted = l.split()
        energy = float(splitted[1])
        if energy == last_energy or len(new_list_lines) < 110000:
            new_list_lines.append(l)
            last_energy = energy
        else:
            break
    new_list_lines.append("\n")
    with open(destination, 'wb') as dest_c:
        dest_c.write("\n".join(new_list_lines))

#     seq = ""
#     with open(seqfile, 'rb') as seq_c:
#         seq = seq_c.readlines()[1].strip()

#     mature5 = ""
#     mature3 = ""
#     with open(matfile, 'rb') as mat_c:
#         list_lines = mat_c.readlines()
#         index = 0
#         while index < len(list_lines)-1:
#             if list_lines[index].strip().endswith("5p"):
#                 mature5 = list_lines[index+1].strip()
#                 index +=1
#             elif list_lines[index].strip().endswith("3p"):
#                 mature3 = list_lines[index+1].strip()
#                 index +=1
#             index +=1
# 
#     printed = 0
#     last_energy = 0
#     for struct in list_struct:
#         if mature5:
#             start_ind = seq.find(mature5)
#             temps = ""
#             for i in xrange(start_ind, start_ind+len(mature5)):
#                 if struct[i] == ")":
#                     continue
# 
#         if mature3:
#             start_ind = seq.find(mature3)
#             temps = ""
#             for i in xrange(start_ind, start_ind+len(mature3)):
#                 if struct[i] == "(":
#                     continue
# 
#         energy = float(struct.split()[1])
#         if printed > 110000 and last_energy != energy:
#             break
#         else:
#             print struct
#             printed += 1
#         last_energy = energy