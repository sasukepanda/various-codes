import os
import subprocess
import shlex
import multiprocessing


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output


project_dir = "/u/leongs/projet_naim/mcff"
mcff_out_dir = os.path.join(project_dir, "2dclean")
new_out_dir = os.path.join(project_dir, "new_2d")

command_file = "/u/leongs/projet_naim/list_commands_mcff.txt"

dict_mir = dict()

def process_mcff_list(mcff_out):
    list_lines = []
    with open(os.path.join(mcff_out_dir, mcff_out), 'rb') as mcff_o:
        list_lines = list(set([elem.strip() for elem in mcff_o.readlines() if elem.strip()]))
        threshold = 7
        if len(list_lines) < 100000:
            print mcff_out
#             while len(list_lines) < 500000:
#                 if mcff_out == "MI0006366":
#                     threshold += 0.1
#                 else:
#                     threshold += 1
#                 print "not enough structure for {name} ({len}), trying with threshold {thr}".format(name=mcff_out,
#                                                                                                     len=len(list_lines),
#                                                                                                     thr=threshold)
#                 del list_lines
#                 cmd_st = ('/u/admc/MC-Flashfold/mcff --tables /u/admc/MC-Flashfold/tables -s '
#                           '{seq} -um {mask} -t {threshold} -ns').format(seq=dict_mir[mcff_out]["seq"],
#                                                                         mask=dict_mir[mcff_out]["mask"],
#                                                                         threshold=threshold)
#                 out, err = call_command(cmd_st, echo=False)
#                 list_lines = list(set([elem.strip() for elem in out.splitlines() if elem.strip()]))
#                 del out
#         list_lines.sort(key=lambda elem: float((elem.split())[1]))
# 
#     new_list_lines = []
#     last_energy = 0
#     for l in list_lines:
#         splitted = l.split()
#         energy = float(splitted[1])
#         if energy == last_energy or len(new_list_lines) < 500000 or len(new_list_lines) == 0:
#             new_list_lines.append(l)
#             last_energy = energy
#         else:
#             break
# 
#     new_list_lines.sort(key=lambda elem: float((elem.split())[1]))
# 
#     with open(os.path.join(new_out_dir, mcff_out), 'wb') as new_out:
#         new_out.write("\n".join(new_list_lines))
#     del list_lines
#     del new_list_lines

if __name__ == '__main__':
    list_mcff_out = os.listdir(mcff_out_dir)

    with open(command_file, 'rb') as cmd_f:
        list_lines = cmd_f.readlines()
        for i, line in enumerate(list_lines):
            splitted = line.split()
            seq = splitted[4]
            mask = splitted[6]
            name = splitted[11].replace("./2d/", "")
            dict_mir[name] = dict(seq=seq, mask=mask)

    for mcff_out in sorted(list_mcff_out):
        process_mcff_list(mcff_out)
#     pool = multiprocessing.Pool(5)
#     pool.map(process_mcff_list, sorted(list_mcff_out), chunksize=1)

#     print "FINISHED"