import os
import shutil
import argparse
import sys
import re

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--mcff_out', action="store", dest="mcff_out")

    ns = parser.parse_args()
    mcff_out = ns.mcff_out

    list_lines = []
    with open(mcff_out, 'rb') as mcff_o:
        list_lines = list(set([elem.strip() for elem in mcff_o.readlines() if elem.strip()]))

    list_lines.sort(key=lambda elem: float((elem.split())[1]))
    new_list_lines = []
    last_energy = 0
    for l in list_lines:
        splitted = l.split()
        energy = float(splitted[1])
        if energy == last_energy or len(new_list_lines) < 500000 or len(new_list_lines) == 0:
            new_list_lines.append(l)
            last_energy = energy
        else:
            break

    new_list_lines.sort(key=lambda elem: float((elem.split())[1]))

    with open(mcff_out, 'wb') as new_out:
        new_out.write("\n".join(new_list_lines))