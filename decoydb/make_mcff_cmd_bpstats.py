import os
import argparse
import subprocess
import shlex
import cPickle

def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bpstats_dir', action="store", required=True, dest="bpstats_dir")
    parser.add_argument('--output_dir', action="store", required=True, dest="output_dir")
    parser.add_argument('--hairpin_dir', action="store", required=True, dest="hairpin_dir")

    ns = parser.parse_args()

    bpstats_dir = ns.bpstats_dir
    output_dir = ns.output_dir
    hairpin_dir = ns.hairpin_dir

    list_acc = os.listdir(hairpin_dir)

    for acc in sorted(list_acc):
        if not os.path.exists(os.path.join(bpstats_dir, acc + ".pk")):
            continue
        curr_t = 0.0
        delta_thr = 5.0
        going_back = False
        num_struct = 10000
        seq = ""
        struct = ""
        with open(os.path.join(bpstats_dir, acc + ".pk"), 'rb') as pk_file:
            temp_dict = cPickle.load(pk_file)
            seq = temp_dict["seq"]
            struct = temp_dict["struct"].replace("*", ".").replace(".", "x")

        set_out = set()

        for i in xrange(500):
            curr_t += delta_thr
#             print curr_t
            
            cmd = ('/u/leongs/test_timeout/timeout -m 10000000 -t 3600 \'/u/admc/MC-Flashfold/mcff --tables /u/admc/MC-Flashfold/tables '
                   '-s "{seq}" -m "{struct}" -t {curr_t}\'').format(seq=seq, struct=struct, curr_t=curr_t)
            out, err = call_command(cmd)

            if err:
                print cmd
                print acc, err

            if ("TIMEOUT" in err or "MEM" in err) and not "FINISHED" in err:
                delta_thr = delta_thr * (-0.5)
                going_back = True

            if out.strip():
                for line in out.splitlines():
                    if line.strip():
                        set_out.add(line.strip())
            if len(set_out) < num_struct and going_back:
                delta_thr = delta_thr * (-0.5)
            elif len(set_out) >= num_struct:
                break

        print acc
        print len(set_out)

        with open(os.path.join(output_dir, acc), 'wb') as o_c:
            o_c.write('\n'.join(set_out) + "\n")