import os
import shutil

# get the list of the hairpins that couldn't be modelized

twod = "/u/leongs/projet_naim/mcff/2dcut"
threed = "/u/leongs/mcweb_shared/decoy_db"

list_twod = os.listdir(twod)
list_threed = os.listdir(threed)

list_rem = [elem for elem in list_twod if not elem in list_threed]

path_workdir = "/u/leongs/projet_naim/finalfold/remaining"

for elem in list_rem:
    pat = os.path.join(twod, elem)
    # copy the file
    shutil.copy(pat,
                os.path.join(path_workdir, "2d", elem))
    # copy the mature
    shutil.copy(os.path.join("/u/leongs/projet_naim/mcff/mature", elem),
                os.path.join(path_workdir, "mature", elem))

    # copy the hairpin
    shutil.copy(os.path.join("/u/leongs/projet_naim/mcff/hairpin", elem),
                os.path.join(path_workdir, "hairpin", elem))