import os

remaining_dir = "/u/leongs/projet_naim/finalfold/remaining/1"
# success_dir = "/u/leongs/projet_naim/finalfold/success/"
# other_success = "/u/leongs/mcweb_shared/decoy_db"

# list_success = os.listdir(success_dir) + os.listdir(other_success)
# 
# for elem in list_success:
#     os.remove(os.path.join(remaining_dir, "2d", elem))
#     os.remove(os.path.join(remaining_dir, "hairpin", elem))
#     os.remove(os.path.join(remaining_dir, "mature", elem))

for elem in sorted(os.listdir(os.path.join(remaining_dir, "2d"))):
    workdir = os.path.join(remaining_dir, "3d", elem)
    
    if not os.path.exists(workdir):
        os.mkdir(workdir)
    
    pbs_cont = ('#!/bin/sh\n'
                '#PBS -N remaining_{elem}\n'
                '#PBS -d {workdir}\n'
                '#PBS -o {out_dir}\n'
                '#PBS -e {err_dir}\n'
                '#PBS -l walltime=48:00:00\n'
                'cd {workdir}\n'
                'python {remaining_dir}/search3d_multithreader.py '
                '--hairpin {remaining_dir}/hairpin/{elem} '
                '--mature {remaining_dir}/mature/{elem} '
                '--ss {remaining_dir}/2d/{elem} '
                '--name {elem} --workdir {workdir} '
                '--search3d_path {remaining_dir}/search3d_leongs.pl '
                '--threads 3 --output_log {remaining_dir}/3d/{elem}/log.txt').format(remaining_dir=remaining_dir,
                                                                                     elem=elem,
                                                                                     workdir=workdir,
                                                                                     out_dir=os.path.join(workdir, elem + ".out"),
                                                                                     err_dir=os.path.join(workdir, elem + ".err"))

    with open(os.path.join(remaining_dir, 'pbs_scripts', elem + ".pbs"), 'wb') as pbs_o:
        pbs_o.write(pbs_cont)
    
    print "qsub " + os.path.join(remaining_dir, elem + ".pbs")