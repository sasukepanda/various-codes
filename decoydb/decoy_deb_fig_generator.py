import os
import shutil
import subprocess
import shlex

new_3d = "/u/leongs/projet_naim/new_3d"

def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output


def generate_ss_images(curr_f_dir, accession):
    name = ""
    sequence = ""
    with open(os.path.join(curr_f_dir, accession + ".fa"), 'rb') as fa_file:
        for line in fa_file:
            if line.startswith(">"):
                name = line.split()[0].replace(">", "")
            else:
                sequence = line.strip()
 
    # read the .ss file and extract structure
    structure = ""
    with open(os.path.join(curr_f_dir, accession + ".ss"), 'rb') as ss_file:
        structure = ss_file.read().strip()

    #generate FASTA
    suffix = ">{name}%20({accession})|{seq}|{struct}".format(name=name, accession=accession, seq=sequence, struct=structure)
     
    out, err = call_command("curl http://www-lbit.iro.umontreal.ca/cgi-bin/mcfold/render.cgi?structure={suff}".format(suff=suffix),
                            echo=True)
    splitted_out = out.split()

    if not os.path.exists(os.path.join(curr_f_dir, 'images')):
        os.mkdir(os.path.join(curr_f_dir, 'images'))
 
    for elem in splitted_out:
        if elem.startswith('HREF="/mcfold/rundata/'):
            url = elem.replace('HREF="', '"http://www-lbit.iro.umontreal.ca')
            os.chdir(curr_f_dir)

            out, err = call_command("wget "+ url, echo=True)


list_struct = os.listdir(new_3d)
for st in list_struct:
    generate_ss_images(os.path.join(new_3d, st),
                       st)