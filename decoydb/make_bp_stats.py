import os
import argparse
import multiprocessing
import math
import cPickle

_ss_dir = ""
_input_dir = ""
_output_dir = ""
_perc= 0.9

def convert_sequence_to_pseudo2d(sequence, structure, name):
    """ get a representation of the sequence+structure in miRBase style """
    left = 0
    right = len(structure) - 1

    lay1 = ''
    lay2 = ''
    lay3 = ''
    lay4 = ''
    lay5 = ''

    # find the pairs and put them in a dict
    dict_pairs = dict()

    list_done = []
    for idx, elem in enumerate(structure):
        if elem == ')':
            for rev in reversed(xrange(idx)):
                if structure[rev] == '(' and (not rev in list_done):
                    dict_pairs[idx] = rev
                    list_done.append(rev)
                    break

    structure_l = []
    for elem in structure:
        structure_l.append(' ')

    for k, v in dict_pairs.iteritems():
        structure_l[k] = v
        structure_l[v] = k

    while True:

        if left == right:
            lay3 += sequence[left]
            break

        elif left > right:
            break

        elif structure_l[left] == right and structure_l[right] == left:
            lay1 += ' '
            lay2 += sequence[left]

            list_temp = [sequence[left].upper(), sequence[right].upper()]
            if ('A' in list_temp and 'U' in list_temp) \
                or ('C' in list_temp and 'G' in list_temp) \
                    or ('U' in list_temp and 'G' in list_temp):
                lay3 += '|'
            else:
                lay3 += ':'

            lay4 += sequence[right]
            lay5 += ' '

            left += 1
            right -= 1

        elif right - left == 1 and structure_l[left] == ' ' and structure_l[right] == ' ':
            lay1 += ' '
            lay2 += sequence[left]
            lay3 += ' '
            lay4 += sequence[right]
            lay5 += ' '

            break

        elif structure_l[left] == ' ':
            if structure_l[right] == ' ':
                r2 = 0
                for idx2 in xrange(left + 1, len(structure)):
                    if not structure_l[idx2] == ' ':
                        r2 = structure_l[idx2]
                        break
                l2 = 0
                for idx2 in reversed(xrange(right)):
                    if not structure_l[idx2] == ' ':
                        l2 = structure_l[idx2]
                        break

                if l2 - left == right - r2 or l2 - left == 1 or right - r2 == 1:
                    lay1 += sequence[left]
                    lay2 += ' '
                    lay3 += ' '
                    lay4 += ' '
                    lay5 += sequence[right]

                    right -= 1
                    left += 1
                    continue

                elif l2 - left < right - r2:
                    lay1 += '-'
                    lay2 += ' '
                    lay3 += ' '
                    lay4 += ' '
                    lay5 += sequence[right]

                    right -= 1
                    continue

            lay1 += sequence[left]
            lay2 += ' '
            lay3 += ' '
            lay4 += ' '
            lay5 += '-'

            left += 1

        elif structure_l[right] == ' ':
            lay1 += '-'
            lay2 += ' '
            lay3 += ' '
            lay4 += ' '
            lay5 += sequence[right]

            right -= 1

    return '{lay1}\n{lay2}\n{lay3}\n{lay4}\n{lay5}'.format(lay1=lay1, lay2=lay2, lay3=lay3, lay4=lay4, lay5=lay5)


def find_shapes(structure):
    #assert given structure is adequate
    list_opener = []
    list_stems = []

    i = 0
    list_stem_end = []
    # separate in stems
    while i < len(structure):
        if structure[i] == "(":
            list_opener.append(i)
        elif structure[i] == ")":
            current_stem = dict(opener=[], closer=[], open_dict=dict())
            while i < len(structure):
                if structure[i] == ")":
                    opener = list_opener.pop(-1)
                    current_stem["opener"].append(opener)
                    current_stem["closer"].append(i)
                    current_stem["open_dict"][str(opener)] = i
                    if list_opener and list_opener[-1] in list_stem_end:
                        list_stem_end.append(list_opener[-1])
                        break
                elif structure[i] == "(":
                    if list_opener:
                        list_stem_end.append(list_opener[-1])
                    i -= 1
                    break
                i += 1
            list_stems.append(current_stem)
        i += 1

    # build the level1 for each stems
    range_occupied = []
    dict_lvl = dict()
    for stem in list_stems:
        range_open = range(min(stem['opener']), max(stem['opener'])+1)
        range_close = range(min(stem['closer']), max(stem['closer'])+1)
        range_occupied.extend(range_open+range_close)

        temp_lvl1_open = ""
        temp_lvl1_close = ""

        last_opener = None
        last_closer = None
        for opener in sorted(stem['opener']):
            if last_opener == None:
                temp_lvl1_open += "["
                temp_lvl1_close = "]" + temp_lvl1_close
            else:
                if math.fabs(opener - last_opener) != 1:
                    temp_lvl1_open += "_"
                if math.fabs(stem["open_dict"][str(opener)] - last_closer) != 1:
                    temp_lvl1_close = "_" + temp_lvl1_close
                if temp_lvl1_open.endswith("_") or temp_lvl1_close.startswith("_"):
                    temp_lvl1_open += "["
                    temp_lvl1_close = "]" + temp_lvl1_close
            last_opener = opener
            last_closer = stem["open_dict"][str(opener)]

        dict_lvl[str(min(stem['opener']))] = dict(elem=temp_lvl1_open, lvl5="[")
        dict_lvl[str(min(stem['closer']))] = dict(elem=temp_lvl1_close, lvl5="]")

    # assemble level1
    level1 = ""
    level5 = ""
    for i, element in enumerate(structure):
        if str(i) in dict_lvl:
            level1 += dict_lvl[str(i)]["elem"].strip()
            level5 += dict_lvl[str(i)]["lvl5"]
        if element == "." and not level1.endswith("_") and not i in range_occupied:
            level1 += "_"

    level1 = level1.strip().replace("[_]", "[]").replace(" ", "")
    level3 = level1.replace("_", "")

    return level5, level3, level1

def dotBracketToBPSet(dotBracket):
    """
    INPUT dotBracket => OUTPUT dict containing lists of BP for regulars and pseudoknots
    """
    list_pairs = []
    regular_opener = []
    pseudoknot_opener = []
    list_unpairs = []
    for i, element in enumerate(dotBracket):
        if element == "(":
            regular_opener.append(i)
        elif element == ")":
            opener = regular_opener.pop(-1)
            closer = i
            list_pairs.append((opener, closer))
        else:
            list_unpairs.append(i)
    return list_pairs, list_unpairs


def build_struct(list_opener, list_closer, list_unpair, len_seq):
    # build the dot_bracket
    struct = ""
    for x in xrange(len_seq):
        if x in list_opener:
            struct += "("
        elif x in list_closer:
            struct += ")"
        elif x in list_unpair:
            struct += "."
        else:
            struct += "*"
    return struct


def recursively_make_struct(list_opener, list_closer, len_seq, list_pairs, id=0):
    print id
    set_result = set()
    if not list_pairs:
        structure = build_struct(list_opener, list_closer, [], len_seq)
        set_result.add(structure)
    else:
        for pair in list_pairs:
            new_list_pairs = [elem for elem in list_pairs if elem != pair]
            op, cl = pair
            if op in list_opener or cl in list_closer:
                set_result.update(recursively_make_struct(list_opener,
                                                          list_closer,
                                                          len_seq,
                                                          new_list_pairs,
                                                          id=0))
            else:
                new_list_opener = list_opener+[op]
                new_list_closer = list_closer+[cl]
                set_result.update(recursively_make_struct(new_list_opener,
                                                          new_list_closer,
                                                          len_seq,
                                                          new_list_pairs,
                                                          id = id+1))
    return set_result


def get_mat(acc):
    p5 = ""
    p3 = ""
    with open(os.path.join(_ss_dir, acc, acc + ".mat"), 'rb') as mat_c:
        list_line = mat_c.readlines()
        i = 0
        while i < len(list_line):
            if list_line[i].strip().endswith("5p"):
                p5 = list_line[i+1].strip()
                i +=1
            elif list_line[i].strip().endswith("3p"):
                p3 = list_line[i+1].strip()
                i +=1
            i +=1
    return p5, p3


def get_seq(acc):
    seq = ""
    with open(os.path.join(_ss_dir, acc, acc + ".fa"), 'rb') as seq_c:
        seq = seq_c.readlines()[1].strip()
    return seq


def convert_to_shade(num):
    level = math.ceil(float(num)*255)
    return int(level)

def compute_seq(acc):
    # compute the mature bp first
    hairpin_seq = get_seq(acc)
    p5, p3 = get_mat(acc)
    chosen_struct = ""
    with open(os.path.join(_ss_dir, acc, acc + ".ss"), 'rb') as ss_f:
        chosen_struct = ss_f.read().strip()
    list_bp_chosen, list_unpair_chose = dotBracketToBPSet(chosen_struct)

    if p5 and p5 in hairpin_seq:
        range_p5 = range(hairpin_seq.find(p5), hairpin_seq.find(p5)+len(p5))
    else:
        range_p5 = []
    
    if p3 and p3 in hairpin_seq:
        range_p3 = range(hairpin_seq.find(p3), hairpin_seq.find(p3)+len(p3))
    else:
        range_p3 = []

    filtered_bp_chosen = []
    list_always_op = []
    list_always_cl = []
    for bp in list_bp_chosen:
        op, cl = bp
        if op in range_p5 or cl in range_p3:
            filtered_bp_chosen.append(bp)
            list_always_op.append(op)
            list_always_cl.append(cl)

    input_filepath = os.path.join(_input_dir, acc)
    all_pairs = []
    all_unpairs = []
    dict_pair_prob = dict()
    len_seq = len(hairpin_seq)
    list_structures = []
    valid_structures = []
    with open(input_filepath, 'rb') as input_content:
        list_structures = [elem for elem in input_content.readlines() if elem.strip()]
    for i, struct_line in enumerate(list_structures):
        splitted = struct_line.split()
        struct = splitted[0]
        l_pairs, l_unpairs = dotBracketToBPSet(struct)
        valid_struct = True
        
        for bp in filtered_bp_chosen:
            if not bp in l_pairs:
                valid_struct = False
                break
        if valid_struct:
            for curr_bp in l_pairs:
                op, cl = curr_bp
                if range_p5:
                    if ((min(range_p5) <= op <= max(range_p5)) and (not curr_bp in filtered_bp_chosen)):
                        valid_struct = False
                        break
                if range_p3:
                    if ((min(range_p3) <= cl <= max(range_p3)) and (not curr_bp in filtered_bp_chosen)):
                        valid_struct = False
                        break
        if valid_struct:
            valid_structures.append(struct)
            all_pairs.extend(l_pairs)
            all_unpairs.extend(l_unpairs)

    filtered_list_unpairs = []
    for unpair in set(all_unpairs):
        perc = float(all_unpairs.count(unpair)) / float(len(valid_structures))
        if perc >= _perc:
            filtered_list_unpairs.append(unpair)

    list_op = []
    list_cl = []
    for bp in list_bp_chosen:
        op, cl = bp
        if (not op in filtered_list_unpairs) and (not cl in filtered_list_unpairs):
            list_op.append(op)
            list_cl.append(cl)

    list_struct = [build_struct(list_op, list_cl, [], len_seq)]

    with open(os.path.join(_output_dir, 'make_unpair', acc + ".ss"), 'wb') as out_c:
        out_c.write("\n".join(list_struct))

    filtered_list_pairs = []
    for pair in set(all_pairs):
        perc = float(all_pairs.count(pair)) / float(len(valid_structures))
        if perc >= _perc:
            filtered_list_pairs.append(pair)
 
    list_op = []
    list_cl = []
    remaining_pairs = []
    for pair1 in filtered_list_pairs:
        op, cl = pair1
        has_clash = False
        for pair2 in filtered_list_pairs:
            if pair1 != pair2:
                op2, cl2 = pair2
                if op==op2 or cl==cl2 or op==cl2 or cl==op2:
                    has_clash = True
                    remaining_pairs.append(pair1)
                    break
        if not has_clash:
            list_op.append(op)
            list_cl.append(cl)
 
    if remaining_pairs:
#         print acc, "recursive solution"
        list_struct = recursively_make_struct(list_op, list_cl, len_seq, remaining_pairs)
    else:
#         print acc, "normal solution"
        list_struct = [build_struct(list_op, list_cl, [], len_seq)]

    with open(os.path.join(_output_dir, 'make_pair', acc + ".ss"), 'wb') as out_c:
        out_c.write("\n".join(list_struct))

    # make the mixed one
    list_struct = [build_struct(list_op, list_cl, filtered_list_unpairs, len_seq)]

    with open(os.path.join(_output_dir, 'make_both', acc + ".ss"), 'wb') as out_c:
        out_c.write("\n".join(list_struct))

    struct = build_struct(list_always_op, list_always_cl, [], len_seq)
    # make the ss perc for the struct
    list_perc_unpair = []
    list_perc_unpair_all = []
    for i in xrange(len_seq):
        list_unpair = [structure for structure in valid_structures if structure[i] == "."]
        list_perc_unpair.append(str(float(len(list_unpair))/float(len(valid_structures))))
        list_unpair_all = [structure for structure in list_structures if structure[i] == "."]
        list_perc_unpair_all.append(str(float(len(list_unpair_all))/float(len(list_structures))))

    shape5, shape3, shape1 = find_shapes(struct.replace("*", "."))

    with open(os.path.join(_output_dir, 'bp_stats', acc + ".stat"), 'wb') as out_c:
        out_c.write("{struct}\n{shape}\n{stat}\n{color_lvl}\{stat_all}\n{color_lvl_all}".format(struct=struct,
                                                                                                shape=shape5,
                                                                                                stat=",".join(list_perc_unpair),
                                                                                                stat_all=",".join(list_perc_unpair_all),
                                                                                                color_lvl=",".join([str(convert_to_shade(elem)) for elem in list_perc_unpair]),
                                                                                                color_lvl_all=",".join([str(convert_to_shade(elem)) for elem in list_perc_unpair_all])))

    with open(os.path.join(_output_dir, 'bp_stats', acc + ".pk"), 'wb') as out_c:
        cPickle.dump(dict(struct=struct,
                          shape=shape5,
                          stat=list_perc_unpair,
                          stat_all=list_perc_unpair_all,
                          color_lvl=[convert_to_shade(elem) for elem in list_perc_unpair],
                          color_lvl_all=[convert_to_shade(elem) for elem in list_perc_unpair_all],
                          list_filtered_struct=valid_structures,
                          list_all_struct=list_structures,
                          seq=hairpin_seq,
                          mature_range_p3=range_p3,
                          mature_range_p5=range_p5,
                          mature_range=range_p5+range_p3,
                          mature_p5=p5,
                          mature_p3=p3), out_c, -1)

    # convert to miRBase-like representation
    if shape5 == "[]":
        with open(os.path.join(_output_dir, 'bp_stats', acc + ".mirbase"), 'wb') as out_c:
            out_c.write(convert_sequence_to_pseudo2d(hairpin_seq, struct, acc))
    else:
        print acc
        print struct

    with open(os.path.join(_output_dir, 'filtered_ss', acc), 'wb') as out_c:
        out_c.write("\n".join(valid_structures))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--ss_dir', '-s', action="store", required=True, dest="ss_dir")
    parser.add_argument('--input_dir', '-i', action="store", required=True, dest="input_dir")
    parser.add_argument('--output_dir', '-o', action="store", dest="output_dir", required=True)
    parser.add_argument('--threads', action="store", dest="threads")
    parser.add_argument('--perc', action="store", dest="perc")

    ns = parser.parse_args()

    _ss_dir = ns.ss_dir
    _input_dir = ns.input_dir
    _output_dir = ns.output_dir
    threads = int(ns.threads)
    _perc = float(ns.perc)

    if not os.path.exists(_output_dir):
        os.mkdir(_output_dir)
    if not os.path.exists(os.path.join(_output_dir, 'make_unpair')):
        os.mkdir(os.path.join(_output_dir, 'make_unpair'))
    if not os.path.exists(os.path.join(_output_dir, 'make_pair')):
        os.mkdir(os.path.join(_output_dir, 'make_pair'))
    if not os.path.exists(os.path.join(_output_dir, 'make_both')):
        os.mkdir(os.path.join(_output_dir, 'make_both'))
    if not os.path.exists(os.path.join(_output_dir, 'bp_stats')):
        os.mkdir(os.path.join(_output_dir, 'bp_stats'))
    if not os.path.exists(os.path.join(_output_dir, 'filtered_ss')):
        os.mkdir(os.path.join(_output_dir, 'filtered_ss'))

    list_input = sorted(os.listdir(_ss_dir))

#     for input in list_input:
#         compute_seq(input)
    pool = multiprocessing.Pool(threads)
    pool.map(compute_seq, list_input, chunksize=1)