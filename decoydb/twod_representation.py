def find_shapes(structure):
    list_opener = []
    list_stems = []

    i = 0
    list_stem_end = []
    # separate in stems
    while i < len(structure):
        if structure[i] == "(":
            list_opener.append(i+1)
        elif structure[i] == ")":
            current_stem = dict(opener=[], closer=[], open_dict=dict())
            while i < len(structure):
                if structure[i] == ")":
                    opener = list_opener.pop(-1)
                    current_stem["opener"].append(opener+1)
                    current_stem["closer"].append(i+1)
                    current_stem["open_dict"][str(opener)] = i+1
                    if list_opener and list_opener[-1] in list_stem_end:
                        list_stem_end.append(list_opener[-1])
                        break
                elif structure[i] == "(":
                    if list_opener:
                        list_stem_end.append(list_opener[-1])
                    i -= 1
                    break
                i += 1
            list_stems.append(current_stem)
        i += 1

    # sort the stems
    list_sorted_stems = []
    while len(list_sorted_stems) < len(list_stems):
        minimum_stem_index = None
        for i, stem in enumerate(list_stems):
            if i not in list_sorted_stems:
                if minimum_stem_index == None:
                    minimum_stem_index = i
                elif min(stem["opener"]) <= min(list_stems[minimum_stem_index]["opener"]):
                    minimum_stem_index = i
        list_sorted_stems.append(minimum_stem_index)
    return list_stems, list_sorted_stems


def convert_sequence_to_pseudo2d(sequence, structure, name):
    """ get a representation of the sequence+structure in miRBase style """
    left = 0
    right = len(structure) - 1

    lay1 = ''
    lay2 = ''
    lay3 = ''
    lay4 = ''
    lay5 = ''

    # find the pairs and put them in a dict
    dict_pairs = dict()

    list_done = []
    for idx, elem in enumerate(structure):
        if elem == ')':
            for rev in reversed(xrange(idx)):
                if structure[rev] == '(' and (not rev in list_done):
                    dict_pairs[idx] = rev
                    list_done.append(rev)
                    break

    structure_l = []
    for elem in structure:
        structure_l.append(' ')

    for k, v in dict_pairs.iteritems():
        structure_l[k] = v
        structure_l[v] = k

    while True:

        if left == right:
            lay3 += sequence[left]
            break

        elif left > right:
            break

        elif structure_l[left] == right and structure_l[right] == left:
            lay1 += ' '
            lay2 += sequence[left]

            list_temp = [sequence[left].upper(), sequence[right].upper()]
            if ('A' in list_temp and 'U' in list_temp) \
                or ('C' in list_temp and 'G' in list_temp) \
                    or ('U' in list_temp and 'G' in list_temp):
                lay3 += '|'
            else:
                lay3 += ':'

            lay4 += sequence[right]
            lay5 += ' '

            left += 1
            right -= 1

        elif right - left == 1 and structure_l[left] == ' ' and structure_l[right] == ' ':
            lay1 += ' '
            lay2 += sequence[left]
            lay3 += ' '
            lay4 += sequence[right]
            lay5 += ' '

            break

        elif structure_l[left] == ' ':
            if structure_l[right] == ' ':
                r2 = 0
                for idx2 in xrange(left + 1, len(structure)):
                    if not structure_l[idx2] == ' ':
                        r2 = structure_l[idx2]
                        break
                l2 = 0
                for idx2 in reversed(xrange(right)):
                    if not structure_l[idx2] == ' ':
                        l2 = structure_l[idx2]
                        break

                if l2 - left == right - r2 or l2 - left == 1 or right - r2 == 1:
                    lay1 += sequence[left]
                    lay2 += ' '
                    lay3 += ' '
                    lay4 += ' '
                    lay5 += sequence[right]

                    right -= 1
                    left += 1
                    continue

                elif l2 - left < right - r2:
                    lay1 += '-'
                    lay2 += ' '
                    lay3 += ' '
                    lay4 += ' '
                    lay5 += sequence[right]

                    right -= 1
                    continue

            lay1 += sequence[left]
            lay2 += ' '
            lay3 += ' '
            lay4 += ' '
            lay5 += '-'

            left += 1

        elif structure_l[right] == ' ':
            lay1 += '-'
            lay2 += ' '
            lay3 += ' '
            lay4 += ' '
            lay5 += sequence[right]

            right -= 1

    return '{lay1}\n{lay2}\n{lay3}\n{lay4}\n{lay5}'.format(lay1=lay1, lay2=lay2, lay3=lay3, lay4=lay4, lay5=lay5)





list_struct = [dict(struct="***((*(((((((((((((((((((((***************************)))))))))))))))))))))))***",
                    name="MI0000060",
                    seq="UGGGAUGAGGUAGUAGGUUGUAUAGUUUUAGGGUCACACCCACCACUGGGAGAUAACUAUACAAUCUACUGUCUUUCCUA"),
               dict(struct="***************************(((((((((((((((((((*(((**))(*************)))))))))))))))))))))**************************",
                    name="MI0018175",
                    seq="UCUGAGGAGACCUGGGCUGUCAGAGGCCAGGGAAGGGGACGAGGGUUGGGGAACAGGUGGUUAGCACUUCAUCCUCGUCUCCCUCCCAGGUUAGAAGGGCCCCCCUCUCUGAAGG"),
               dict(struct="**********(((((((((((((((((((((((****))**(********))))))))))))))))))))))**********",
                    name="MI0000748",
                    seq="GGCCUGCCCGACACUCUUUCCCUGUUGCACUACUAUAGGCCGCUGGGAAGCAGUGCAAUGAUGAAAGGGCAUCGGUCAGGUC")]

for struct_dict in list_struct:
    struct = struct_dict["struct"]
    list_stems, sorted_list = find_shapes(struct.replace("*", "."))
    if len(list_stems) == 1:
        print convert_sequence_to_pseudo2d(struct_dict["seq"], struct_dict["struct"], struct_dict["name"])
    else:
        continue
        for elem in sorted_list:
            min_open = min(list_stems[elem]["opener"])
            max_open = max(list_stems[elem]["opener"])
            min_close = min(list_stems[elem]["closer"])
            max_close = max(list_stems[elem]["closer"])
    
            struct_open = ""
            seq_open = ""
            for i in xrange(min_open, max_open+1):
                seq_open += struct_dict["seq"][i-1]
                if i in list_stems[elem]["opener"]:
                    struct_open += "("
                else:
                    struct_open += "."
    
            struct_close = ""
            seq_close = ""
            for i in xrange(min_close, max_close+1):
                seq_close += struct_dict["seq"][i-1]
                if i in list_stems[elem]["closer"]:
                    struct_close += ")"
                else:
                    struct_close += "."
    
            print convert_sequence_to_pseudo2d(seq_open+seq_close, struct_open+struct_close, struct_dict["name"])