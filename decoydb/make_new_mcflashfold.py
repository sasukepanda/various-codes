import os
import subprocess
import shlex

def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output


workdir = "/u/leongs/projet_naim/mcff"
hairpin_dir = "/u/leongs/projet_naim/mcff/hairpin"

seq_dict = dict()
for acc in os.listdir(hairpin_dir):
    with open(os.path.join(hairpin_dir, acc), 'rb') as fasta_c:
        sequence = fasta_c.readlines()[1].strip()
        seq_dict[acc] = sequence

for stats_dir in sorted([elem for elem in os.listdir(workdir) if elem.startswith("2dstats")]):
    perc = stats_dir.split("_")[1]
    print "mkdir " + os.path.join(workdir, '2d_'+perc)
    for ss_file in sorted(os.listdir(os.path.join(workdir, stats_dir, "make_both"))):
        acc = ss_file.replace(".ss", "")
        mask = ""
        with open(os.path.join(workdir, stats_dir, "make_both", ss_file), 'rb') as mask_f:
            mask = mask_f.read().strip()
        print ('/u/admc/MC-Flashfold/mcff --tables /u/admc/MC-Flashfold/tables '
               '-s "{seq}" -m "{mask}" -ft 1000 > {output_file}').format(seq=seq_dict[acc],
                                                                         mask=mask.replace("*", 'x'),
                                                                         output_file=os.path.join(workdir, '2d_'+perc, acc))