import os
import shutil

success_dir = "/u/leongs/projet_naim/finalfold/success"
failed_dir = "/u/leongs/projet_naim/finalfold/failed"

for superdir in ["/u/leongs/projet_naim/finalfold/remaining/1", "/u/leongs/projet_naim/finalfold/remaining/2"]:
    remaining_dir = os.path.join("/u/leongs/projet_naim/finalfold/remaining", superdir, "3d")
    list_struct = sorted(os.listdir(remaining_dir))
    for st in list_struct:
        list_st_cont = [elem for elem in os.listdir(os.path.join(remaining_dir, st)) if elem.endswith("pdb.gz")]
        if list_st_cont:
            shutil.copytree(os.path.join(remaining_dir, st),
                            os.path.join(success_dir, st),
                            symlinks=True)
            os.remove(os.path.join("/u/leongs/projet_naim/finalfold/remaining", superdir, "2d", st))
            os.remove(os.path.join("/u/leongs/projet_naim/finalfold/remaining", superdir, "hairpin", st))
            os.remove(os.path.join("/u/leongs/projet_naim/finalfold/remaining", superdir, "mature", st))
        else:
            shutil.copytree(os.path.join(remaining_dir, st),
                            os.path.join(failed_dir, st),
                            symlinks=True)