import os
import tarfile
import subprocess
import shlex


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output

success_dir = "/u/leongs/projet_naim/finalfold/success"

for acc in sorted(os.listdir(success_dir)):
    workdir = os.path.join(success_dir, acc, "work")
    list_pdb = [elem for elem in os.listdir(workdir) if elem.endswith("pdb.gz")]

    if len(list_pdb) > 1000:
        print acc
#     os.chdir(workdir)
#     for i, pdb in enumerate(sorted(list_pdb)):
#         if i > 
#     os.chdir(workdir)
#     for pdb in sorted(list_pdb):
#         try:
#             call_command("gzip "+pdb, echo=True)
#         except Exception as e:
#             print e
