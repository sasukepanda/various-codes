import os
import shutil
from random import shuffle

source_dir ="/export/home/leongs/VMWare_share/test_pseudoviewer/2dstats_50p"
# source_dir = "/export/home/leongs/VMWare_share/test_pseudoviewer/50p/1"
dest_dir ="/export/home/leongs/VMWare_share/test_pseudoviewer/success_50p"

list_svg = [elem for elem in os.listdir(source_dir) if elem.endswith(".svg")]

# for svg in list_svg:
#     shutil.move(os.path.join(source_dir, svg),
#                 os.path.join(dest_dir, svg))
#     shutil.move(os.path.join(source_dir, svg.replace(".svg", ".txt")),
#                 os.path.join(dest_dir, svg.replace(".svg", ".txt")))
#     for f in [elem for elem in os.listdir(source_dir) if svg.replace(".svg", "") in elem and elem != svg and not elem.endswith(".txt")]:
#         os.remove(os.path.join(source_dir, f))

separate_dir = "/export/home/leongs/VMWare_share/test_pseudoviewer/50p"
# separate_dir = "/export/home/leongs/VMWare_share/test_pseudoviewer/50p/sub_1"
 
# for i in xrange(4):
#     list_dir = [elem for elem in os.listdir(os.path.join(separate_dir, str(i))) if elem.endswith(".svg")]
#     for svg in list_dir:
#         shutil.copy(os.path.join(separate_dir, str(i), svg),
#                     os.path.join(dest_dir, svg))
#         shutil.copy(os.path.join(separate_dir, str(i), svg.replace(".svg", ".txt")),
#                     os.path.join(dest_dir, svg.replace(".svg", ".txt")))

for i in xrange(4):
    if not os.path.exists(os.path.join(separate_dir, str(i))):
        os.mkdir(os.path.join(separate_dir, str(i)))
  
list_dest = os.listdir(dest_dir)
list_dir = [elem for elem in os.listdir(source_dir) if not elem in list_dest]
shuffle(list_dir)
  
for i, elem in enumerate(list_dir):
    shutil.copy(os.path.join(source_dir, elem),
                os.path.join(separate_dir, str(i%4), elem))