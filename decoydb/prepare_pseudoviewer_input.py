import cPickle
import os

pickle_dir = "/u/leongs/projet_naim/mcff/2dstats_50p_with_stat_dir/bp_stats"
destination_dir = "/export/home/leongs/VMWare_share/test_pseudoviewer"

seq_dict = dict()
for pickle_f in sorted([elem for elem in os.listdir(pickle_dir) if elem.endswith(".pk")]):
    pick_dict = dict()

    if not os.path.exists(os.path.join(destination_dir, "only_mature_pair")):
        os.mkdir(os.path.join(destination_dir, "only_mature_pair"))

    with open(os.path.join(pickle_dir, pickle_f), 'rb') as pk_f:
        pick_dict = cPickle.load(pk_f)

        with open(os.path.join(destination_dir, "only_mature_pair", pickle_f.replace("pk", "txt")), 'wb') as out_c:
            out_c.write("{seq}\n{struct}".format(seq=pick_dict["seq"],
                                                 struct=pick_dict["struct"].replace("*", ".")))
        seq_dict[pickle_f.replace(".pk", "")] = pick_dict["seq"]


list_various_perc = ["2dstats_90p", "2dstats_95p", "2dstats_80p", "2dstats_70p", "2dstats_60p", "2dstats_50p"]
mcff_dir = "/u/leongs/projet_naim/mcff"

for perc in list_various_perc:
    perc_dir = os.path.join(mcff_dir, perc)

    if not os.path.exists(os.path.join(destination_dir, perc)):
        os.mkdir(os.path.join(destination_dir, perc))

    for sub_elem in os.listdir(perc_dir):
        if not os.path.exists(os.path.join(destination_dir, perc, sub_elem)):
            os.mkdir(os.path.join(destination_dir, perc, sub_elem))

        for acc, seq in seq_dict.iteritems():
            with open(os.path.join(mcff_dir, perc, sub_elem, acc + ".ss"), 'rb') as structure_c:
                struct = structure_c.read().strip().replace("*", ".")

                with open(os.path.join(destination_dir, perc, sub_elem, acc + ".txt"), 'wb') as formatted_c:
                    formatted_c.write("{seq}\n{struct}".format(seq=seq_dict[acc],
                                                               struct=struct))