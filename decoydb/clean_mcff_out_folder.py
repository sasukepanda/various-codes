import os
import shutil
import argparse
import sys
import re
import subprocess
import shlex
import multiprocessing

_size = 100000
_temp_filelist_dir = "."

def clean(mcff_out):
    list_lines = []
    with open(mcff_out, 'rb') as mcff_o:
        list_lines = list(set([elem.strip() for elem in mcff_o.readlines() if elem.strip()]))

    list_lines.sort(key=lambda elem: float((elem.split())[1]))
    new_list_lines = []
    last_energy = 0
    for l in list_lines:
        splitted = l.split()
        energy = float(splitted[1])
        if energy == last_energy or len(new_list_lines) < _size or len(new_list_lines) == 0:
            new_list_lines.append(l)
            last_energy = energy
        else:
            break
 
    new_list_lines.sort(key=lambda elem: float((elem.split())[1]))

    if len(new_list_lines) < _size:
        if not os.path.exists(os.path.join(_temp_filelist_dir, os.path.basename(mcff_out))):
            os.mkdir(os.path.join(_temp_filelist_dir, os.path.basename(mcff_out)))
#         print os.path.basename(mcff_out)

    with open(mcff_out, 'wb') as new_out:
        new_out.write("\n".join(new_list_lines))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--mcff_out_dir', action="store", dest="mcff_out_dir")
    parser.add_argument('--temp_filelist_dir', action="store", dest="temp_filelist_dir")
    parser.add_argument('--threads', action="store", dest="threads")
    parser.add_argument('--size', action="store", dest="size")

    ns = parser.parse_args()
    mcff_out_dir = ns.mcff_out_dir
    threads = int(ns.threads)
    _size = int(ns.size)
    _temp_filelist_dir = ns.temp_filelist_dir

    list_mcff_out = sorted([os.path.join(mcff_out_dir, elem) for elem in os.listdir(mcff_out_dir)])

    pool = multiprocessing.Pool(threads)
    pool.map(clean, list_mcff_out, chunksize=1)
    
    print "\n".join(sorted(os.listdir(_temp_filelist_dir)))
    os.chdir(_temp_filelist_dir)
    for dir in os.listdir(_temp_filelist_dir):
        shutil.rmtree(dir)
