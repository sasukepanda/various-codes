import os
import gzip
import subprocess
import shlex
import cPickle


def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output

def dotBracketToBPSet(dotBracket):
    """
    INPUT dotBracket => OUTPUT dict containing lists of BP for regulars and pseudoknots
    """
    list_pairs = []
    regular_opener = []
    pseudoknot_opener = []
    list_unpairs = []
    for i, element in enumerate(dotBracket):
        if element == "(":
            regular_opener.append(i)
        elif element == ")":
            opener = regular_opener.pop(-1)
            closer = i
            list_pairs.append((opener, closer))
        else:
            list_unpairs.append(i)

    return list_pairs, list_unpairs

def get_mat(acc):
    p5 = ""
    p3 = ""
    with open(os.path.join("/u/leongs/projet_naim/finalfold/mature", acc), 'rb') as mat_c:
        list_line = mat_c.readlines()
        i = 0
        while i < len(list_line):
            if list_line[i].strip().endswith("5p"):
                p5 = list_line[i+1].strip()
                i +=1
            elif list_line[i].strip().endswith("3p"):
                p3 = list_line[i+1].strip()
                i +=1
            i +=1
    return p5, p3

def get_seq(acc):
    seq = ""
    with open(os.path.join("/u/leongs/projet_naim/finalfold/hairpin", acc), 'rb') as seq_c:
        seq = seq_c.readlines()[1].strip()
    return seq

hairpin_dir = "/u/leongs/projet_naim/finalfold/hairpin"
mature_dir = "/u/leongs/projet_naim/finalfold/mature"

list_hairpin = os.listdir(hairpin_dir)

# this dict will contains list of begin and ends for each acc
# dict_mature_pos = dict()
# for acc in list_hairpin:
#     seq = ""
#     with open(os.path.join(hairpin_dir, acc), 'rb') as hairpin_c:
#         seq = hairpin_c.readlines()[1].strip()
# 
#     list_mature = []
#     with open(os.path.join(mature_dir, acc), 'rb') as mature_c:
#         for i, line in enumerate(mature_c):
#             if i%2:
#                 list_mature.append(line.strip())
# 
#     list_mature_pos = []
#     for mature in list_mature:
#         start = seq.find(mature)+1
#         end = start+len(mature)
#         list_mature_pos.extend(range(start, end))
# 
#     dict_mature_pos[acc] = list_mature_pos


decoy_db_path = "/u/leongs/mcweb_shared/decoy_db"
for acc in sorted(os.listdir(decoy_db_path)):
    decoy_path = os.path.join(decoy_db_path, acc)
    
    hairpin_seq = get_seq(acc)
    p5, p3 = get_mat(acc)
    chosen_struct = ""
    with open(os.path.join(decoy_db_path, acc, acc + ".ss"), 'rb') as ss_f:
        chosen_struct = ss_f.read().strip()
    list_bp_chosen, list_unpair_chose = dotBracketToBPSet(chosen_struct)

    if p5 and p5 in hairpin_seq:
        range_p5 = range(hairpin_seq.find(p5), hairpin_seq.find(p5)+len(p5))
    else:
        range_p5 = []
    
    if p3 and p3 in hairpin_seq:
        range_p3 = range(hairpin_seq.find(p3), hairpin_seq.find(p3)+len(p3))
    else:
        range_p3 = []

    list_op = []
    list_cl = []
    for bp in list_bp_chosen:
        op, cl = bp
        if op in range_p5 or cl in range_p3:
            list_op.append(int(op))
            list_cl.append(int(cl))

    hairpin_range = range(min(list_op), max(list_op)+1) + range(min(list_cl), max(list_cl)+1)

    pdb_file = [elem for elem in os.listdir(decoy_path) if elem.endswith(".pdb.gz")][0]
    pdb_path = os.path.join(decoy_path, pdb_file)
    pdb_c = gzip.open(pdb_path, 'rb')
    list_new_pdb = []
    set_index = set()
    for line in pdb_c.readlines():
        splitted = line.split()
        if len(splitted) > 4:
            try:
                index = int(splitted[-4])
            except Exception as e:
                try:
                    index = int(splitted[-3])
                except Exception as e:
                    index = int(splitted[-2])

            if index-1 in hairpin_range:
                list_new_pdb.append(line.strip())
                set_index.add(index)
    with open(os.path.join("/u/leongs/projet_naim/mature_pdbs", acc + ".index"), 'wb') as index_o:
         index_o.write("\n".join([str(elem) for elem in sorted(list(set_index))]))
         
    with open(os.path.join("/u/leongs/projet_naim/mature_pdbs", acc + ".pk"), 'wb') as pkl:
        cPickle.dump(dict(op=range(min(list_op), max(list_op)+1), cl=range(min(list_cl), max(list_cl)+1)), pkl, -1)
#     with open(os.path.join("/u/leongs/projet_naim/mature_pdbs", acc + ".pdb"), 'wb') as pdb_o:
#         pdb_o.write("\n".join(list_new_pdb))
# 
#     # make script pymol
#     pymol_script = ('cmd.show("cartoon"   ,"all");'
#                     'zoom complete=1;'
#                     'set antialias, 2;'
#                     'set hash_max, 220;'
#                     'set ray_trace_mode, 0;'
#                     'bg_color white;'
#                     'set cartoon_fancy_helices, 0;'
#                     'set cartoon_side_chain_helper, off;'
#                     'set surface_quality, 2;'
#                     'set cartoon_ring_mode, 3;'
#                     'set cartoon_ring_finder, 1;'
#                     'set cartoon_ladder_mode, 1;'
#                     'set cartoon_nucleic_acid_mode, 4;'
#                     'set cartoon_ring_transparency, 0.5;'
#                     'ray 2400;'
#                     'png {png_file};quit').format(png_file=os.path.join("/u/leongs/projet_naim/mature_pdbs", acc + ".png"))
#     
#     print("pymol -cq {pdb_file} -d '{scr}'".format(pdb_file=os.path.join("/u/leongs/projet_naim/mature_pdbs", acc + ".pdb"),
#                                                    scr=pymol_script))