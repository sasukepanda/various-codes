import os
import subprocess
import shlex
import argparse
import multiprocessing
import shutil
import cPickle

def call_command(command, pipe=None, echo=False):
    if echo:
        print command

    process = subprocess.Popen(shlex.split(command.encode("ascii")),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output = process.communicate(input=pipe)
    return output


def call_search3d(dict_params):
    out, err = call_command('perl {search3d_path} {seq} "{struct}" {matfile} {pdb} {name}'.format(search3d_path=dict_params["search3d_path"],
                                                                                                  seq=dict_params["seq"],
                                                                                                  struct=dict_params["struct"],
                                                                                                  matfile=dict_params["matfile"],
                                                                                                  pdb=dict_params["pdb"],
                                                                                                  name=dict_params["name"]), echo=True)

    if err:
        print err
    if out:
        with open(dict_params["output_log"], 'a') as out_c:
            out_c.write(out + "\n")
    try:
        call_command("gzip "+dict_params["pdb"].replace(".gz", ""), echo=True)
    except Exception as e:
        print e

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--hairpin',action="store", required=True, dest="hairpin")
    parser.add_argument('--mature', action="store", required=True, dest="mature")
    parser.add_argument('--ss', action="store", dest="ss", required=True)
    parser.add_argument('--name', action="store", dest="name", required=True)
    parser.add_argument('--threads', action="store", dest="threads", required=True)
    parser.add_argument('--workdir', action="store", dest="workdir", required=True)
    parser.add_argument('--search3d_path', action="store", dest="search3d_path", required=True)
    parser.add_argument('--output_log', action="store", dest="output_log", required=True)
    

    ns = parser.parse_args()

    ss = ns.ss
    mature = ns.mature
    hairpin = ns.hairpin
    name = ns.name
    threads = int(ns.threads)
    workdir = ns.workdir
    search3d_path = ns.search3d_path
    output_log = ns.output_log

    seq = ""
    with open(hairpin, 'rb') as hairpin_c:
        seq = hairpin_c.readlines()[1].strip()

    print "log for:\n{name}".format(name=name)

    list_structures = []
    with open(ss, 'rb') as ss_c:
        list_structures = [elem.strip().split()[0] for elem in ss_c.readlines() if elem.strip()]

    for i, structure in enumerate(list_structures):
        with open(output_log, 'wb') as out_c:
            out_c.write("processing structure #{i}: {struct}\n".format(i=i,
                                                                       struct=structure))
            
        os.chdir(workdir)
        if os.path.exists(os.path.join(workdir, "work")):
            try:
                shutil.rmtree("work")
            except Exception as e:
                print e

        os.mkdir(os.path.join(workdir, "work"))
        os.chdir(os.path.join(workdir, "work"))

        # get the external lib info
        dict_pos = dict()
        with open(os.path.join("/u/leongs/projet_naim/mature_pdbs", name + ".pk"), 'rb') as pk_f:
            dict_pos = cPickle.load(pk_f)

        # build the external library string
        ext_lib_str = "{path},{left_start},{left_end},{right_start},{right_end}".format(path=os.path.join("/u/leongs/projet_naim/mature_pdbs", name + ".pdb"),
                                                                                        left_start=min(dict_pos["op"])+1,
                                                                                        left_end=max(dict_pos["op"])+1,
                                                                                        right_start=min(dict_pos["cl"])+1,
                                                                                        right_end=max(dict_pos["cl"])+1)

        out, err = call_command(('python /u/leongs/projet_naim/finalfold/remaining/bp_stats_frag_both_50/mcsymizer.py '
                                 '--db_path /u/leongs/Downloads/MC-Sym_Linux/MCSYM-DB '
                                 '--name "{name}_{index}" --sequence1 "{seq}" '
                                 '--structure1 "{structure}" '
                                 '--max_number 1000 --timeout 60 '
                                 '--external_library "{ext_lib_str}" '
                                 '--library_diversity 0.0').format(name=name,
                                                                   seq=seq,
                                                                   structure=structure,
                                                                   index=i+1,
                                                                   ext_lib_str=ext_lib_str), echo=True)
        with open(os.path.join(workdir, "work", name + ".mcc"), 'wb') as mcc_o:
            mcc_o.write(out)
        with open(os.path.join(workdir, name + "_" + str(i+1) + ".mcc"), 'wb') as mcc_o:
            mcc_o.write(out)

        # prepare to launch MCSYM
        os.environ["MCSYM_DB"] = "/soft/bioinfo/share/mcsym/db/mcsymdb-4.2.1.bin.gz"
        MCSYM_DB = "/u/leongs/Downloads/MC-Sym_Linux/MCSYM-DB"
        # make the symling to MCSYM_DB
        os.symlink(MCSYM_DB, os.path.join(workdir, "work", 'MCSYM-DB'))

        try:
            out, err = call_command('/u/admc/soft/MCSYM-soft/cgi-bin/MC-Sym/prep_mcsym.exe {script}'.format(script=os.path.join(workdir, "work", name + ".mcc")), echo=True)
            if err:
                with open(os.path.join(workdir, name + "_" + str(i+1) + ".err"), 'wb') as out_l:
                    out_l.write(err)
                print err
        except Exception as e:
            print (str(e))

        # run MCSYM
        out, err = call_command("/soft/bioinfo/linux_RH5/mcsym-4.2.2/bin/mcsym " + name + ".mcc", echo=True)

        if err:
            print err

        list_pdb = [elem for elem in os.listdir(os.path.join(workdir, "work")) if elem.endswith("pdb.gz")]
        print list_pdb

        list_params = [dict(search3d_path=search3d_path,
                            seq=seq,
                            struct=structure,
                            matfile=mature,
                            pdb=elem,
                            name=name + "_" + str(i+1),
                            workdir=workdir,
                            output_log=output_log,
                            struct_num=i) for elem in sorted(list_pdb)]

        if list_pdb:
            for param in list_params:
                call_search3d(param)
            
#             pool = multiprocessing.Pool(threads)
#             pool.map(call_search3d, list_params, chunksize=1)

        list_fit = [elem for elem in os.listdir(workdir) if elem.endswith("pdb.gz")]

        if os.path.exists(os.path.join(workdir, "work")):
            try:
                shutil.rmtree("work")
            except Exception as e:
                print e
#         if len(list_fit):
#             for pdb in [elem for elem in os.listdir(os.path.join(workdir, "work")) if elem.endswith("pdb")]:
#                 try:
#                     call_command("gzip "+pdb, echo=True)
#                 except Exception as e:
#                     print e
#             break