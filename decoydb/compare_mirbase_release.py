diff_file = "/u/leongs/Downloads/miRNA.diff/miRNA.diff"


count_new = 0
count_del = 0
count_nc = 0
count_sc = 0
with open(diff_file, 'rb') as diff_f:
    for line in diff_f:
        if line.startswith("#"):
            continue
        else:
            splitted = line.split()
            if len(splitted) == 3:
                acc = splitted[0]
                name = splitted[1]
                action = splitted[2]
                
                if name.startswith("hsa") and not acc.startswith("MIMAT"):
                    if "NEW" in action:
                        count_new += 1
                    elif "DELETED" in action:
                        count_del += 1
                    elif "SEQUENCE" in action:
                        count_sc += 1
                    elif "NAME" in action:
                        count_nc += 1

print count_new, count_del, count_sc, count_nc

hairpin_file = "/u/leongs/Downloads/hairpin.fa/hairpin.fa"

with open(hairpin_file, 'rb') as h_f:
    print h_f.read().count("hsa-mir")